<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Unauthorized Page -401</title>
        
    </head>
    <%
        // Invalidate the session.
         session.invalidate();
    %>
    <body bgcolor="#e6e2d7">
    
        <div>
            <p>&nbsp;</p>
        </div>
        <table width="450" align="center"
               style="border-radius: 20px; background: #fdfdfd; /* Old browsers */
background: -moz-linear-gradient(top,  #fdfdfd 0%, #cccccc 50%, #fdfdfd 100%, #a6a6a6 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#fdfdfd), color-stop(50%,#cccccc), color-stop(100%,#fdfdfd), color-stop(100%,#a6a6a6)); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  #fdfdfd 0%,#cccccc 50%,#fdfdfd 100%,#a6a6a6 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  #fdfdfd 0%,#cccccc 50%,#fdfdfd 100%,#a6a6a6 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  #fdfdfd 0%,#cccccc 50%,#fdfdfd 100%,#a6a6a6 100%); /* IE10+ */
background: linear-gradient(to bottom,  #fdfdfd 0%,#cccccc 50%,#fdfdfd 100%,#a6a6a6 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fdfdfd', endColorstr='#a6a6a6',GradientType=0 ); /* IE6-9 */ ">
            <tr>
                <td height="53" align="center" valign="top">
                    <p>&nbsp;</p>
                </td>
            </tr>
             
            <tr>
                <td width="100%" align="center" dir="rtl"
                    style="font-family:Arial; font-size:24px; color:#6c0033; font-weight:bold;">
                    <p>
                        الصفحة غير مصرح بها
                        <br/>
                         الرجاء المحاولة لاحقاً<br/>
                    </p>
                </td>
            </tr>
             
            <tr>
                <td align="center" style="font-family:Arial; font-size:18px; color:#6c0033; font-weight:bold;">
                    The page is not authorized,
                    <br/>
                     Please check back later
                </td>
            </tr>
             
            <tr>
                <td align="center">&nbsp;</td>
            </tr>
             
            <!--<tr>
    <td align="center" style="font-family:Arial; font-size:14px; color:#e66c00;">webmaster@baladiya.gov.qa</td>
  </tr>-->
             
            <tr>
                <td align="center">&nbsp;</td>
            </tr>
             
            <tr>
                <td align="center" dir="rtl"
                    style="font-family:Arial; font-size:20px; color:#666666; font-weight:bold;">وزارة البلديــــة
                                                                                                والبيئـــة</td>
            </tr>
             
            <tr>
                <td align="center" style="font-family:Arial; font-size:16px; color:#666666; font-weight:bold;">Ministry of Municipality
                                                                                                and Environment</td>
            </tr>
             
            <tr>
                <td align="font-family:Arial; font-size:16px; color:#666666; font-weight:bold;">
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                </td>
            </tr>
        </table>
        <p>&nbsp;</p>
    </body>
</html>