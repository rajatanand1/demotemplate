package qa.gov.mme.envservices.naturalresources.view.constant;

public class NaturalResourcesConstants {
    public NaturalResourcesConstants() {
        super();
    }
    public static final String NATURAL_RESOURCES_VIEW_CONTROLLER_BUNDLE =
        "qa.gov.mme.envservices.naturalresources.view.bundle.NaturalResourcesViewControllerBundle";
    public static final String PUBLIC_INBOX_PARAM = "PUB_INBOX";
    public static final String PRIVATE_INBOX_PARAM = "PRV_INBOX";
    public static final String TASKS_ASSIGN_PARAM = "TASKS_ASSIGN";
    public static final String DEBRIS_INVENTORIES_PARAM = "DEBRIS_INVENTORIES";
    public static final String NATURAL_RESOURCES_PRIV_PROCESS_DEV =
        "IssueCrusherLicense,QuarryProductionReportProcess,NewQuarryPermitRequestProcess,InspectionNaturalResProcess,RenewQuarryPermitRequest,IssueTransferDebrisLicense,AmendTransferDebrisLicense,RenewTransferDebrisLicense,TransferDebrisLicenseProcesses,RenewCrusherLicense,AmendCrusherLicense,IssueDetonationLicense,RenewDetonationLicense";
    public static final String REASSIGN_POPUP_ID = "reassignTaskPopup";
    //Constants for common attachment
    public static final String CMN_ATTACHMENTS_CONFIRMATION_POPUP_ID = "cmnAttachmentConfirmationPopupId";
    public static final String APPLICATION_PDF = "application/pdf";
    public static final String IMAGE_PNG = "image/png";
    public static final String IMAGE_JPG = "image/jpg";
    public static final String IMAGE_JPEG = "image/jpeg";
    public static final String FILE_CONTENT_T = "FileContentT";
    public static final String FILE_TITLE_T = "FileTitleT";
    public static final String MIME_TYPE_T = "MimeTypeT";
    public static final String P_SHOW_MSG = "pShowMsg";
    public static final String P_POPUP_VAL = "pPopupVal";
    public static final String FILE_UPLOAD = "FILE_UPLOAD";
    public static final String FILE_DELETE = "FILE_DELETE";
    public static final String CMN_ATTACHMENTS_INPUT_FILE_ID = "cmnAttachmentInputFileId";
    public static final String CMN_ATTACHMENTS_FILE_UPLOAD_POPUP_ID = "cmnAttachmentFileUploadPopupId";
    public static final String CMN_PREVIEW_CONFIRMATION_POPUP_ID = "previewConfirmPopupId";
    public static final String DEFAULT_ACCESS_MODE = "BACK";
    public static final String HOME_TF =
        "/WEB-INF/naturalresources/taskflows/home/NaturalResourcesHomeTF.xml#NaturalResourcesHomeTF";
    public static final String INBOX_TF =
        "/WEB-INF/naturalresources/taskflows/inbox/NaturalResourcesInboxTF.xml#NaturalResourcesInboxTF";
    public static final String DEBRIS_INVENTORIES_TF =
        "/WEB-INF/naturalresources/taskflows/DebrisInve/DebrisInveAdminTF.xml#DebrisInveAdminTF";
    public static final String REQ_TYPE = "REQ_TYPE";
    public static final String NONE_TF =
        "/WEB-INF/naturalresources/taskflows/viewrequestdetails/TaskFlowNoneTF.xml#TaskFlowNoneTF";
    //#####################Common View Objects#####################
    public static final String BENEFICIARY_LIST = "CmnEntitiesBeneficiaryListRVO1";
    public static final String ATTACHMENT_LIST = "CmnRequestsAttachmentsRVO1";
    public static final String APPLICANT_LIST = "CmnEntityVwRVO";
    public static final String ADDRESS_DATAIL = "CmnAddressesVO1";
    public static final String COMMON_LAND_LIST = "CommonDataHoldingPVO";
    //#####################Attributes#####################
    public static final String EID = "EstablishmentId";
    public static final String ENGLISH_NAME = "NameEn";
    public static final String ARABIC_NAME = "NameAr";
    public static final String TASKS_ASSIGN_TF =
        "/WEB-INF/naturalresources/taskflows/taskassignment/NaturalResourcesTaskAssignmentTF.xml#NaturalResourcesTaskAssignmentTF";
    public static final String SEARCH_REQUESTS_TF =
        "/WEB-INF/naturalresources/taskflows/requestsearch/NaturalResourcesRequestSearchTF.xml#NaturalResourcesRequestSearchTF";
    public static final String DEBRIS_SEARCH_TF =
        "/WEB-INF/naturalresources/taskflows/movedberispermitsearch/MoveDebrisPermitSearchTF.xml#MoveDebrisPermitSearchTF";
    public static final String APPROVE_REQ_TD = "ApproveRequestTask";
    public static final String ACTV_CONFIRM_TD = "ActivityConfirmationTask";
    public static final String P_APPROVE = "pApprove";
    public static final String P_REJECT = "pReject";
    public static final String P_ACCEPT = "pAccept";
    public static final String P_RETURN = "pReturn";
    public static final String P_INSPECTION = "pInspection";
    public static final String P_RETURN_NAME = "pReturnName";
    public static final String P_ACCEPT_NAME = "pAcceptName";
    public static final String CHAR_Y = "Y";
    public static final String CHAR_N = "N";
    public static final String CLAIM = "CLAIM";
    public static final String P_TASK_ID = "pTaskId";
    public static final String REQUEST_ASSIGNED_SUCCESS_MSG = "REQUEST_ASSIGNED_SUCCESS_MSG";
    public static final String CLAIM_TASK_BTN = "ClaimTaskBtn";
    public static final String RELEASE = "RELEASE";
    public static final String CHAR_NA = "NA";
    public static final String P_TASK_DEFINITION = "pTaskDefinition";
    public static final String VALIDATE_APPROVE_DATA = "validateApprovalData";
    public static final String P_REQ_TYPE = "pReqType";
    public static final String P_IS_DELIVER = "pIsDeliver";
    public static final String P_REQUEST_ID = "pRequestId";
    public static final String APPROVE = "APPROVE";
    public static final String ACCEPT = "ACCEPT";
    public static final String P_APPROVAL_COMMENTS = "pApprovalComments";
    public static final String P_VD_CONFIRM_MSG = "pViewDetailsConfirmMsg";
    public static final String REQUEST_APPROVAL_SUBMISSION_MSG = "REQUEST_APPROVAL_SUBMISSION_MSG";
    public static final String REQ_PROCESSED_SUCCESS = "REQ_PROCESSED_SUCCESS";
    public static final String SUCCESS = "SUCCESS";
    public static final String REJECT = "REJECT";
    public static final String RETURN = "RETURN";
    public static final String INSPECTION = "INSPECTION";
    public static final String REQUEST_REJECT_SUBMISSION_MSG = "REQUEST_REJECT_SUBMISSION_MSG";
    public static final String TO_INBOX = "toInbox";
    public static final String SUBMIT_APPROVE_DATA = "submitApprovalData";
    public static final String VIEW_DTLS_POPUPID = "viewDtlsPopupId";
    public static final String PDF = "PDF";
    public static final String ROW_ID = "ROW_ID";
    public static final String PARAMETER_VALUE = "ParameterValue";

    public static final String P_FROM_OUTCOME = "pFromOutcome";
    public static final String DO_FILTER_REQ_ID_NO = "doFilterByRequestIdOrNo";
    public static final String BPM_PROC_INS_ID = "BpmProcessInstanceId";
    public static final String P_COMP_INS_ID = "pCompositeInstanceId";
    public static final String P_REQ_STAT = "pReqStat";
    public static final String P_REQUEST_NO = "pRequestNo";
    public static final String INVALID_REQUEST = "INVALID_REQUEST";
    public static final String MUNICIPALITY_ID = "MunicipalityId";
    public static final String DELIVERY_TF = "/WEB-INF/delivery/taskflows/DeliverySearchTF.xml#DeliverySearchTF";
    public static final String REVIEW_REQUEST_TASK = "ReviewRequestTask";
    public static final String ATTACH_FLAG = "AttachFlag";
    public static final String FALSE_BOOLEAN = "false";
    public static final String TRUE_BOOLEAN = "true";
    public static final String ACK_POPUP_CID = "ackPopup";
    public static final String GET_ACKMSG_MTHD = "getAcknowledgeMessage";
    public static final String P_ACK_MSG = "pAckMsg";
    public static final String AGREE_BTN_CID = "agreeBtn";
    public static final String CONF_POPUP_CID = "confPopup";
    public static final String PLS_TRY_LATER_KEY = "PLEASE_TRY_AGAIN_LATER";
    public static final String ERR_SRV_CALL_KEY = "ERROR_WHILE_SERVICE_CALL";
    public static final String TRANSACTION_ID = "TransactionId";
    public static final String ERROR_CODE = "ErrorCode";
    public static final String ERROR_MESSAGE = "ErrorMessage";
    public static final String P_TRANSACTION_ID = "pTransactionId";
    public static final String FAILURE = "FAILURE";
    public static final String P_SAVE = "pSave";

    //Login Servlet code
    public static final String NATURAL_RESOURCES_AM_DEF =
        "qa.gov.mme.envservices.naturalresources.model.bc.app.NaturalResourcesAM";
    public static final String NATURAL_RESOURCES_AM_CONFIG = "NaturalResourcesAMLocal";
    public static final String DELIVERY = "DELIVERY";

    //#####################Data Control#####################
    public static final String CUSTOM_COMPONENT_DC = "CustomComponentAMDataControl";
    public static final String NATURAL_RESOURCE_DC = "NaturalResourcesAMDataControl";
    //#####################Data Control#####################

    //*** Below constants are for Inbox ***
    public static final String TASKS_TRANSFER = "TASKS_TRANSFER";
    //Constants for common attachment
    //#####################Common View Objects#####################

    public static final String BENEFICIARIES = "beneficiaries";
    //#####################Attributes#####################

    public static final String FAILED = "FAILED";

    public static final String QUARRIES_DATA_ADMIN_TF =
        "/WEB-INF/naturalresources/taskflows/quarries/QuarriesDataAdminTF.xml#QuarriesDataAdminTF";
    public static final String QUARRY_PRODREPORT_DETAIL_TF =
        "/WEB-INF/naturalresources/taskflows/quarryprodrpt/QuarryProdReportTF.xml#QuarryProdReportTF";
    public static final String QUARRY_PRODREPORT_SUBMIT_TF =
        "/WEB-INF/naturalresources/taskflows/quarryprodrpt/QuarryProdReportSubmitTF.xml#QuarryProdReportSubmitTF";
    public static final String NEW_QUARRY_PERMIT_SUBMIT_TF =
        "/WEB-INF/naturalresources/taskflows/newquarrypermit/NewQuarryPermitsSubmitTF.xml#NewQuarryPermitsSubmitTF";
    public static final String RENEW_QUARRY_PERMIT_SUBMIT_TF =
        "/WEB-INF/naturalresources/taskflows/renewquarrypermit/RenewQuarryPermitSubmitTF.xml#RenewQuarryPermitSubmitTF";
    public static final String NEW_DETONATION_PERMIT_SUBMIT_TF =
        "/WEB-INF/naturalresources/taskflows/detonation/SubmitDetonationTF.xml#SubmitDetonationTF";
    public static final String NEW_DBRS_PERMIT_SUBMIT_TF =
        "/WEB-INF/naturalresources/taskflows/newMvDbrsPermits/NewMvDbrsPermitSubmitTF.xml#NewMvDbrsPermitSubmitTF";
    public static final String RENEW_MOVE_DBRS_PERMIT_SUBMIT_TF =
        "/WEB-INF/naturalresources/taskflows/renewMvDbrsPmtSubmit/RenewMvDbrsPmtSubmitTF.xml#RenewMvDbrsPmtSubmitTF";
    public static final String MODIFY_MOVE_DBRS_PERMIT_SUBMIT_TF =
        "/WEB-INF/naturalresources/taskflows/modifyMvDbrsPermit/modifyMvDbrsPermitSubmitTF.xml#modifyMvDbrsPermitSubmitTF";
    public static final String RENEW_DETONATION_PERMIT_SUBMIT_TF =
        "/WEB-INF/naturalresources/taskflows/renewquarrypermit/RenewQuarryPermitSubmitTF.xml#RenewQuarryPermitSubmitTF";
    public static final String P_ACCESS_MODE = "pAccessMode";
    public static final String P_TIME_STAMP = "pTimeStamp";
    public static final String P_SERVICE_TITLE = "pTitle";

    public static final String CLEAR_DATE_NEWREQ_MTHD = "clearDataForNewRequest";

    public static final String ID = "Id";
    public static final String IS_MANDATORY = "IsMandatory";
    public static final String YES = "Y";
    public static final String NO = "N";
    public static final String NEW_QRY_PERMIT_REQ_DET =
        "/WEB-INF/naturalresources/taskflows/newquarrypermit/NewQuarryPermitsReqDetailsTF.xml#NewQuarryPermitsReqDetailsTF";
    public static final String MOVE_DEBRIS_PERMIT_REQ_DET =
        "/WEB-INF/naturalresources/taskflows/movedberispermitsearch/MoveDebrisPermitReqDetailsTF.xml#MoveDebrisPermitReqDetailsTF";
    public static final String BENEFICIARY_ID = "pBeneficiaryId";

    public static final String UPDATE_REQ_TASK = "UpdateRequestTask";
    public static final String VALIDATE_REQ_TASK = "ValidateRequestTask";
    public static final String ATTEST_REQ_TASK = "AttestRequestTask";

    public static final String SEARCH_QRY_PERMIT =
        "/WEB-INF/naturalresources/taskflows/quarries/SearchQuarryPermitsAdminTF.xml#SearchQuarryPermitsAdmin";
    public static final String RENEW_QRY_PERMIT_REQ_DET =
        "/WEB-INF/naturalresources/taskflows/renewquarrypermit/RenewQuarryRequestDetailsTF.xml#RenewQuarryRequestDetailsTF";

    public static final String DETONATION_PERMIT_REQ_DET =
        "/WEB-INF/naturalresources/taskflows/detonation/DetonationRequestDetailsTF.xml#DetonationRequestDetailsTF";

    public static final String DETONATION_PERMIT_DET =
        "/WEB-INF/naturalresources/taskflows/detonation/DetonationPermitsTF.xml#DetonationPermitsTF";

    public static final String QRY_RPT_REQ_DET =
        "/WEB-INF/naturalresources/taskflows/quarryprodrpt/QuarryProdReportTF.xml#QuarryProdReportTF";
    public static final String ENV_DEBRIS_ADMIN =
        "/WEB-INF/naturalresources/taskflows/debrisinvadmin/DebrisInvAdminTF.xml#DebrisInvAdminTF";
    public static final String P_SHOW_INSPECT = "pShowInspect";
    public static final String P_SHOW_FEES = "pShowFees";
    public static final String APPROVE_REQ_TASK = "ApproveRequestTask";
    public static final String QUARRY_PERMIT_REPORT = "MME/Env/Nat Resource/Quarries/Permit.xdo";
    public static final String DEBRIS_PERMIT_REPORT = "MME/Env/Nat Resource/Dibres/Dibress_Report.xdo";
    public static final String DEBRIS_SUMMARY_REPORT = "MME/Env/Nat Resource/Dibres/Summary.xdo";
    public static final String SELECT_INSPECT_TASK = "SelectInspectorTask";
    public static final String INSPECT_LOCATE_TASK = "InspectLocationTask";
    public static final String REVIEW_INSPECT_TASK = "ReviewInspectionTask";
    public static final String ATTST_INSPECT_RPT_TASK = "AttestInspectionReportTask";
    public static final String IS_REQUEST_INSPECTION_EXIST = "isRequestInspectionExist";
    public static final String P_INSPECTION_EDIT_MODE = "pInspectionEditMode";

    public static final String SEARCH_QUARRY_RPT_TF =
        "/WEB-INF/naturalresources/taskflows/srchqryrpt/SearchQuarryReportTF.xml#SearchQuarryReportTF";
    public static final String CRUSHER_PERMIT_SUBMIT_TF =
        "/WEB-INF/naturalresources/taskflows/crusherpermit/submit/CrusherPermitSubmitTF.xml#CrusherPermitSubmitTF";
    public static final String P_DEBRIS_PERMIT_REPORT = "debrispermitreport";
    public static final String P_SUMMARY_REPORT = "SUMMARY_REPORT";

    public static final String SUBMIT_DETONATION = "SUBMIT_DETONATION";
    public static final String RENEW_DETONATION = "RENEW_DETONATION";


    public static final String REPORT_TYPE = "reportType";
    public static final String QUARRY_RPT_CODE = "qrypermitreport";
    public static final String CRUSHER_SUBMIT = "CRUSHER_SUBMIT";
    public static final String RENEW_CRUSHER_SUBMIT = "RENEW_CRUSHER_SUBMIT";
    public static final String MODIFY_CRUSHER_SUBMIT = "MODIFY_CRUSHER_SUBMIT";
    public static final String CRUSHER_PERMIT_DETAILS_TF =
        "/WEB-INF/naturalresources/taskflows/crusherpermit/CrusherPermitDetailsTF.xml#CrusherPermitDetailsTF";
    public static final String CRUSHER_PERMIT_SEARCH_TF =
        "/WEB-INF/naturalresources/taskflows/crusherpermitsearch/EnvCrusherPermitsSearchTF.xml#EnvCrusherPermitsSearchTF";
    public static final String SEARCH_CRUSHER_PERMITS = "SEARCH_CRUSHER_PERMITS";
    public static final String SEARCH_REQUESTS = "SEARCH_REQUESTS";
    public static final String CHANGES_SAVE_SUCCESS_MSG = "CHANGES_SAVE_SUCCESS_MSG";
    public static final String SUBMIT_APPROVAL_DATA = "submitApprovalData";
    public static final String PREVIOUS_PERMITS = "PREVIOUS_PERMITS";
    public static final String CRUSHER_PERMITS_TF =
        "/WEB-INF/naturalresources/taskflows/crusherpermit/CrusherPermitsTF.xml#CrusherPermitsTF";
    public static final String HOME = "HOME";
    public static final String TASKS_ASSIGNMENT = "TASKS_ASSIGNMENT";
    public static final String PRIVATE_INBOX = "PRIVATE_INBOX";
    public static final String PUBLIC_INBOX = "PUBLIC_INBOX";
    public static final String CRUSHER_PERMIT_REQUEST = "CRUSHER_PERMIT_REQUEST";
    public static final String RENEW_CRUSHER_PERMIT_REQUEST = "RENEW_CRUSHER_PERMIT_REQUEST";
    public static final String MODIFY_CRUSHER_PERMIT_REQUEST = "MODIFY_CRUSHER_PERMIT_REQUEST";
    public static final String p_REQUEST_TYPE_ID = "pRequestTypeId";
    public static final String PERMIT_NO_REQUIRED_MSG = "PERMIT_NO_REQUIRED_MSG";
    public static final String PERMIT_DATE_REQUIRED_MSG = "PERMIT_DATE_REQUIRED_MSG";
    public static final String P_INSPECTION_CHECK_BOX = "pInspectionCheckBox";

    /**--------------------------------------------------------------------------------------------**/

    public static final String FALCON_PASSPORT_SUBMIT_TF =
     //   "/WEB-INF/naturalresources/taskflows/crusherpermit/submit/CrusherPermitSubmitTF.xml#CrusherPermitSubmitTF";
      "WEB-INF/naturalresources/taskflows/falconpassport/submit/FalconPassportSubmitTF.xml#FalconPassportSubmitTF";
}
