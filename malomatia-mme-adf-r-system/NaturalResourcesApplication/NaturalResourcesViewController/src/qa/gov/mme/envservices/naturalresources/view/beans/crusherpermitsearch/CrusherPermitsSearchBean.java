package qa.gov.mme.envservices.naturalresources.view.beans.crusherpermitsearch;

import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.RichPopup;

import oracle.adf.view.rich.util.ResetUtils;

import qa.gov.mme.common.utils.ADFUtils;
import qa.gov.mme.envservices.naturalresources.view.constant.NaturalResourcesConstants;

public class CrusherPermitsSearchBean {
    private RichPopup crusherPermitDetailsPopupBnd;

    public CrusherPermitsSearchBean() {
    }

    public void setCrusherPermitDetailsPopupBnd(RichPopup crusherPermitDetailsPopupBnd) {
        this.crusherPermitDetailsPopupBnd = crusherPermitDetailsPopupBnd;
    }

    public RichPopup getCrusherPermitDetailsPopupBnd() {
        return crusherPermitDetailsPopupBnd;
    }

    public void onCrusherPermitSearchDetailsActionListener(ActionEvent actionEvent) {
        // Add event code here...
        ADFUtils.executeOperationBinding("doFilterPermitDetails");
        if (null != crusherPermitDetailsPopupBnd) {
            ResetUtils.reset(crusherPermitDetailsPopupBnd);
            ADFUtils.showPopup(crusherPermitDetailsPopupBnd, true);
        }

    }

    public void onCrusherPermitSearchPrintActionListener(ActionEvent actionEvent) {
        // Add event code here...
    }

    public void onCrusherPermitSearchActionListener(ActionEvent actionEvent) {
        // Add event code here...
        ADFUtils.executeOperationBinding("doEnvCrusherPermitSearch");
    }

    public void onCrusherPermitClearActionListener(ActionEvent actionEvent) {
        // Add event code here...
        ADFUtils.executeOperationBinding("doClearPermitDetails");
    }

    public void initPermitsSearch() {
        // Add event code here...
        String lcSource = null;
        lcSource = (String) ADFUtils.getPageFlowScopeAttribute("pSource");
        if (null != lcSource && NaturalResourcesConstants.PREVIOUS_PERMITS.equals(lcSource)) {
            ADFUtils.executeOperationBinding("doFilterPermitByBeneficiaryId");
        }


    }
}
