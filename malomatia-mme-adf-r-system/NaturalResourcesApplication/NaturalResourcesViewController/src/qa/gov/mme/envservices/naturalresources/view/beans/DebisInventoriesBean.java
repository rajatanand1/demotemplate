package qa.gov.mme.envservices.naturalresources.view.beans;

import javax.faces.component.UIComponent;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import oracle.jbo.ViewObject;

import oracle.jbo.server.ViewObjectImpl;

import qa.gov.mme.common.beans.CommonBaseBean;
import qa.gov.mme.common.utils.ADFUtils;
import qa.gov.mme.common.utils.JSFUtils;

public class DebisInventoriesBean extends CommonBaseBean{
    public DebisInventoriesBean() {
    }

    public void clearActionListener(ActionEvent actionEvent) {
        ADFUtils.setBoundAttributeValue("nameAr", null);
        ADFUtils.setBoundAttributeValue("nameEn", null);
        ADFUtils.setBoundAttributeValue("schMunicipalityAr", null);
        ADFUtils.setBoundAttributeValue("schMunicipalityEn", null);
        ADFUtils.setBoundAttributeValue("ZoneNoAr", null);
        ADFUtils.setBoundAttributeValue("ZoneNoEn", null);
        ADFUtils.setBoundAttributeValue("StreetNoAr", null);
        ADFUtils.setBoundAttributeValue("StreetNoEn", null);
        ADFUtils.setBoundAttributeValue("isActive", null);
        ADFUtils.setBoundAttributeValue("locationName", null);
        
        RichInputText nameEnBinding = (RichInputText)JSFUtils.findComponentInRoot("it1");
        nameEnBinding.resetValue();
        RichInputText nameArBinding = (RichInputText)JSFUtils.findComponentInRoot("it2");
        nameArBinding.resetValue();
        RichSelectOneChoice schMunicipalityAr = (RichSelectOneChoice)JSFUtils.findComponentInRoot("soc8");
        schMunicipalityAr.resetValue();
        RichSelectOneChoice schMunicipalityEn = (RichSelectOneChoice)JSFUtils.findComponentInRoot("soc7");
        schMunicipalityEn.resetValue();
        
        RichSelectOneChoice zoneNoAr = (RichSelectOneChoice)JSFUtils.findComponentInRoot("soc6");
        zoneNoAr.resetValue();
        RichSelectOneChoice zoneNoEn = (RichSelectOneChoice)JSFUtils.findComponentInRoot("soc71");
        zoneNoEn.resetValue();
        
        RichSelectOneChoice streetNoAr = (RichSelectOneChoice)JSFUtils.findComponentInRoot("soc61");
        streetNoAr.resetValue();
        RichSelectOneChoice streetNoEn = (RichSelectOneChoice)JSFUtils.findComponentInRoot("soc72");
        streetNoEn.resetValue();
                
        RichSelectBooleanCheckbox isActive = (RichSelectBooleanCheckbox)JSFUtils.findComponentInRoot("it7");
        isActive.resetValue();
        RichInputText locationName = (RichInputText)JSFUtils.findComponentInRoot("it6");
        locationName.resetValue();

        
        reRenderComponents(new String[]{"it1","it2","soc8","soc7","soc6","soc71","soc61","soc72","it7","it6"});
    }

    public void addSeedlingsActionListener(ActionEvent actionEvent) {
        // Add event code here...
    }

    public void addInventoryActionListener(ActionEvent actionEvent) {
        // Add event code here...
    }

    public void changeZoneNoListener(ValueChangeEvent valueChangeEvent) {
        ViewObject vo=ADFUtils.findIterator("LkpZonesView1Iterator").getViewObject();
        vo.setNamedWhereClauseParam("pZoneBind", valueChangeEvent.getNewValue());
        vo.executeQuery();
        //reRenderComponents(new String[]{"mfa31"});
    }

    public void changeMunicipalityListener(ValueChangeEvent valueChangeEvent) {
        ViewObject vo=ADFUtils.findIterator("LkpZonesView1Iterator").getViewObject();
        vo.setNamedWhereClauseParam("pMunicipalityBind", valueChangeEvent.getNewValue());
        vo.executeQuery();
        //reRenderComponents(new String[]{"mfa3","mfa31"});
        ViewObject vo2=ADFUtils.findIterator("LkpZonesView1Iterator").getViewObject();
        vo2.executeEmptyRowSet();
        
        UIComponent component1=findComponentInRoot("soc6");
        UIComponent component2=findComponentInRoot("soc71");
        UIComponent component3=findComponentInRoot("soc61");
        UIComponent component4=findComponentInRoot("soc72");
        AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
        adfFacesContext.addPartialTarget(component1);
        adfFacesContext.addPartialTarget(component2);
        adfFacesContext.addPartialTarget(component3);
        adfFacesContext.addPartialTarget(component4);
    }
}
