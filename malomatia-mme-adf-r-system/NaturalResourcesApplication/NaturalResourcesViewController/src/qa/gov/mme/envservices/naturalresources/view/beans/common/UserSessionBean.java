package qa.gov.mme.envservices.naturalresources.view.beans.common;

import java.io.Serializable;

import java.math.BigDecimal;

import javax.faces.event.ActionEvent;

import oracle.adf.controller.TaskFlowId;

import oracle.jbo.domain.Timestamp;

import qa.gov.mme.common.beans.CommonBaseBean;
import qa.gov.mme.common.utils.ADFUtils;
import qa.gov.mme.common.utils.UIUtils;
import qa.gov.mme.envservices.naturalresources.view.constant.NaturalResourcesConstants;
import qa.gov.mme.envservices.naturalresources.view.utils.NaturalResourcesUtils;


public class UserSessionBean extends CommonBaseBean implements Serializable {
    @SuppressWarnings("compatibility:7902898217969623773")
    private static final long serialVersionUID = 1L;
    private transient String accessMode = NaturalResourcesConstants.DEFAULT_ACCESS_MODE;
    private String currentPageTitle =
        NaturalResourcesUtils.getMessageFromResourceBundle(NaturalResourcesConstants.HOME);
    private String taskFlowId = NaturalResourcesConstants.HOME_TF;

    public UserSessionBean() {
        super();
    }

    public void setAccessMode(String accessMode) {
        this.accessMode = accessMode;
    }

    public String getAccessMode() {
        return accessMode;
    }

    public void setCurrentPageTitle(String currentPageTitle) {
        if (null != currentPageTitle) {
            this.currentPageTitle = NaturalResourcesUtils.getMessageFromResourceBundle(currentPageTitle);
        } else {
            this.currentPageTitle = currentPageTitle;
        }
    }

    public String getCurrentPageTitle() {
        return currentPageTitle;
    }


    public void setTaskFlowId(String taskFlowId) {
        this.taskFlowId = taskFlowId;
    }

    public String getTaskFlowId() {
        return taskFlowId;
    }

    public TaskFlowId getDynamicTaskFlowId() {
        return TaskFlowId.parse(taskFlowId);
    }

    public void setDynamicTaskFlowId(String taskFlowId) {
        setCurrentPageTitle(NaturalResourcesConstants.HOME);
        this.taskFlowId = taskFlowId;
    }

    public String publicInboxTF() {
        setDynamicTaskFlowId(NaturalResourcesConstants.INBOX_TF);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_FROM_OUTCOME,
                                           NaturalResourcesConstants.PUBLIC_INBOX_PARAM);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_ACCESS_MODE,
                                           NaturalResourcesConstants.DEFAULT_ACCESS_MODE);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        ADFUtils.markPageFlowScopeAsDirty();
        return null;
    }

    public String privateInboxTF() {
        setDynamicTaskFlowId(NaturalResourcesConstants.INBOX_TF);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_FROM_OUTCOME,
                                           NaturalResourcesConstants.PRIVATE_INBOX_PARAM);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_ACCESS_MODE,
                                           NaturalResourcesConstants.DEFAULT_ACCESS_MODE);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        ADFUtils.markPageFlowScopeAsDirty();
        return null;
    }


    public String tasksAssignmentTF() {
        System.out.println("Task assignment screen called");
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_FROM_OUTCOME,
                                           NaturalResourcesConstants.TASKS_ASSIGN_PARAM);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_ACCESS_MODE,
                                           NaturalResourcesConstants.DEFAULT_ACCESS_MODE);
        setDynamicTaskFlowId(NaturalResourcesConstants.TASKS_ASSIGN_TF);
        setCurrentPageTitle(NaturalResourcesConstants.TASKS_ASSIGNMENT);
        ADFUtils.markPageFlowScopeAsDirty();
        return null;
    }


    public String onHomeAction() {
        // Add event code here...
        this.setDynamicTaskFlowId(NaturalResourcesConstants.HOME_TF);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_ACCESS_MODE,
                                           NaturalResourcesConstants.DEFAULT_ACCESS_MODE);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        ADFUtils.markPageFlowScopeAsDirty();
        return null;
    }

    public String searchRequestsTF() {
        setDynamicTaskFlowId(NaturalResourcesConstants.SEARCH_REQUESTS_TF);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_ACCESS_MODE,
                                           NaturalResourcesConstants.DEFAULT_ACCESS_MODE);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        setCurrentPageTitle(NaturalResourcesConstants.SEARCH_REQUESTS);
        ADFUtils.markPageFlowScopeAsDirty();
        return null;
    }

    public String searchQuarryReportTF() {
        setDynamicTaskFlowId(NaturalResourcesConstants.SEARCH_QUARRY_RPT_TF);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_ACCESS_MODE,
                                           NaturalResourcesConstants.DEFAULT_ACCESS_MODE);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        setCurrentPageTitle("SRCH_QRY_PROD_RPT");
        ADFUtils.markPageFlowScopeAsDirty();
        return null;
    }

    public String debrisSearchTF() {
        setDynamicTaskFlowId(NaturalResourcesConstants.DEBRIS_SEARCH_TF);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        setCurrentPageTitle("MOVE_DEBRIS_PERMIT_SEARCH");
        ADFUtils.markPageFlowScopeAsDirty();
        return null;
    }

    public void goPrivateInboxTF(ActionEvent ActionEvent) {
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_FROM_OUTCOME,
                                           NaturalResourcesConstants.PRIVATE_INBOX_PARAM);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_ACCESS_MODE,
                                           NaturalResourcesConstants.DEFAULT_ACCESS_MODE);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        setCurrentPageTitle(NaturalResourcesConstants.TASKS_ASSIGNMENT);
        ADFUtils.markPageFlowScopeAsDirty();
    }

    public void goPublicInboxTF(ActionEvent ActionEvent) {
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_FROM_OUTCOME,
                                           NaturalResourcesConstants.PUBLIC_INBOX_PARAM);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_ACCESS_MODE,
                                           NaturalResourcesConstants.DEFAULT_ACCESS_MODE);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        setCurrentPageTitle(NaturalResourcesConstants.PRIVATE_INBOX);
        ADFUtils.markPageFlowScopeAsDirty();
    }

    public String onSearchDebrisInventoriesAction() {
        // Add event code here...
        this.setDynamicTaskFlowId(NaturalResourcesConstants.DEBRIS_INVENTORIES_TF);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_FROM_OUTCOME,
                                           NaturalResourcesConstants.PUBLIC_INBOX_PARAM);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        ADFUtils.markPageFlowScopeAsDirty();
        return null;
    }

    private Timestamp getCurrentTimeStamp() {
        return new Timestamp(System.currentTimeMillis());
    }

    public String onQuarriesDataAdminAction() {
        // Add event code here...
        this.setDynamicTaskFlowId(NaturalResourcesConstants.QUARRIES_DATA_ADMIN_TF);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        ADFUtils.markPageFlowScopeAsDirty();
        return null;
    }

    public String onNewQuarryPermitRequestAction() {
        // Add event code here...
        this.setDynamicTaskFlowId(NaturalResourcesConstants.NEW_QUARRY_PERMIT_SUBMIT_TF);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_FROM_OUTCOME, "New Quarry Permit");
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_ACCESS_MODE,
                                           NaturalResourcesConstants.DEFAULT_ACCESS_MODE);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        ADFUtils.markPageFlowScopeAsDirty();
        return null;
    }

    public String onRenewQuarryPermitRequestAction() {
        // Add event code here...
        this.setDynamicTaskFlowId(NaturalResourcesConstants.QUARRY_PRODREPORT_SUBMIT_TF);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        ADFUtils.markPageFlowScopeAsDirty();
        return null;
    }

    public String onQuarryProductionReportAction() {
        // Add event code here...
        this.setDynamicTaskFlowId(NaturalResourcesConstants.QUARRY_PRODREPORT_SUBMIT_TF);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        ADFUtils.markPageFlowScopeAsDirty();
        return null;
    }

    public String renewQuarryPermitAction() {
        // Add event code here...
        this.setDynamicTaskFlowId(NaturalResourcesConstants.RENEW_QUARRY_PERMIT_SUBMIT_TF);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        ADFUtils.markPageFlowScopeAsDirty();
        return null;
    }

    public String submitDetonationPermitAction() {
        // Add event code here...
        this.setDynamicTaskFlowId(NaturalResourcesConstants.NEW_DETONATION_PERMIT_SUBMIT_TF);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_FROM_OUTCOME,
                                           NaturalResourcesConstants.SUBMIT_DETONATION);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_ACCESS_MODE, accessMode);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_SERVICE_TITLE, "NEW");
        ADFUtils.markPageFlowScopeAsDirty();
        return null;
    }
    public String submitRenewDetonationPermitAction() {
        // Add event code here...
        this.setDynamicTaskFlowId(NaturalResourcesConstants.NEW_DETONATION_PERMIT_SUBMIT_TF);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_FROM_OUTCOME,NaturalResourcesConstants.RENEW_DETONATION);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_ACCESS_MODE, accessMode);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_SERVICE_TITLE, "RENEW");
        
        ADFUtils.markPageFlowScopeAsDirty();
        return null;
    }

    public String submitNewDbrsPermitAction() {
        // Add event code here...
        this.setDynamicTaskFlowId(NaturalResourcesConstants.NEW_DBRS_PERMIT_SUBMIT_TF);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        ADFUtils.markPageFlowScopeAsDirty();
        return null;
    }

    public String submitModifyMoveDbrsPermitAction() {
        // Add event code here...
        this.setDynamicTaskFlowId(NaturalResourcesConstants.MODIFY_MOVE_DBRS_PERMIT_SUBMIT_TF);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        ADFUtils.markPageFlowScopeAsDirty();
        return null;
    }

    public String submitRenewMoveDbrsPermitAction() {
        // Add event code here...
        this.setDynamicTaskFlowId(NaturalResourcesConstants.RENEW_MOVE_DBRS_PERMIT_SUBMIT_TF);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        ADFUtils.markPageFlowScopeAsDirty();
        return null;
    }

    public String onSearchQuarryPermitAdmin() {
        // Add event code here...
        this.setDynamicTaskFlowId(NaturalResourcesConstants.SEARCH_QRY_PERMIT);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        ADFUtils.markPageFlowScopeAsDirty();
        return null;
    }

    public String onCrusherPermitRequestAction() {
        // Add event code here...
        this.setDynamicTaskFlowId(NaturalResourcesConstants.CRUSHER_PERMIT_SUBMIT_TF);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_FROM_OUTCOME,
                                           NaturalResourcesConstants.CRUSHER_SUBMIT);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.p_REQUEST_TYPE_ID, new BigDecimal(56));
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_ACCESS_MODE, accessMode);
        setCurrentPageTitle(NaturalResourcesConstants.CRUSHER_PERMIT_REQUEST);
        ADFUtils.markPageFlowScopeAsDirty();
        return null;
    }

    public String onRenewCrusherPermitRequestAction() {
        // Add event code here...
        this.setDynamicTaskFlowId(NaturalResourcesConstants.CRUSHER_PERMIT_SUBMIT_TF);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_FROM_OUTCOME,
                                           NaturalResourcesConstants.RENEW_CRUSHER_SUBMIT);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.p_REQUEST_TYPE_ID, new BigDecimal(58));
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_ACCESS_MODE, accessMode);
        setCurrentPageTitle(NaturalResourcesConstants.RENEW_CRUSHER_PERMIT_REQUEST);
        ADFUtils.markPageFlowScopeAsDirty();
        return null;
    }

    public String onModifyCrusherPermitRequestAction() {
        // Add event code here...
        this.setDynamicTaskFlowId(NaturalResourcesConstants.CRUSHER_PERMIT_SUBMIT_TF);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_FROM_OUTCOME,
                                           NaturalResourcesConstants.MODIFY_CRUSHER_SUBMIT);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.p_REQUEST_TYPE_ID, new BigDecimal(59));
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_ACCESS_MODE, accessMode);
        setCurrentPageTitle(NaturalResourcesConstants.MODIFY_CRUSHER_PERMIT_REQUEST);
        ADFUtils.markPageFlowScopeAsDirty();
        return null;
    }

    public String onSearchCrusherPermitsAction() {
        // Add event code here...
        setDynamicTaskFlowId(NaturalResourcesConstants.CRUSHER_PERMIT_SEARCH_TF);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_ACCESS_MODE,
                                           NaturalResourcesConstants.DEFAULT_ACCESS_MODE);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_FROM_OUTCOME,
                                           NaturalResourcesConstants.SEARCH_CRUSHER_PERMITS);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        setCurrentPageTitle("SEARCH_CRUSHER_PERMITS");
        ADFUtils.markPageFlowScopeAsDirty();
        return null;
    }

    public String onIssueCrusherPemitAction() {
        // Add event code here...
        String retVal = null;
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_FROM_OUTCOME,
                                           NaturalResourcesConstants.CRUSHER_SUBMIT);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_ACCESS_MODE,
                                           NaturalResourcesConstants.DEFAULT_ACCESS_MODE);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.p_REQUEST_TYPE_ID, new BigDecimal(56));
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        ADFUtils.markPageFlowScopeAsDirty();
        retVal = "issuecrusher";
        return retVal;
    }

    public String onModifyCrusherPemitAction() {
        // Add event code here...
        String retVal = null;
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_FROM_OUTCOME,
                                           NaturalResourcesConstants.MODIFY_CRUSHER_SUBMIT);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_ACCESS_MODE,
                                           NaturalResourcesConstants.DEFAULT_ACCESS_MODE);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.p_REQUEST_TYPE_ID, new BigDecimal(59));
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        ADFUtils.markPageFlowScopeAsDirty();
        retVal = "modifycrusher";
        return retVal;
    }

    public String onRenewCrusherPemitAction() {
        // Add event code here...
        String retVal = null;
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_FROM_OUTCOME,
                                           NaturalResourcesConstants.RENEW_CRUSHER_SUBMIT);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_ACCESS_MODE,
                                           NaturalResourcesConstants.DEFAULT_ACCESS_MODE);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.p_REQUEST_TYPE_ID, new BigDecimal(58));
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        ADFUtils.markPageFlowScopeAsDirty();
        retVal = "renewcrusher";
        return retVal;
    }

    public String onIssueDetonationPemitAction() {
        // Add event code here...
        String retVal = null;
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_FROM_OUTCOME,
                                           NaturalResourcesConstants.SUBMIT_DETONATION);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        ADFUtils.markPageFlowScopeAsDirty();
        retVal = "issuedetonation";
        return retVal;
    }

    public String onRenewDetonationPemitAction() {
        // Add event code here...
        String retVal = null;
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_FROM_OUTCOME,
                                           NaturalResourcesConstants.RENEW_DETONATION);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        ADFUtils.markPageFlowScopeAsDirty();
        retVal = "renewdetonation";
        return retVal;
    }

    public String onNewQuarryPemitAction() {
        // Add event code here...
        String retVal = null;
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_FROM_OUTCOME, "New Quarry Permit");
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        ADFUtils.markPageFlowScopeAsDirty();
        retVal = "quarrypermit";
        return retVal;
    }

    public String onRenewQuarryPemitAction() {
        // Add event code here...
        String retVal = null;
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        ADFUtils.markPageFlowScopeAsDirty();
        retVal = "renewquarrypermit";
        return retVal;
    }
    public String submitNewMoveDbrsPermitActionFront() {
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        ADFUtils.markPageFlowScopeAsDirty();
        return "newMoveDebris";
    }
    public String submitModifyMoveDbrsPermitActionFront() {
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        ADFUtils.markPageFlowScopeAsDirty();
        return "modifyMoveDebris";
    }
    
    public String submitRenewMoveDbrsPermitActionFront() {
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        ADFUtils.markPageFlowScopeAsDirty();
        return "renewMoveDebris";
    }
    public String onQuarryProductionReportActionFront() {
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        ADFUtils.markPageFlowScopeAsDirty();
        return "submitQuarryProd";
    }


    //    public String onModifyCrusherPemitAction() {
    //        // Add event code here...
    //        String retVal = null;
    //        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_FROM_OUTCOME,
    //                                           NaturalResourcesConstants.MODIFY_CRUSHER_SUBMIT);
    //        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_ACCESS_MODE,
    //                                           NaturalResourcesConstants.DEFAULT_ACCESS_MODE);
    //        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.p_REQUEST_TYPE_ID,new BigDecimal(59));
    //        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
    //        setCurrentPageTitle(NaturalResourcesConstants.MODIFY_CRUSHER_PERMIT_REQUEST);
    //        ADFUtils.markPageFlowScopeAsDirty();
    //        retVal = "modifycrusher";
    //        return retVal;
    //    }
    //
    //    public String onRenewCrusherPemitAction() {
    //        // Add event code here...
    //        String retVal = null;
    //        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_FROM_OUTCOME,
    //                                           NaturalResourcesConstants.RENEW_CRUSHER_SUBMIT);
    //        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_ACCESS_MODE,
    //                                           NaturalResourcesConstants.DEFAULT_ACCESS_MODE);
    //        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.p_REQUEST_TYPE_ID,new BigDecimal(58));
    //        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
    //        setCurrentPageTitle(NaturalResourcesConstants.RENEW_CRUSHER_PERMIT_REQUEST);
    //        ADFUtils.markPageFlowScopeAsDirty();
    //        retVal = "renewcrusher";
    //        return retVal;
    //    }
    public String onEnvDebrisInvAdmin() {
        // Add event code here...
        this.setDynamicTaskFlowId(NaturalResourcesConstants.ENV_DEBRIS_ADMIN);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        ADFUtils.markPageFlowScopeAsDirty();
        return null;
    }


    public String onIssueFalconPassportAction() {
        this.setDynamicTaskFlowId(NaturalResourcesConstants.FALCON_PASSPORT_SUBMIT_TF);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_FROM_OUTCOME,
                                           NaturalResourcesConstants.CRUSHER_SUBMIT);
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.p_REQUEST_TYPE_ID, new BigDecimal(56));
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_TIME_STAMP, this.getCurrentTimeStamp());
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_ACCESS_MODE, accessMode);
       // setCurrentPageTitle(NaturalResourcesConstants.CRUSHER_PERMIT_REQUEST);
        ADFUtils.markPageFlowScopeAsDirty();
        return null;
    }
}
