package qa.gov.mme.envservices.naturalresources.view.beans.falconpassport;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;

import oracle.jbo.Row;

import qa.gov.mme.common.beans.CommonBaseBean;
import qa.gov.mme.common.utils.ADFUtils;
import qa.gov.mme.component.model.view.readonly.CmnBeneficiaryTVORowImpl;
import qa.gov.mme.envservices.naturalresources.view.utils.NaturalResourcesUtils;

public class FalconPassportBean  extends CommonBaseBean{
    @SuppressWarnings("compatibility:-7817286963497314807")
    private static final long serialVersionUID = 1L;

    public FalconPassportBean() {
        super();
    }
    
    public List<BigDecimal> setEntityTypeIdList() {
        List<BigDecimal> attachmentList = null;
        attachmentList = new ArrayList<BigDecimal>();
        attachmentList.add(new BigDecimal(1));
        return attachmentList;
    }
    
    public String onValidateApplicantDetailsAction() {
        System.out.println("====================validateat "+ADFUtils.getValueFrmExpression("#{'APPLICANT'}"));
        String retVal = null;
        //ADFUtils.executeOperationBinding("validateApplicantFormAndData");
        ADFUtils.putPageFlowScopeAttribute("pEntityTypeIdList", this.setEntityTypeIdList());
        ADFUtils.markPageFlowScopeAsDirty();
        return retVal;
    }
    public String onValidateBeneficiaryDetailsAction() {
        System.out.println(" Validating beneficiery");
        String retVal = null;
//        CmnBeneficiaryTVORowImpl cmnBeneficiaryTVORowImpl = null;
//        cmnBeneficiaryTVORowImpl = (CmnBeneficiaryTVORowImpl) ADFUtils.findIterator("CmnBeneficiaryTVO1Iterator")
//                                                                      .getViewObject()
//                                                                      .first();
//        if (null != cmnBeneficiaryTVORowImpl && null != cmnBeneficiaryTVORowImpl.getEntityTypeIdT()) {
//            if (!(new BigDecimal(2).equals(cmnBeneficiaryTVORowImpl.getEntityTypeIdT()) &&
//                  ("GOVERNMENT".equals(cmnBeneficiaryTVORowImpl.getSectorDescEnT())))) {
//
//                Row compRow = null;
//                compRow = (Row) ADFUtils.executeOperationBinding("doFilterRegisterCompanies");
//                System.out.println("" + ADFUtils.getBoundAttributeValue("EIDT"));
//                if (null == compRow) {
//                    ADFUtils.setBoundAttributeValue("IsGovtEstablishmentT", "N");
//                    NaturalResourcesUtils.showErrorMessageFromResourceBundle("BENEFICIARY_GOVT_EST_REG_COMPANY_MSG");
//                } else {
//                    retVal = "validateBeneficiaryData";
//                }
//            } else {
//                ADFUtils.executeOperationBinding("validateBeneficiaryFormAndData");
//                retVal = "validateBeneficiaryData";
//                ADFUtils.setBoundAttributeValue("IsGovtEstablishmentT", "Y");
//            }
//        }
        return retVal;
    }

    public void searchCitePermitDetailACE(ActionEvent actionEvent) {
        ADFUtils.executeOperationBinding("doFilterCitePermitDetails");
    }

    public void clearCitePermitDetailsACE(ActionEvent actionEvent) {
        ADFUtils.executeOperationBinding("clearCitePermitDetails");
    }
}
