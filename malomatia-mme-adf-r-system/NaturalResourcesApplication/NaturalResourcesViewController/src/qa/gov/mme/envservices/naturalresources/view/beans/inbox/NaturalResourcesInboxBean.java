package qa.gov.mme.envservices.naturalresources.view.beans.inbox;

import com.google.common.base.Predicate;

import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;

import oracle.binding.OperationBinding;

import qa.gov.mme.common.beans.SearchTemplateBean;
import qa.gov.mme.common.utils.ADFUtils;
import qa.gov.mme.common.utils.UIUtils;
import qa.gov.mme.common.ws.proxy.client.BpmSoapServiceClient;
import qa.gov.mme.common.ws.proxy.client.MmeTaskInstance;
import qa.gov.mme.envservices.naturalresources.view.constant.NaturalResourcesConstants;
import qa.gov.mme.envservices.naturalresources.view.utils.NaturalResourcesUtils;

public class NaturalResourcesInboxBean extends SearchTemplateBean implements Serializable {
    @SuppressWarnings("compatibility:1925160598456472028")
    private static final long serialVersionUID = 1L;

    public NaturalResourcesInboxBean() {
        super();
    }
    private List<MmeTaskInstance> tasksList = null;
    
    
    private String reqType;    

    public void setTasksList(List<MmeTaskInstance> tasksList) {
        this.tasksList = tasksList;
    }

    public List<MmeTaskInstance> getTasksList() {
        return tasksList;
       
    }
    
    

    public void initInbox() {
        try {
            String actionOutCome = null;
            List<MmeTaskInstance> tasksList = null;
            actionOutCome = (String) ADFUtils.getPageFlowScopeAttribute("pFromOutcome");
            if (NaturalResourcesConstants.PRIVATE_INBOX_PARAM.equalsIgnoreCase(actionOutCome)) {
                tasksList = getMyBPMTasks();
            } else if (NaturalResourcesConstants.PUBLIC_INBOX_PARAM.equalsIgnoreCase(actionOutCome)) {
                tasksList = getBPMTasks();
            } else if (NaturalResourcesConstants.TASKS_ASSIGN_PARAM.equalsIgnoreCase(actionOutCome)) {
                tasksList = getAllAcqiuredByTasks();
            }
            this.setTasksList(tasksList);
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }
    }

    private List<MmeTaskInstance> getBPMTasksList() {
        String actionOutCome = null;
        List<MmeTaskInstance> tasksList = null;
        try {
            actionOutCome = (String) ADFUtils.getPageFlowScopeAttribute("pFromOutcome");
            if (NaturalResourcesConstants.PRIVATE_INBOX_PARAM.equalsIgnoreCase(actionOutCome)) {
                tasksList = getMyBPMTasks();
            } else if (NaturalResourcesConstants.PUBLIC_INBOX_PARAM.equalsIgnoreCase(actionOutCome)) {
                tasksList = getBPMTasks();
            } else if (NaturalResourcesConstants.TASKS_ASSIGN_PARAM.equalsIgnoreCase(actionOutCome)) {
                tasksList = getAllAcqiuredByTasks();
            }
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }
        this.setTasksList(tasksList);
        return tasksList;
    }

    /**
     * This method is used for getting all task list.
     */
    private List<MmeTaskInstance> getBPMTasks() {
        String lcReqType = null;
        BpmSoapServiceClient bpmSoapServiceClient = null;
        bpmSoapServiceClient = new BpmSoapServiceClient();
        lcReqType = (String) ADFUtils.getViewScopeAttribute("pInboxRequestType");
        List<MmeTaskInstance> tasksList = null;
        if (null != lcReqType) {
            tasksList = bpmSoapServiceClient.retrieveUserInboxTasks(this.getLoggedInUser(), lcReqType);
        } else {
            tasksList =
                bpmSoapServiceClient.retrieveUserInboxTasks(this.getLoggedInUser(),
                                                            NaturalResourcesConstants.NATURAL_RESOURCES_PRIV_PROCESS_DEV);
        }
        return tasksList;
    }

    /**
     * This method is used for getting My task list
     */
    private List<MmeTaskInstance> getMyBPMTasks() {
        String lcReqType = null;
        BpmSoapServiceClient bpmSoapServiceClient = null;
        bpmSoapServiceClient = new BpmSoapServiceClient();
        lcReqType = (String) ADFUtils.getViewScopeAttribute("pInboxRequestType");
        List<MmeTaskInstance> myTasksList = null;
        if (null != lcReqType) {
            myTasksList = bpmSoapServiceClient.retrieveUserMyTasks(this.getLoggedInUser(), null);
        } else {
            myTasksList =
                bpmSoapServiceClient.retrieveUserMyTasks(this.getLoggedInUser(),
                                                         NaturalResourcesConstants.NATURAL_RESOURCES_PRIV_PROCESS_DEV);
        }
        System.out.println("myTasksList size:::" + myTasksList.size());
        return myTasksList;
    }

    /**
     * Get logged in user details
     */
    private String getLoggedInUser() {
        String user = null;
        user = ADFUtils.getCurrentUserUsername();
        return user;
    }


    private void resetFilter() {
        try {
            RichInputText req = (RichInputText) UIUtils.findComponentInRoot("reqNoInp");
            RichInputText aplcnt = (RichInputText) UIUtils.findComponentInRoot("aplInp");
            RichSelectOneChoice reqType = (RichSelectOneChoice) UIUtils.findComponentInRoot("reqType");
            req.resetValue();
            aplcnt.resetValue();
            reqType.resetValue();
            initInbox();
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }
    }

    public void claimTaskActionListener(ActionEvent actionEvent) {

        // Add event code here...
        String taskId = null;
        try {
            Boolean status = false;
            taskId = (String) actionEvent.getComponent()
                                         .getAttributes()
                                         .get("TaskId");
            System.out.println("Task id is:::" + taskId);
            if (null != taskId) {
                status = isTaskAssignToUser(this.getLoggedInUser(), taskId);
                NaturalResourcesUtils.showInfoMessageFromResourceBundle("REQUEST_ASSIGNED_SUCCESS_MSG");
                if (status)
                    getBPMTasksList();
            }
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }
    }


    /**
     * BPM service call to update claimed task.
     */
    private boolean isTaskAssignToUser(String userId, String taskId) {
        Boolean isAssigned = false;
        BpmSoapServiceClient bpmSoapServiceClient = null;
        bpmSoapServiceClient = new BpmSoapServiceClient();
        isAssigned = bpmSoapServiceClient.claimTask(userId, taskId);
        return isAssigned;
    }


    public void btnReleaseTask_action(ActionEvent actionEvent) {
        String userId = (String) actionEvent.getComponent()
                                            .getAttributes()
                                            .get("UserId");
        String taskId = (String) actionEvent.getComponent()
                                            .getAttributes()
                                            .get("TaskId");
        Boolean status = releaseTask(userId, taskId);
        NaturalResourcesUtils.showInfoMessageFromResourceBundle("ReleasedTaskMsg");
        if (status) {
            reloadAllAcquiredTasks();
        }
    }

    private void reloadAllAcquiredTasks() {
        List<MmeTaskInstance> tasksList = getAllAcqiuredByTasks();
        this.setTasksList(tasksList);
    }

    /**
     * BPM service call to release task.
     */
    private boolean releaseTask(String userId, String taskId) {
        Boolean isReleased = false;
        BpmSoapServiceClient bpmSoapServiceClient = null;
        bpmSoapServiceClient = new BpmSoapServiceClient();
        isReleased = bpmSoapServiceClient.releaseTask(userId, taskId);
        return isReleased;
    }

    /**
     * BPM service call to release task.
     */
    private boolean reassignTask(String userId, String taskId, String assignTo) {
        Boolean isReleased = false;
        BpmSoapServiceClient bpmSoapServiceClient = null;
        bpmSoapServiceClient = new BpmSoapServiceClient();
        isReleased = bpmSoapServiceClient.reassignTask(userId, taskId, assignTo);
        return isReleased;
    }

    public void reassignTaskOkListener(ActionEvent actionEvent) {
        String taskId = (String) ADFUtils.getPageFlowScopeAttribute("reassignTaskId");
        String userId = (String) ADFUtils.getPageFlowScopeAttribute("reassignAcquiredBy");
        // Release the task
        Boolean status = releaseTask(userId, taskId);
        if (status) {
            // Reassign the task
            status = reassignTask(userId, taskId, "");
            RichPopup reassignPopupBnd =
                (RichPopup) UIUtils.findComponentInRoot(NaturalResourcesConstants.REASSIGN_POPUP_ID);
            if (null != reassignPopupBnd) {
                reassignPopupBnd.hide();
            }
            NaturalResourcesUtils.showInfoMessageFromResourceBundle("reassignTaskMsg");
            if (status) {
                reloadAllAcquiredTasks();
            }
        }
    }

    /**
     * This method is used for getting all acquired by task list
     */
    private List<MmeTaskInstance> getAllAcqiuredByTasks() {
        BpmSoapServiceClient bpmSoapServiceClient = null;
        bpmSoapServiceClient = new BpmSoapServiceClient();
        String requestType = null;
        List<MmeTaskInstance> acqiuredByTasks = null;
        if (requestType != null) {
            acqiuredByTasks = bpmSoapServiceClient.retrieveAllAcquiredByTasks(this.getLoggedInUser(), requestType);
        } else {
            acqiuredByTasks =
                bpmSoapServiceClient.retrieveAllAcquiredByTasks(this.getLoggedInUser(),
                                                                NaturalResourcesConstants.NATURAL_RESOURCES_PRIV_PROCESS_DEV);
        }
        //        this.sortTaskInstances(acqiuredByTasks);
        //initialize waiting time
        LocalDateTime now = LocalDateTime.now();


        for (MmeTaskInstance t : acqiuredByTasks) {
            if (null != t.getUpdateDate()) {
                Date date = new Date();
                LocalDateTime ldt = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
                Duration duration = Duration.between(now, ldt);
                t.setPriority("" + Math.abs(duration.toHours()));
            }
            List<String> nameList = this.getNamesFromVC(t.getAcquiredBy());
            if (nameList.size() > 1) {

                t.setAcquiredBy(nameList.get(1));
            }
        }
        return acqiuredByTasks;
        //        this.setTaskList(acqiuredByTasks);

    }

    private List<String> getNamesFromVC(String qid) {
        OperationBinding operBnd = ADFUtils.findOperation("findNameByQid");
        operBnd.getParamsMap().put("qid", qid);
        return (List) operBnd.execute();
    }

    public final class RequestNumberOrAppNameFilter implements Predicate<MmeTaskInstance> {
        private final String beneficiaryName;
        private final String requestNo;
        private final String requestType;

        public RequestNumberOrAppNameFilter(final String requestNo, final String beneficiaryName,
                                            final String requestType) {
            this.requestNo = requestNo;
            this.beneficiaryName = beneficiaryName;
            this.requestType = requestType;
        }

        @Override
        public boolean apply(final MmeTaskInstance mmeTaskInstance) {
            if (requestNo != null && !requestNo.isEmpty() && beneficiaryName != null && !beneficiaryName.isEmpty() &&
                requestType != null && !requestType.isEmpty()) {
                if ((mmeTaskInstance.getReqNumber() != null && mmeTaskInstance.getReqNumber().contains(requestNo)) &&
                    (mmeTaskInstance.getReqBeneficiaryName() != null &&
                     mmeTaskInstance.getReqBeneficiaryName().contains(beneficiaryName)) &&
                    (mmeTaskInstance.getProcessName() != null &&
                     mmeTaskInstance.getProcessName().contains(requestType))) {
                    return true;
                }
            } else if (requestNo != null && !requestNo.isEmpty() && requestType != null && !requestType.isEmpty()) {
                if ((mmeTaskInstance.getReqNumber() != null && mmeTaskInstance.getReqNumber().contains(requestNo)) &&
                    (mmeTaskInstance.getProcessName() != null &&
                     mmeTaskInstance.getProcessName().contains(requestType))) {
                    return true;
                }
            } else if (beneficiaryName != null && !beneficiaryName.isEmpty() && requestType != null &&
                       !requestType.isEmpty()) {
                if ((mmeTaskInstance.getReqBeneficiaryName() != null &&
                     mmeTaskInstance.getReqBeneficiaryName().contains(beneficiaryName)) &&
                    (mmeTaskInstance.getProcessName() != null &&
                     mmeTaskInstance.getProcessName().contains(requestType))) {
                    return true;
                }
            } else if (requestNo != null && !requestNo.isEmpty() && beneficiaryName != null && !beneficiaryName.isEmpty()) {
                if ((mmeTaskInstance.getReqNumber() != null && mmeTaskInstance.getReqNumber().contains(requestNo)) &&
                    (mmeTaskInstance.getReqBeneficiaryName() != null &&
                     mmeTaskInstance.getReqBeneficiaryName().contains(beneficiaryName))) {
                    return true;
                }
            } else {
                if ((requestNo == null || requestNo.isEmpty()) && (beneficiaryName == null || beneficiaryName.isEmpty()) &&
                    (requestType == null || requestType.isEmpty())) {
                    return true;
                } else if (requestNo != null && !requestNo.isEmpty() &&
                           (mmeTaskInstance.getReqNumber() != null &&
                            mmeTaskInstance.getReqNumber().contains(requestNo))) {
                    return true;
                } else if ((beneficiaryName != null) && !beneficiaryName.isEmpty() &&
                           (mmeTaskInstance.getReqBeneficiaryName() != null &&
                            mmeTaskInstance.getReqBeneficiaryName().contains(beneficiaryName))) {
                    return true;
                } else if ((requestType != null) && !requestType.isEmpty() &&
                           (mmeTaskInstance.getProcessName() != null &&
                            mmeTaskInstance.getProcessName().contains(requestType))) {
                    return true;
                }
            }
            return false;
        }
    }


    public void onInboxSearchActionListener(ActionEvent actionEvent) {
        // Add event code here...
        System.out.println("Calling Search");
        try {
            String lcReqNo = null;
            String beneficiaryName = null;
            String lcReqType = null;
            lcReqNo = (String) ADFUtils.getViewScopeAttribute("pRequestNo");
            beneficiaryName = (String) ADFUtils.getViewScopeAttribute("pBeneficiaryName");
            lcReqType = (String) ADFUtils.getViewScopeAttribute("pInboxRequestType");
            List<MmeTaskInstance> existTasksList = null;
            existTasksList = getBPMTasksList();
            List<MmeTaskInstance> filteredTasksList = null;
            if (null != lcReqNo || null != beneficiaryName || null != lcReqType) {
                filteredTasksList =
                    Lists.newArrayList(Collections2.filter(existTasksList,
                                                           new RequestNumberOrAppNameFilter(lcReqNo, beneficiaryName,
                                                                                            lcReqType)));
                this.setTasksList(filteredTasksList);

            }
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }
        reRenderComponents(new String[] { "t1" });
    }


    public void onInboxClearSearchActionListener(ActionEvent actionEvent) {
        // Add event code here...
        ADFUtils.putViewScopeAttribute("pInboxRequestType", null);
        initInbox();
        reRenderComponents(new String[] { "t1" });

    }


}
