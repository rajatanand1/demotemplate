package qa.gov.mme.envservices.naturalresources.view.beans.login;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;


import javax.security.auth.Subject;
import javax.security.auth.callback.CallbackHandler;

import javax.security.auth.login.FailedLoginException;

import javax.security.auth.login.LoginException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import qa.gov.mme.common.utils.ADFUtils;
import qa.gov.mme.envservices.naturalresources.view.utils.NaturalResourcesUtils;

import weblogic.security.URLCallbackHandler;

import weblogic.security.services.Authentication;

import weblogic.servlet.security.ServletAuthentication;

public class LoginBean {
    public LoginBean() {
    }

    public void onLoginActionListener(ActionEvent actionEvent) {
        // Add event code here...
        FacesContext fctx = FacesContext.getCurrentInstance();
        ExternalContext ectx = fctx.getExternalContext();
        HttpServletRequest request = (HttpServletRequest) ectx.getRequest();
        String lcLoginUserName = null;
        String lcPassword = null;
        String SUCCESS_URL = "/faces/Home";
        lcLoginUserName = (String) ADFUtils.getViewScopeAttribute("loginUserName");
        lcPassword = (String) ADFUtils.getViewScopeAttribute("loginPassword");
        if ((lcLoginUserName == null || lcLoginUserName.trim().length() == 0)) {
            NaturalResourcesUtils.showErrorMessageFromResourceBundle("USER_NAME_MSG");

            return;
        } else if (lcPassword == null || lcPassword.trim().length() == 0) {
            NaturalResourcesUtils.showErrorMessageFromResourceBundle("PASSWORD_MSG");
            return;
        }
        try {
            request.setCharacterEncoding("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        CallbackHandler handler = new URLCallbackHandler(lcLoginUserName.toLowerCase(), lcPassword.getBytes());
        try {
            // Get the authenticator Subject which has all the user login information
            Subject mySubject = null;
            mySubject = Authentication.login(handler);
            ServletAuthentication.runAs(mySubject, request);
            ServletAuthentication.generateNewSessionID(request);
            String loginUrl = "/adfAuthentication?success_url=" + SUCCESS_URL;
            HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance()
                                                                             .getExternalContext()
                                                                             .getResponse();
            sendForward(request, response, loginUrl);

        } catch (FailedLoginException fle) {
            NaturalResourcesUtils.showErrorMessageFromResourceBundle("WRONG_USER_PASSWORD");
            ADFUtils.putViewScopeAttribute("pPassword", null);
            ADFUtils.markViewScopeAsDirty();
        } catch (LoginException le) {
            le.printStackTrace();
        }
        return;

    }

    /**
     * Helper method to redirect user to Home page upon successful authentication.
     * @param request
     * @param response
     * @param forwardUrl
     */
    private void sendForward(HttpServletRequest request, HttpServletResponse response, String forwardUrl) {
        FacesContext ctx = FacesContext.getCurrentInstance();
        RequestDispatcher dispatcher = request.getRequestDispatcher(forwardUrl);
        try {
            dispatcher.forward(request, response);
        } catch (ServletException se) {
            se.printStackTrace();
        } catch (IOException ie) {
            ie.printStackTrace();
        }
        ctx.responseComplete();
    }
}
