package qa.gov.mme.envservices.naturalresources.view.beans.viewrequestdetails;

import java.io.Serializable;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.controller.TaskFlowId;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.jbo.ApplicationModule;
import oracle.jbo.Row;
import oracle.jbo.ViewObject;

import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;

import qa.gov.mme.common.constants.MMEConstants;
import qa.gov.mme.common.utils.ADFUtils;
import qa.gov.mme.common.utils.EncryptionUtils;
import qa.gov.mme.common.utils.JSFUtils;
import qa.gov.mme.common.utils.ReportsUtils;
import qa.gov.mme.common.utils.UIUtils;
import qa.gov.mme.component.view.beans.utils.CustomComponentsMiscUtils;
import qa.gov.mme.component.view.beans.viewreq.ViewRequestBaseBean;
import qa.gov.mme.component.view.constant.CustomComponentConstants;
import qa.gov.mme.envservices.naturalresources.view.constant.NaturalResourcesConstants;
import qa.gov.mme.envservices.naturalresources.view.utils.NaturalResourcesUtils;

public class ViewRequestDetailsBean extends ViewRequestBaseBean implements Serializable {
    @SuppressWarnings("compatibility:-3310579555742683518")
    private static final long serialVersionUID = 1L;
    private static final String IS_INSPECT_REQ_ATTR = "IsInspectionRequired";
    private static final String NEW_MOVEMENT_TYPE_ATTR = "MovementTypeId";
    private static final String RENEW_MOVEMENT_TYPE_ATTR = "MovementTypeId2";
    private static final String MODIFY_MOVEMENT_TYPE_ATTR = "MovementTypeId1";
    private String taskFlowId = NaturalResourcesConstants.NONE_TF;
    private String pComments;
    private String previousRequestTaskFlowId = NaturalResourcesConstants.NONE_TF;
    private String permitsTaskFlowId = NaturalResourcesConstants.NONE_TF;
    private String oldPermitsTaskFlowId = NaturalResourcesConstants.NONE_TF;
    private String otherContentTaskFlowId = NaturalResourcesConstants.NONE_TF;

    public ViewRequestDetailsBean() {
    }

    /**
     * Initialize method to configure request details page
     *
     */

    public void init() {
        Row requestRow = null;
        requestRow = (Row) ADFUtils.executeOperationBinding(NaturalResourcesConstants.DO_FILTER_REQ_ID_NO);
        if (null != requestRow) {
            ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_REQUEST_ID,
                                               requestRow.getAttribute("RequestId"));
            this.renderInspectionTab();

            System.out.println("Application Id" + NaturalResourcesUtils.getMessageFromResourceBundle("APPLICATION_ID"));
            System.out.println("REQ id:" + ADFUtils.getPageFlowScopeAttribute(NaturalResourcesConstants.P_REQUEST_ID));
            System.out.println("***Task Definition:" +
                               ADFUtils.getPageFlowScopeAttribute(NaturalResourcesConstants.P_TASK_DEFINITION));
            System.out.println("***Task Id:" + ADFUtils.getPageFlowScopeAttribute(NaturalResourcesConstants.P_TASK_ID));
            ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_COMP_INS_ID,
                                               requestRow.getAttribute(NaturalResourcesConstants.BPM_PROC_INS_ID));
            ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_REQ_TYPE,
                                               requestRow.getAttribute("RequestTypeId"));
            ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_REQ_STAT,
                                               requestRow.getAttribute("StatusId"));
            ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.BENEFICIARY_ID,
                                               requestRow.getAttribute("BeneficiaryId"));
            if (ADFUtils.getPageFlowScopeAttribute(NaturalResourcesConstants.P_REQUEST_NO) == null)
                ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_REQUEST_NO,
                                                   requestRow.getAttribute("RequestNo"));

            processRequestDetails();
            this.populateBeneficiaryPreviousRequestsTab((BigDecimal) requestRow.getAttribute("RequestTypeId"));
            
            if (null != requestRow.getAttribute("StatusId") &&
                new BigDecimal(7).equals(requestRow.getAttribute("StatusId"))) {
                this.populatePermitsTab((BigDecimal) requestRow.getAttribute("RequestTypeId"));
            }
            runJavaScriptCode("if($(\"body\").hasClass(\"javascriptLoaded\")||($(\".main-menu  ul  li  div\").each(function(){var e=$(this).closest(\"li\").children(\"a\").find(\".label\");e.append('<i class=\"fas fa-chevron-down\"></i>'),e.closest(\"li\").addClass(\"has-sub-menu\")}),$(\".main-menu > ul  li.has-sub-menu > a\").click(function(e){e.preventDefault(),$(this).closest(\".active\").removeClass(\"active\"),$(this).children(\"span\").find(\".fa-chevron-up\").length&&$(this).children(\"span\").find(\".fa-chevron-up\").removeClass(\"fa-chevron-up\").addClass(\"fa-chevron-down\");$(this).parent(\"li\").children(\"div\").children(\"ul:visible\");if($(this).parent(\"li\").children(\"div\").children(\"ul:visible\").first().slideUp(200),$(this).addClass(\"active\"),$(this).closest(\"li\").hasClass(\"has-sub-menu\")){$(this).closest(\"li\").children(\"a\").find(\"i\").removeClass(\"fa-chevron-down\").addClass(\"fa-chevron-up\");var a=$(this).closest(\"li\").find(\"ul\");a.is(\":visible\")?(a.first().slideUp(200),$(this).closest(\"li\").find(\"i\").removeClass(\"fa-chevron-up\").addClass(\"fa-chevron-down\")):a.first().slideDown(200)}return!1}),$(\".menu-toggel\").click(function(){$(\"body\").hasClass(\"menu-close\")?$(\"body\").removeClass(\"menu-close\"):$(\"body\").addClass(\"menu-close\"),setTimeout(function(){window.dispatchEvent(new Event(\"resize\"))},400)}),console.log(\"javascriptLoaded\"),$(\"body\").addClass(\"javascriptLoaded\")),$(\".navbar-header > span.tab-header\").length&&!$(\".navbar-header > span.tab-header\").hasClass(\"javascriptTabLoaded\")){var head,content,animateTime=250;$(\".navbar-header > span.tab-header\").click(function(){var e,a,s,n;(head=$(this)).hasClass(\"opened\")?(head.removeClass(\"opened\"),head.next(\"span.tab-content\").stop().animate({height:\"0\"},animateTime)):(content=$(this).next(\"span.tab-content\"),$(\"span.tab-header\").each(function(e){var a=$(this);console.log(a.hasClass(\"opened\")),a.hasClass(\"opened\")&&(a.removeClass(\"opened\"),a.next(\"span.tab-content\").stop().animate({height:\"0\"},animateTime))}),head.addClass(\"opened\"),a=animateTime,s=(e=content).height(),n=e.css(\"height\",\"auto\").height(),e.height(s),e.stop().animate({height:n},a,function(){window.dispatchEvent(new Event(\"resize\"))}),setTimeout(function(){$(\"html, body\").animate({scrollTop:head.offset().top},animateTime)},animateTime))}),$(\".navbar-header > span.tab-header\").addClass(\"javascriptTabLoaded\")}");
        } else {
            ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_REQUEST_ID, null);
            CustomComponentsMiscUtils.showInfoMessageFromResourceBundle(NaturalResourcesConstants.INVALID_REQUEST);
        }
        ADFUtils.markPageFlowScopeAsDirty();

    }

    private void processRequestDetails() {
        try {
            if (ADFUtils.getPageFlowScopeAttribute(NaturalResourcesConstants.P_REQ_TYPE) != null) {
                BigDecimal typeId =
                    (BigDecimal) ADFUtils.getPageFlowScopeAttribute(NaturalResourcesConstants.P_REQ_TYPE);
                String taskdef =
                    (String) ADFUtils.getPageFlowScopeAttribute(NaturalResourcesConstants.P_TASK_DEFINITION);
                String outcome = (String) ADFUtils.getPageFlowScopeAttribute(NaturalResourcesConstants.P_FROM_OUTCOME);
                if (typeId.compareTo(new BigDecimal("44")) == 0) {
                    setDynamicTaskFlowId(NaturalResourcesConstants.NEW_QRY_PERMIT_REQ_DET);
                } else if ((typeId.compareTo(new BigDecimal("51")) == 0) ||
                           (typeId.compareTo(new BigDecimal("52")) == 0) ||
                           (typeId.compareTo(new BigDecimal("53")) == 0)) {
                    setDynamicTaskFlowId(NaturalResourcesConstants.MOVE_DEBRIS_PERMIT_REQ_DET);
                } else if (typeId.compareTo(new BigDecimal("43")) == 0) {
                    setDynamicTaskFlowId(NaturalResourcesConstants.QRY_RPT_REQ_DET);
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.REPORT_TYPE,
                                                       NaturalResourcesConstants.QUARRY_RPT_CODE);
                } else if (typeId.compareTo(new BigDecimal("45")) == 0) {
                    setDynamicTaskFlowId(NaturalResourcesConstants.RENEW_QRY_PERMIT_REQ_DET);
                } else if (typeId.compareTo(new BigDecimal("54")) == 0 || typeId.compareTo(new BigDecimal("55")) == 0) {
                    
                    setDynamicTaskFlowId(NaturalResourcesConstants.DETONATION_PERMIT_REQ_DET);
                    if(typeId.compareTo(new BigDecimal("55")) == 0){
                        populateOldPermitsTab(typeId);
                    }
                } else if ((typeId.compareTo(new BigDecimal("56")) == 0) ||
                           (typeId.compareTo(new BigDecimal("58")) == 0) ||
                           (typeId.compareTo(new BigDecimal("59")) == 0)) {
                    setDynamicTaskFlowId(NaturalResourcesConstants.CRUSHER_PERMIT_DETAILS_TF);
                }

                if (NaturalResourcesConstants.PRIVATE_INBOX_PARAM.equals(outcome)) {
                    evaluateButtonRenderConditions(typeId, taskdef);
                }

            }
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }
    }


    /**
     * Go to the specific request type and change the button flag values if you want to make any change to the button
     * render conditions
     * @param typeId
     * @param taskDef
     */
    public void evaluateButtonRenderConditions(BigDecimal typeId, String taskDef) {
        try {
            System.out.println("typeId ::" + typeId);
            System.out.println("taskDef ::" + taskDef);

            ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_APPROVE, NaturalResourcesConstants.CHAR_Y);
            ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_REJECT, NaturalResourcesConstants.CHAR_Y);
            ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_ACCEPT, NaturalResourcesConstants.CHAR_N);
            ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_RETURN, NaturalResourcesConstants.CHAR_N);
            ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_INSPECTION,
                                               NaturalResourcesConstants.CHAR_N);
            ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_SAVE, NaturalResourcesConstants.CHAR_N);
            ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_SHOW_FEES, NaturalResourcesConstants.CHAR_N);
            ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_INSPECTION_CHECK_BOX,
                                               NaturalResourcesConstants.CHAR_N);

            if (typeId.compareTo(new BigDecimal("54")) == 0 || typeId.compareTo(new BigDecimal("55")) == 0) {
                if (NaturalResourcesConstants.SELECT_INSPECT_TASK.equals(taskDef) ||
                    NaturalResourcesConstants.INSPECT_LOCATE_TASK.equals(taskDef) ||
                    NaturalResourcesConstants.REVIEW_INSPECT_TASK.equals(taskDef) ||
                    NaturalResourcesConstants.ATTST_INSPECT_RPT_TASK.equals(taskDef)) {
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_APPROVE, "N");
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_REJECT, "N");
                } else if (NaturalResourcesConstants.VALIDATE_REQ_TASK.equals(taskDef)) {
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_REJECT, "Y");
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_RETURN, "Y");
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_APPROVE, "Y");

                } else if (NaturalResourcesConstants.REVIEW_REQUEST_TASK.equals(taskDef)) {
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_INSPECTION_CHECK_BOX,
                                                       NaturalResourcesConstants.CHAR_Y);
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_SAVE, "Y");
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_REJECT, "Y");
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_RETURN, "Y");

                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_APPROVE, "Y");
                    //   ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_INSPECTION, "Y");
                } else if (NaturalResourcesConstants.APPROVE_REQ_TASK.equals(taskDef) ||
                           NaturalResourcesConstants.ATTEST_REQ_TASK.equals(taskDef)) {
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_REJECT, "Y");
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_RETURN, "N");

                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_APPROVE, "Y");
                }
            }


            if (typeId.compareTo(new BigDecimal("44")) == 0) {
                if (NaturalResourcesConstants.VALIDATE_REQ_TASK.equals(taskDef) ||
                    NaturalResourcesConstants.REVIEW_REQUEST_TASK.equals(taskDef)) {
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_RETURN, "Y");
                }
                if (NaturalResourcesConstants.UPDATE_REQ_TASK.equals(taskDef)) {
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_REJECT, "N");
                }
                if (NaturalResourcesConstants.REVIEW_REQUEST_TASK.equals(taskDef)) {
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_INSPECTION, "Y");
                }
                if (NaturalResourcesConstants.SELECT_INSPECT_TASK.equals(taskDef) ||
                    NaturalResourcesConstants.INSPECT_LOCATE_TASK.equals(taskDef) ||
                    NaturalResourcesConstants.REVIEW_INSPECT_TASK.equals(taskDef) ||
                    NaturalResourcesConstants.ATTST_INSPECT_RPT_TASK.equals(taskDef)) {
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_APPROVE, "N");
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_REJECT, "N");
                }
                if (NaturalResourcesConstants.ATTEST_REQ_TASK.equals(taskDef)) {
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_RETURN,
                                                       NaturalResourcesConstants.CHAR_Y);
                }
            } else if (typeId.compareTo(new BigDecimal("43")) == 0) {
                ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_SHOW_FEES,
                                                   NaturalResourcesConstants.CHAR_N);
                if (NaturalResourcesConstants.UPDATE_REQ_TASK.equals(taskDef)) {
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_REJECT,
                                                       NaturalResourcesConstants.CHAR_N);
                }

                if (NaturalResourcesConstants.APPROVE_REQ_TASK.equals(taskDef)) {
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_RETURN,
                                                       NaturalResourcesConstants.CHAR_Y);
                }
            } else if ((typeId.compareTo(new BigDecimal("51")) == 0) || (typeId.compareTo(new BigDecimal("52")) == 0) ||
                       (typeId.compareTo(new BigDecimal("53")) == 0)) {
                if (NaturalResourcesConstants.VALIDATE_REQ_TASK.equals(taskDef) ||
                    NaturalResourcesConstants.REVIEW_REQUEST_TASK.equals(taskDef)) {
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_RETURN, "Y");
                }
                if (NaturalResourcesConstants.UPDATE_REQ_TASK.equals(taskDef)) {
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_REJECT, "N");
                }
                if (NaturalResourcesConstants.REVIEW_REQUEST_TASK.equals(taskDef)) {
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_INSPECTION, "Y");
                }
                if (NaturalResourcesConstants.SELECT_INSPECT_TASK.equals(taskDef) ||
                    NaturalResourcesConstants.INSPECT_LOCATE_TASK.equals(taskDef) ||
                    NaturalResourcesConstants.REVIEW_INSPECT_TASK.equals(taskDef) ||
                    NaturalResourcesConstants.ATTST_INSPECT_RPT_TASK.equals(taskDef)) {
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_APPROVE, "N");
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_REJECT, "N");
                }
                if (NaturalResourcesConstants.ATTEST_REQ_TASK.equals(taskDef)) {
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_RETURN,
                                                       NaturalResourcesConstants.CHAR_Y);
                }
            } else if (typeId.compareTo(new BigDecimal("45")) == 0) {
                System.out.println("taskDef inside 45 is :::" + taskDef);
                if (NaturalResourcesConstants.UPDATE_REQ_TASK.equals(taskDef)) {
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_REJECT, "N");
                }
                if (NaturalResourcesConstants.VALIDATE_REQ_TASK.equals(taskDef)) {
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_RETURN, "Y");
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_APPROVE, "Y");
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_REJECT, "Y");
                }
                if (NaturalResourcesConstants.REVIEW_REQUEST_TASK.equals(taskDef)) {
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_RETURN, "Y");
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_APPROVE, "Y");
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_REJECT, "Y");
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_INSPECTION, "Y");
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_SAVE,
                                                       NaturalResourcesConstants.CHAR_Y);
                }
                if (NaturalResourcesConstants.SELECT_INSPECT_TASK.equals(taskDef) ||
                    NaturalResourcesConstants.INSPECT_LOCATE_TASK.equals(taskDef) ||
                    NaturalResourcesConstants.REVIEW_INSPECT_TASK.equals(taskDef) ||
                    NaturalResourcesConstants.ATTST_INSPECT_RPT_TASK.equals(taskDef)) {
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_APPROVE, "N");
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_REJECT, "N");
                }
                if (NaturalResourcesConstants.APPROVE_REQ_TASK.equals(taskDef)) {
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_RETURN, "Y");
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_APPROVE, "Y");
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_REJECT, "Y");

                }
                if (NaturalResourcesConstants.ATTEST_REQ_TASK.equals(taskDef)) {
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_RETURN, "Y");
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_APPROVE, "Y");
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_REJECT, "Y");
                }
            } else if ((typeId.compareTo(new BigDecimal("56")) == 0) || (typeId.compareTo(new BigDecimal("58")) == 0) ||
                       (typeId.compareTo(new BigDecimal("59")) == 0)) {
                if (NaturalResourcesConstants.VALIDATE_REQ_TASK.equals(taskDef)) {
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_RETURN, "Y");
                }
                if (NaturalResourcesConstants.UPDATE_REQ_TASK.equals(taskDef)) {
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_REJECT, "N");
                }
                if (NaturalResourcesConstants.REVIEW_REQUEST_TASK.equals(taskDef)) {
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_INSPECTION_CHECK_BOX,
                                                       NaturalResourcesConstants.CHAR_Y);
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_SAVE, "Y");
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_RETURN, "Y");
                }

                if (NaturalResourcesConstants.SELECT_INSPECT_TASK.equals(taskDef) ||
                    NaturalResourcesConstants.INSPECT_LOCATE_TASK.equals(taskDef) ||
                    NaturalResourcesConstants.REVIEW_INSPECT_TASK.equals(taskDef) ||
                    NaturalResourcesConstants.ATTST_INSPECT_RPT_TASK.equals(taskDef)) {
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_APPROVE, "N");
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_REJECT, "N");
                }
            }
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }
    }

    public void claimTaskActionListener(ActionEvent actionEvent) {
        // Add event code here...
        String taskId = (String) ADFUtils.getPageFlowScopeAttribute(NaturalResourcesConstants.P_TASK_ID);
        Boolean status = false;
        RichButton claimButtonBnd = null;
        try {
            if (null != taskId) {
                status = invokeBPMServices(taskId, NaturalResourcesConstants.CLAIM, null, null);
            }

            if (status)
                CustomComponentsMiscUtils.showInfoMessageFromResourceBundle(NaturalResourcesConstants.REQUEST_ASSIGNED_SUCCESS_MSG);
            claimButtonBnd = (RichButton) UIUtils.findComponentInRoot(NaturalResourcesConstants.CLAIM_TASK_BTN);
            if (null != claimButtonBnd) {
                claimButtonBnd.setVisible(false);
                AdfFacesContext.getCurrentInstance().addPartialTarget(claimButtonBnd);
            }
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }
    }

    public TaskFlowId getDynamicTaskFlowId() {
        return TaskFlowId.parse(taskFlowId);
    }

    public void setDynamicTaskFlowId(String taskFlowId) {
        this.taskFlowId = taskFlowId;
    }


    public void onReleaseTask(ActionEvent actionEvent) {
        String taskId = (String) ADFUtils.getPageFlowScopeAttribute(NaturalResourcesConstants.P_TASK_ID);
        Boolean status = invokeBPMServices(taskId, NaturalResourcesConstants.RELEASE, null, null);
        if (status) {
            ADFUtils.putViewScopeAttribute(NaturalResourcesConstants.P_POPUP_VAL, NaturalResourcesConstants.RELEASE);
            doViewDetailsConfirmationPopup(NaturalResourcesConstants.CHAR_Y);
        }
    }

    public void validateApproval(ActionEvent actionEvent) {
        if (ADFUtils.getPageFlowScopeAttribute(NaturalResourcesConstants.P_REQ_TYPE) != null) {
            Object retVal = NaturalResourcesConstants.CHAR_NA;
            Boolean flag = false;
            String taskId = (String) ADFUtils.getPageFlowScopeAttribute(NaturalResourcesConstants.P_TASK_ID);
            BigDecimal typeId = (BigDecimal) ADFUtils.getPageFlowScopeAttribute(NaturalResourcesConstants.P_REQ_TYPE);
            String taskDef = (String) ADFUtils.getPageFlowScopeAttribute(NaturalResourcesConstants.P_TASK_DEFINITION);


            if (flag) {
                retVal = ADFUtils.executeOperationBinding(NaturalResourcesConstants.VALIDATE_APPROVE_DATA);
            }


            if (NaturalResourcesConstants.CHAR_Y.equals(retVal) || NaturalResourcesConstants.CHAR_NA.equals(retVal)) {
                doViewDetailsConfirmationPopup("Y");
            }
        }
    }

    public String onViewDetailsConfirmationOkAction() {
        // Add event code here...
        System.out.println("onViewDetailsConfirmationOkAction...!");
        String pComments = null;
        String popupVal = (String) ADFUtils.getViewScopeAttribute(NaturalResourcesConstants.P_POPUP_VAL);
        String taskDef = (String) ADFUtils.getPageFlowScopeAttribute(NaturalResourcesConstants.P_TASK_DEFINITION);
        String taskId = (String) ADFUtils.getPageFlowScopeAttribute(NaturalResourcesConstants.P_TASK_ID);
        BigDecimal typeId = (BigDecimal) ADFUtils.getPageFlowScopeAttribute(NaturalResourcesConstants.P_REQ_TYPE);
        BigDecimal reqId = (BigDecimal) ADFUtils.getPageFlowScopeAttribute(NaturalResourcesConstants.P_REQUEST_ID);
        RichPopup confirmationPopupBnd = (RichPopup) UIUtils.findComponentInRoot("viewDtlsPopupId");
        Boolean bpmResponse = false;
        Boolean inspectionFlag = false;
        System.out.println("***** popupVal:" + popupVal);

        if (null != popupVal) {
            if (NaturalResourcesConstants.APPROVE.equals(popupVal) ||
                NaturalResourcesConstants.ACCEPT.equals(popupVal)) {
                pComments = (String) ADFUtils.getViewScopeAttribute(NaturalResourcesConstants.P_APPROVAL_COMMENTS);
                updateApprovalData();

                if (NaturalResourcesConstants.APPROVE.equals(popupVal))
                    ADFUtils.putViewScopeAttribute(NaturalResourcesConstants.P_VD_CONFIRM_MSG,
                                                   CustomComponentsMiscUtils.getMessageFromResourceBundle(NaturalResourcesConstants.REQUEST_APPROVAL_SUBMISSION_MSG));
                else {
                    ADFUtils.putViewScopeAttribute(NaturalResourcesConstants.P_VD_CONFIRM_MSG,
                                                   CustomComponentsMiscUtils.getMessageFromResourceBundle(NaturalResourcesConstants.REQ_PROCESSED_SUCCESS));
                    popupVal = NaturalResourcesConstants.APPROVE;
                }

                if (taskDef != null && isRequiredDBProcedureCall(taskDef, typeId)) {
                    ApplicationModule nsAm =
                        NaturalResourcesUtils.getApplicationModule(NaturalResourcesConstants.NATURAL_RESOURCE_DC);
                    callCmnRequestDBProcedure(nsAm, reqId, popupVal, pComments);
                }
                String isInspectRequired = null;
                isInspectRequired =
                    (String) ADFUtils.getPageFlowScopeAttribute(NaturalResourcesConstants.P_INSPECTION_CHECK_BOX);
                if (null != isInspectRequired && "Y".equals(isInspectRequired)) {
                    inspectionFlag = (Boolean) ADFUtils.getBoundAttributeValue("IsInspectionCheckFlagT");
                } else {
                    inspectionFlag = isInspectionRequired(taskDef, typeId);
                }
                if (inspectionFlag &&
                    (null != taskDef && NaturalResourcesConstants.REVIEW_REQUEST_TASK.equals(taskDef))) {
                    bpmResponse = invokeBPMServices(taskId, NaturalResourcesConstants.INSPECTION, null, pComments);
                } else {
                    bpmResponse = invokeBPMServices(taskId, popupVal, null, pComments);
                }
                System.out.println("***** after callCmnRequest ****");
                if (bpmResponse) {
                    ADFUtils.putViewScopeAttribute(NaturalResourcesConstants.P_POPUP_VAL,
                                                   NaturalResourcesConstants.SUCCESS);
                    ADFUtils.putViewScopeAttribute("pViewDetailsConfirmMsg",
                                                   CustomComponentsMiscUtils.getMessageFromResourceBundle("REQUEST_APPROVAL_SUBMISSION_MSG"));
                    ADFUtils.putViewScopeAttribute(NaturalResourcesConstants.P_APPROVAL_COMMENTS,
                                                   NaturalResourcesConstants.APPROVE);
                } else {
                    ADFUtils.putViewScopeAttribute(NaturalResourcesConstants.P_POPUP_VAL,
                                                   NaturalResourcesConstants.FAILED);
                }

                ADFUtils.markViewScopeAsDirty();
                AdfFacesContext.getCurrentInstance().addPartialTarget(confirmationPopupBnd);

            } else if (NaturalResourcesConstants.REJECT.equals(popupVal) ||
                       NaturalResourcesConstants.RETURN.equals(popupVal)) {
                System.out.println("return listener...");
                pComments = (String) ADFUtils.getViewScopeAttribute(NaturalResourcesConstants.P_APPROVAL_COMMENTS);

                if (NaturalResourcesConstants.REJECT.equals(popupVal)) {
                    ApplicationModule nsAm =
                        NaturalResourcesUtils.getApplicationModule(NaturalResourcesConstants.NATURAL_RESOURCE_DC);
                    callCmnRequestDBProcedure(nsAm, reqId, popupVal, pComments);
                }
                System.out.println("BPM call..." + popupVal);
                invokeBPMServices(taskId, popupVal, null, pComments);
                System.out.println("BPM call ends...");
                if (NaturalResourcesConstants.RETURN.equals(popupVal))
                    ADFUtils.putViewScopeAttribute(NaturalResourcesConstants.P_VD_CONFIRM_MSG,
                                                   CustomComponentsMiscUtils.getMessageFromResourceBundle(NaturalResourcesConstants.REQ_PROCESSED_SUCCESS));
                else
                    ADFUtils.putViewScopeAttribute(NaturalResourcesConstants.P_VD_CONFIRM_MSG,
                                                   CustomComponentsMiscUtils.getMessageFromResourceBundle(NaturalResourcesConstants.REQUEST_REJECT_SUBMISSION_MSG));

                ADFUtils.putViewScopeAttribute(NaturalResourcesConstants.P_POPUP_VAL,
                                               NaturalResourcesConstants.SUCCESS);
                ADFUtils.markViewScopeAsDirty();
                AdfFacesContext.getCurrentInstance().addPartialTarget(confirmationPopupBnd);
                System.out.println("popupVal:" + popupVal);
            }
            //            else if (NaturalResourcesConstants.INSPECTION.equals(popupVal)) {
            //                System.out.println("Inspection listener...");
            //                pComments = (String) ADFUtils.getViewScopeAttribute(NaturalResourcesConstants.P_APPROVAL_COMMENTS);
            //
            //                if (NaturalResourcesConstants.INSPECTION.equals(popupVal)) {
            //                    ApplicationModule nsAm =
            //                        NaturalResourcesUtils.getApplicationModule(NaturalResourcesConstants.NATURAL_RESOURCE_DC);
            //                    callCmnRequestDBProcedure(nsAm, reqId, popupVal, pComments);
            //                }
            //                System.out.println("BPM call..." + popupVal);
            //                invokeBPMServices(taskId, popupVal, null, pComments);
            //
            //                if (NaturalResourcesConstants.INSPECTION.equals(popupVal))
            //                    ADFUtils.putViewScopeAttribute(NaturalResourcesConstants.P_VD_CONFIRM_MSG,
            //                                                   CustomComponentsMiscUtils.getMessageFromResourceBundle(NaturalResourcesConstants.REQ_PROCESSED_SUCCESS));
            //                else
            //                    ADFUtils.putViewScopeAttribute(NaturalResourcesConstants.P_VD_CONFIRM_MSG,
            //                                                   CustomComponentsMiscUtils.getMessageFromResourceBundle(NaturalResourcesConstants.REQUEST_REJECT_SUBMISSION_MSG));
            //
            //                ADFUtils.putViewScopeAttribute(NaturalResourcesConstants.P_POPUP_VAL,
            //                                               NaturalResourcesConstants.SUCCESS);
            //                ADFUtils.markViewScopeAsDirty();
            //                AdfFacesContext.getCurrentInstance().addPartialTarget(confirmationPopupBnd);
            //                System.out.println("popupVal:" + popupVal);
            //            }
            else if (NaturalResourcesConstants.SUCCESS.equals(popupVal) ||
                     NaturalResourcesConstants.RELEASE.equals(popupVal)) {
                ADFUtils.putViewScopeAttribute(NaturalResourcesConstants.P_POPUP_VAL, null);

                ADFUtils.markViewScopeAsDirty();
                AdfFacesContext.getCurrentInstance().addPartialTarget(confirmationPopupBnd);
                this.doViewDetailsConfirmationPopup("N");

                return NaturalResourcesConstants.TO_INBOX;
            }
        }

        return null;
    }

    private Boolean isRequiredDBProcedureCall(String taskDef, BigDecimal typeId) {
        Boolean flag = true;
        if (typeId != null) {
            if ((typeId.compareTo(new BigDecimal("44")) == 0) || (typeId.compareTo(new BigDecimal("51")) == 0) ||
                (typeId.compareTo(new BigDecimal("54")) == 0) || (typeId.compareTo(new BigDecimal("55")) == 0) ||
                (typeId.compareTo(new BigDecimal("52")) == 0) || (typeId.compareTo(new BigDecimal("53")) == 0) ||
                (typeId.compareTo(new BigDecimal("45")) == 0) || (typeId.compareTo(new BigDecimal("56")) == 0)) {
                if (!NaturalResourcesConstants.ATTEST_REQ_TASK.equals(taskDef)) {
                    flag = false;
                }
            } else if ((typeId.compareTo(new BigDecimal("43")) == 0) || (typeId.compareTo(new BigDecimal("56")) == 0)) {
                if (!NaturalResourcesConstants.APPROVE_REQ_TASK.equals(taskDef)) {
                    flag = false;
                }
            }
        }

        return flag;
    }

    public void renderInspectionTab() {
        String lcInspectionExist = null;
        String lcFromOutcome = null;
        String lcTaskDefinition = null;
        lcFromOutcome = (String) ADFUtils.getPageFlowScopeAttribute(NaturalResourcesConstants.P_FROM_OUTCOME);
        lcTaskDefinition = (String) ADFUtils.getPageFlowScopeAttribute(NaturalResourcesConstants.P_TASK_DEFINITION);
        System.out.println("View Details OutCome: " + lcFromOutcome);
        if (null != lcTaskDefinition) {
            final String[] INSPECTION_TASK_LIST = {
                CustomComponentConstants.SELECT_INSPECTOR_TASK, CustomComponentConstants.INSPECT_LOCATION_TASK,
                CustomComponentConstants.REVIEW_INSPECTION_TASK, CustomComponentConstants.ATTEST_INSPECTION_REPORT_TASK
            };
            List<String> inspectionTaskList = null;
            inspectionTaskList = new ArrayList<>(Arrays.asList(INSPECTION_TASK_LIST));
            if (null != inspectionTaskList && inspectionTaskList.contains(lcTaskDefinition)) {
                if (CustomComponentConstants.PRIVATE_INBOX_PARAM.equals(lcFromOutcome)) {
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_INSPECTION_EDIT_MODE,
                                                       NaturalResourcesConstants.CHAR_Y);
                } else {
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_INSPECTION_EDIT_MODE,
                                                       NaturalResourcesConstants.CHAR_N);
                }
                ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_SHOW_INSPECT,
                                                   NaturalResourcesConstants.CHAR_Y);
            } else {
                ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_INSPECTION_EDIT_MODE,
                                                   NaturalResourcesConstants.CHAR_N);
                lcInspectionExist =
                    (String) ADFUtils.executeOperationBinding(NaturalResourcesConstants.IS_REQUEST_INSPECTION_EXIST);
                if (null != lcInspectionExist && NaturalResourcesConstants.CHAR_Y.equals(lcInspectionExist)) {
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_SHOW_INSPECT,
                                                       NaturalResourcesConstants.CHAR_Y);
                } else {
                    ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_SHOW_INSPECT,
                                                       NaturalResourcesConstants.CHAR_N);
                }
            }
        }
    }

    public String onViewDetailsConfirmationCancelAction() {
        // Add event code here...
        this.doViewDetailsConfirmationPopup(NaturalResourcesConstants.CHAR_N);
        ADFUtils.putViewScopeAttribute(NaturalResourcesConstants.P_APPROVAL_COMMENTS, null);
        ADFUtils.markViewScopeAsDirty();
        return null;
    }

    public void doViewDetailsConfirmationPopup(String pShow) {
        RichPopup confirmationPopupBnd = null;
        if (null != pShow) {
            confirmationPopupBnd = (RichPopup) UIUtils.findComponentInRoot(NaturalResourcesConstants.VIEW_DTLS_POPUPID);
            if (null != confirmationPopupBnd) {
                if (NaturalResourcesConstants.CHAR_Y.equals(pShow)) {
                    confirmationPopupBnd.show(new RichPopup.PopupHints());
                } else {
                    confirmationPopupBnd.hide();
                }
            }
        }
    }


    public void setPComments(String pComments) {
        this.pComments = pComments;
    }

    public String getPComments() {
        return pComments;
    }

    public void onReportListener(ActionEvent actionEvent) {
        String url = null;
        String rptTemp = null;
        String reportType = (String) UIUtils.getFromPageFlowScope("reportType");
        BigDecimal typeId = (BigDecimal) ADFUtils.getPageFlowScopeAttribute(NaturalResourcesConstants.P_REQ_TYPE);

        if ("qrypermitreport".equals(reportType)) {
            rptTemp = NaturalResourcesConstants.QUARRY_PERMIT_REPORT;
        } else if (NaturalResourcesConstants.P_DEBRIS_PERMIT_REPORT.equalsIgnoreCase(reportType)) {
            rptTemp = NaturalResourcesConstants.DEBRIS_PERMIT_REPORT;
        } else if (NaturalResourcesConstants.P_SUMMARY_REPORT.equalsIgnoreCase(reportType)) {
            if (null != typeId) {
                if ((typeId.compareTo(new BigDecimal("51")) == 0) || (typeId.compareTo(new BigDecimal("52")) == 0) ||
                    (typeId.compareTo(new BigDecimal("53")) == 0)) {
                    rptTemp = NaturalResourcesConstants.DEBRIS_SUMMARY_REPORT;
                }
            }
        }
        if (rptTemp != null) {
            try {
                BigDecimal reqId =
                    (BigDecimal) ADFUtils.getPageFlowScopeAttribute(NaturalResourcesConstants.P_REQUEST_ID);

                if (reqId != null) {
                    ViewObject vo = ADFUtils.findIterator("CmnSysParametersRVO1Iterator").getViewObject();
                    vo.setWhereClause(null);
                    vo.setWhereClause("PARAMETER_NAME in ('" + MMEConstants.SYSTEM_PARAMETER_ENCRYPTION_CHARSET +
                                      "','" + MMEConstants.SYSTEM_PARAMETER_ENCRYPTION_KEY + "','" +
                                      MMEConstants.BI_REPORT_WSDL_USERNAME + "','" +
                                      MMEConstants.BI_REPORT_WSDL_PASSWORD + "','" +
                                      MMEConstants.BI_REPORT_WSDL_END_POINT + "')");
                    vo.setOrderByClause("PARAMETER_NAME");
                    vo.executeQuery();
                    Row requestRow = vo.next();
                    String reportEndPointURL =
                        (String) requestRow.getAttribute(NaturalResourcesConstants.PARAMETER_VALUE);
                    System.out.println("reportEndPointURL" + reportEndPointURL);
                    requestRow = vo.next();
                    String serverPassword = (String) requestRow.getAttribute(NaturalResourcesConstants.PARAMETER_VALUE);
                    System.out.println("serverPassword" + serverPassword);
                    requestRow = vo.next();
                    String serverUsername = (String) requestRow.getAttribute(NaturalResourcesConstants.PARAMETER_VALUE);
                    System.out.println("serverUsername" + serverUsername);
                    requestRow = vo.next();
                    String characterSet = (String) requestRow.getAttribute(NaturalResourcesConstants.PARAMETER_VALUE);
                    System.out.println("characterSet" + characterSet);
                    requestRow = vo.next();
                    String key = (String) requestRow.getAttribute(NaturalResourcesConstants.PARAMETER_VALUE);
                    System.out.println("key" + key);
                    requestRow = vo.next();
                    String username = EncryptionUtils.decrypt(key, serverUsername, characterSet);
                    String password = EncryptionUtils.decrypt(key, serverPassword, characterSet);
                    Map<String, String> paramList = new HashMap<String, String>();
                    paramList.put(NaturalResourcesConstants.ROW_ID, null != reqId ? reqId.toString() : null);
                    url =
                        ReportsUtils.callGetReportURLWebService(rptTemp, paramList, NaturalResourcesConstants.PDF,
                                                                username, password, reportEndPointURL, "0");
                    System.out.println("URL is:::" + url);

                    if (null != url) {
                        UIUtils.openUrlInNewWindow(url);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public void validateReject(ActionEvent actionEvent) {
        // Add event code here...

        doViewDetailsConfirmationPopup("Y");
    }

    private void runJavaScriptCode(String javascriptCode) {
        FacesContext facesCtx = FacesContext.getCurrentInstance();

        ExtendedRenderKitService service = Service.getRenderKitService(facesCtx, ExtendedRenderKitService.class);

        service.addScript(facesCtx, javascriptCode);
    }


    /**
     * Method to process report for permit reports
     * @param actn action listener
     */
    public void benefQryPermitReport(ActionEvent actn) {
        DCIteratorBinding benefQryPrmtIter = ADFUtils.findIterator("BeneficiaryQryPermitsRVOIterator");
        BigDecimal requestId = (BigDecimal) benefQryPrmtIter.getCurrentRow().getAttribute("RequestId");
        ADFUtils.putPageFlowScopeAttribute(NaturalResourcesConstants.P_REQUEST_ID, requestId);
        onReportListener(actn);
    }

    /**
     * Method to update DB tables as per the task definition
     */
    private void updateApprovalData() {
        if (ADFUtils.getPageFlowScopeAttribute(NaturalResourcesConstants.P_REQ_TYPE) != null) {
            BigDecimal typeId = (BigDecimal) ADFUtils.getPageFlowScopeAttribute(NaturalResourcesConstants.P_REQ_TYPE);
            String taskDef = (String) ADFUtils.getPageFlowScopeAttribute(NaturalResourcesConstants.P_TASK_DEFINITION);
            Boolean flag = false;

            if (typeId.compareTo(new BigDecimal("44")) == 0) {
                if (NaturalResourcesConstants.REVIEW_REQUEST_TASK.equals(taskDef))
                    flag = true;
            }else if (typeId.compareTo(new BigDecimal("43")) == 0) {
                if (NaturalResourcesConstants.UPDATE_REQ_TASK.equals(taskDef))
                    flag = true;
            }

            if (flag) {
                System.out.println("execute submit approval data");
                ADFUtils.executeOperationBinding("submitApprovalData");
            }
        }
    }

    /**
     * Method to check if inspection is required
     */
    private boolean isInspectionRequired(String pTaskDef, BigDecimal pTypeId) {
        boolean inspectionRequired = false;
        String inspectionFlag = null;

        if (pTypeId.compareTo(new BigDecimal("54")) == 0) {
            inspectionFlag = (String) ADFUtils.getBoundAttributeValue(IS_INSPECT_REQ_ATTR);
            if (NaturalResourcesConstants.REVIEW_REQUEST_TASK.equals(pTaskDef) && "Y".equals(inspectionFlag)) {
                inspectionRequired = true;
            }
        }

        if (pTypeId.compareTo(new BigDecimal("44")) == 0) {
            inspectionFlag = (String) ADFUtils.getBoundAttributeValue(IS_INSPECT_REQ_ATTR);
            if (NaturalResourcesConstants.REVIEW_REQUEST_TASK.equals(pTaskDef) && "Y".equals(inspectionFlag)) {
                inspectionRequired = true;
            }
        } else if ((pTypeId.compareTo(new BigDecimal("51")) == 0) || (pTypeId.compareTo(new BigDecimal("52")) == 0) ||
                   (pTypeId.compareTo(new BigDecimal("53")) == 0)) {
            BigDecimal movementTypeId = null;
            if (pTypeId.compareTo(new BigDecimal("51")) == 0) {
                movementTypeId = (BigDecimal) ADFUtils.getBoundAttributeValue(NEW_MOVEMENT_TYPE_ATTR);
            } else if (pTypeId.compareTo(new BigDecimal("52")) == 0) {
                // renew
                movementTypeId = (BigDecimal) ADFUtils.getBoundAttributeValue(RENEW_MOVEMENT_TYPE_ATTR);
            } else if (pTypeId.compareTo(new BigDecimal("53")) == 0) {
                // modify
                movementTypeId = (BigDecimal) ADFUtils.getBoundAttributeValue(MODIFY_MOVEMENT_TYPE_ATTR);
            }

            if (movementTypeId.compareTo(new BigDecimal(4)) == 0) {
                if (NaturalResourcesConstants.REVIEW_REQUEST_TASK.equals(pTaskDef)) {
                    inspectionRequired = true;
                }
            }
        } else if (pTypeId.compareTo(new BigDecimal("45")) == 0) {
            inspectionFlag = (String) ADFUtils.getBoundAttributeValue(IS_INSPECT_REQ_ATTR);
            if (NaturalResourcesConstants.REVIEW_REQUEST_TASK.equals(pTaskDef) && "Y".equals(inspectionFlag)) {
                inspectionRequired = true;
            }
        }
        return inspectionRequired;
    }

    public void saveRenewQuarryBackend(ActionEvent actionEvent) {
        ADFUtils.executeOperationBinding("submitApprovalData");
        JSFUtils.addFacesInformationMessage("Changes Saved Successfully");
    }

    public void onSaveActionListener(ActionEvent actionEvent) {
        // Add event code here...
        ADFUtils.executeOperationBinding(NaturalResourcesConstants.SUBMIT_APPROVAL_DATA);
        JSFUtils.addFacesInformationMessage(NaturalResourcesUtils.getMessageFromResourceBundle(NaturalResourcesConstants.CHANGES_SAVE_SUCCESS_MSG));
    }

    public void onIsInspectionValueChangeListener(ValueChangeEvent valueChangeEvent) {
        // Add event code here...s
        if (null != valueChangeEvent.getNewValue()) {
            if (Boolean.TRUE.equals(valueChangeEvent.getNewValue())) {
                ADFUtils.setBoundAttributeValue("IsInspectionRequired", NaturalResourcesConstants.CHAR_Y);
            } else {
                ADFUtils.setBoundAttributeValue("IsInspectionRequired", NaturalResourcesConstants.CHAR_N);
            }
        }
    }

    public void populateBeneficiaryPreviousRequestsTab(BigDecimal pReqTypeId) {
        if (null != pReqTypeId) {
            if (new BigDecimal(56).equals(pReqTypeId)) {
                ADFUtils.putPageFlowScopeAttribute("pPreviousRequestRender", NaturalResourcesConstants.CHAR_Y);
                ADFUtils.putPageFlowScopeAttribute("pSource", NaturalResourcesConstants.PREVIOUS_PERMITS);
                setDynamicPreviousRequestTaskFlowId(NaturalResourcesConstants.CRUSHER_PERMIT_SEARCH_TF);
                ADFUtils.putPageFlowScopeAttribute("pPreviousRequestsTitle",
                                                   NaturalResourcesUtils.getMessageFromResourceBundle("BENEFICIARY_CRUSHER_PERMITS"));
            }
        }
    }

    public void populatePermitsTab(BigDecimal pReqTypeId) {
        if (null != pReqTypeId) {
            if (new BigDecimal(56).equals(pReqTypeId)) {
                ADFUtils.putPageFlowScopeAttribute("pPermitsRender", NaturalResourcesConstants.CHAR_Y);
                setDynamicPermitsTaskFlowId(NaturalResourcesConstants.CRUSHER_PERMITS_TF);
                ADFUtils.putPageFlowScopeAttribute("pPermitsTitle",
                                                   NaturalResourcesUtils.getMessageFromResourceBundle("CRUSHER_PERMIT_DETAILS"));
            } else if (new BigDecimal(54).equals(pReqTypeId) || new BigDecimal(55).equals(pReqTypeId)) {
                ADFUtils.putPageFlowScopeAttribute("pPermitsRender", NaturalResourcesConstants.CHAR_Y);
                setDynamicPermitsTaskFlowId(NaturalResourcesConstants.DETONATION_PERMIT_DET);
                ADFUtils.putPageFlowScopeAttribute("pPermitsTitle",
                                                   NaturalResourcesUtils.getMessageFromResourceBundle("DETONATION_PERMIT_DETAILS"));
            }
        }
    }

    public void populateOtherContentTab(BigDecimal pReqTypeId) {
        if (null != pReqTypeId) {
            if (new BigDecimal(56).equals(pReqTypeId)) {
                ADFUtils.putPageFlowScopeAttribute("pPermitsRender", NaturalResourcesConstants.CHAR_Y);
                setDynamicPermitsTaskFlowId(NaturalResourcesConstants.CRUSHER_PERMITS_TF);
                ADFUtils.putPageFlowScopeAttribute("pPermitsTitle",
                                                   NaturalResourcesUtils.getMessageFromResourceBundle("CRUSHER_PERMIT_DETAILS"));
            }
        }
    }

    public void populateOldPermitsTab(BigDecimal pReqTypeId) {
        if (null != pReqTypeId) {
            if (new BigDecimal(56).equals(pReqTypeId)) {
                ADFUtils.putPageFlowScopeAttribute("pPermitsRender", NaturalResourcesConstants.CHAR_Y);
                setDynamicPermitsTaskFlowId(NaturalResourcesConstants.CRUSHER_PERMITS_TF);
                ADFUtils.putPageFlowScopeAttribute("pPermitsTitle",
                                                   NaturalResourcesUtils.getMessageFromResourceBundle("CRUSHER_PERMIT_DETAILS"));
            } else if (new BigDecimal(55).equals(pReqTypeId)) {
                ADFUtils.putPageFlowScopeAttribute("pPermitsRender", NaturalResourcesConstants.CHAR_Y);
                setDynamicPermitsTaskFlowId(NaturalResourcesConstants.DETONATION_PERMIT_DET);
                ADFUtils.putPageFlowScopeAttribute("pPermitsTitle",
                                                   NaturalResourcesUtils.getMessageFromResourceBundle("DETONATION_OLD_PERMIT_DETAILS"));
                ADFUtils.putPageFlowScopeAttribute("mode","OLD");
                
            }
        }
    }

    public TaskFlowId getDynamicPreviousRequestTaskFlowId() {
        return TaskFlowId.parse(previousRequestTaskFlowId);
    }

    public void setDynamicPreviousRequestTaskFlowId(String taskFlowId) {
        this.previousRequestTaskFlowId = taskFlowId;
    }

    public void setDynamicPermitsTaskFlowId(String taskFlowId) {
        this.permitsTaskFlowId = taskFlowId;
    }

    public TaskFlowId getDynamicPermitsTaskFlowId() {
        return TaskFlowId.parse(permitsTaskFlowId);
    }
    
    public void setDynamicOldPermitsTaskFlowId(String taskFlowId) {
        this.oldPermitsTaskFlowId = taskFlowId;
    }

    public TaskFlowId getDynamicOldPermitsTaskFlowId() {
        return TaskFlowId.parse(oldPermitsTaskFlowId);
    }


    public void setDynamicOtherContentTaskFlowId(String taskFlowId) {
        this.otherContentTaskFlowId = taskFlowId;
    }

    public TaskFlowId getDynamicOtherContentTaskFlowId() {
        return TaskFlowId.parse(otherContentTaskFlowId);
    }

}
