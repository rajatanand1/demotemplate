package qa.gov.mme.envservices.naturalresources.view.utils;

import java.util.ResourceBundle;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import oracle.javatools.resourcebundle.BundleFactory;

import oracle.jbo.server.ApplicationModuleImpl;

import oracle.jbo.server.ViewObjectImpl;

import qa.gov.mme.common.model.bc.extension.MmeViewObjectImpl;
import qa.gov.mme.component.model.app.CustomComponentAMImpl;
import qa.gov.mme.envservices.naturalresources.view.constant.NaturalResourcesConstants;

public class NaturalResourcesUtils {
    public NaturalResourcesUtils() {
        super();
    }

    public static void showErrorMessageFromResourceBundle(String key) {
        String m =
            messageFromResourceBundle(NaturalResourcesConstants.NATURAL_RESOURCES_VIEW_CONTROLLER_BUNDLE,
                                      key);
        showErrorMessage(m);
        return;
    }

    public static String getMessageFromResourceBundle(String key) {
        String m =
            messageFromResourceBundle(NaturalResourcesConstants.NATURAL_RESOURCES_VIEW_CONTROLLER_BUNDLE,
                                      key);
        return m;
    }

    public static void showInfoMessageFromResourceBundle(String key) {
        String m =
            messageFromResourceBundle(NaturalResourcesConstants.NATURAL_RESOURCES_VIEW_CONTROLLER_BUNDLE,
                                      key);
        showMessage(m);
    }
    
    public static void showWarrningMessageFromResourceBundle(String key) {
        String m =
            messageFromResourceBundle(NaturalResourcesConstants.NATURAL_RESOURCES_VIEW_CONTROLLER_BUNDLE,
                                      key);
        showWarrningMessage(m);
    }

    /** Method Take The Path Of The Resource Bundle And the Key And Return The Value Releated To This Key */
    public static String messageFromResourceBundle(String path, String key) {
        ResourceBundle bundle = null;
        if (null != path) {
            bundle = BundleFactory.getBundle(path, FacesContext.getCurrentInstance()
                                                               .getViewRoot()
                                                               .getLocale());
        }
        if (bundle != null) {
            return bundle.getString(key);
        } else
            return null;
    }

    public static void showErrorMessage(String message) {
        FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, null);
        fm.setDetail(message);
        FacesContext.getCurrentInstance().addMessage(null, fm);
    }

    public static void showMessage(String message) {
        FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, null, null);
        fm.setDetail(message);
        FacesContext.getCurrentInstance().addMessage(null, fm);
    }
    public static void showWarrningMessage(String message) {
        FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_WARN, null, null);
        fm.setDetail(message);
        FacesContext.getCurrentInstance().addMessage(null, fm);
    }
    
    public static ApplicationModuleImpl getApplicationModule(String dataControl) {
        FacesContext fc = FacesContext.getCurrentInstance();
        Application app = fc.getApplication();
        ExpressionFactory elFactory = app.getExpressionFactory();
        ELContext elContext = fc.getELContext();
        ValueExpression valueExp =
            elFactory.createValueExpression(elContext, "#{data." + dataControl + ".dataProvider}", Object.class);
        return (ApplicationModuleImpl) valueExp.getValue(elContext);
    }
    
    
    public static MmeViewObjectImpl getCustomComponentVOInstance(String viewObject) {
        ApplicationModuleImpl applicationModuleImpl = NaturalResourcesUtils.getApplicationModule("CustomComponentAMDataControl");
        MmeViewObjectImpl mmeViewObjectImpl = (MmeViewObjectImpl) applicationModuleImpl.findViewObject(viewObject);
        return mmeViewObjectImpl;
    }

}
