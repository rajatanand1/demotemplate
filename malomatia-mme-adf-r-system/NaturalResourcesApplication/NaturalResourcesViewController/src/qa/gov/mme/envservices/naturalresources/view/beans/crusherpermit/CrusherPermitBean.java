package qa.gov.mme.envservices.naturalresources.view.beans.crusherpermit;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.math.BigDecimal;

import java.net.URLConnection;

import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputFile;
import oracle.adf.view.rich.util.ResetUtils;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.domain.BlobDomain;
import oracle.jbo.domain.Date;

import org.apache.myfaces.trinidad.model.UploadedFile;

import qa.gov.mme.common.beans.CommonBaseBean;
import qa.gov.mme.common.constants.MMEConstants;
import qa.gov.mme.common.model.bc.view.readonly.CmnRegisteredCompaniesRVORowImpl;
import qa.gov.mme.common.model.bc.view.readonly.CmnRequestsAttachmentsRVOImpl;
import qa.gov.mme.common.model.bc.view.readonly.CmnRequestsAttachmentsRVORowImpl;
import qa.gov.mme.common.utils.ADFUtils;
import qa.gov.mme.common.utils.StringUtils;
import qa.gov.mme.common.ws.proxy.client.NaturalResourcesServiceClient;
import qa.gov.mme.common.ws.proxy.request.naturalresources.types.AttachmentTO;
import qa.gov.mme.common.ws.proxy.request.naturalresources.types.EntitiesTO;
import qa.gov.mme.common.ws.proxy.request.naturalresources.types.EnvAmdCrusherPmtRequestTO;
import qa.gov.mme.common.ws.proxy.request.naturalresources.types.EnvCrusherLocCoordinatesTO;
import qa.gov.mme.common.ws.proxy.request.naturalresources.types.EnvCrusherPermitCategoriesTO;
import qa.gov.mme.common.ws.proxy.request.naturalresources.types.EnvCrusherPermitDetailsTO;
import qa.gov.mme.common.ws.proxy.request.naturalresources.types.EnvNewCrusherPmtRequestTO;
import qa.gov.mme.common.ws.proxy.request.naturalresources.types.EnvRenewCrusherPmtRequestTO;
import qa.gov.mme.common.ws.proxy.request.naturalresources.types.TransactionalWSRequest;
import qa.gov.mme.common.ws.proxy.request.naturalresources.types.TransactionalWSReturn;
import qa.gov.mme.component.model.view.readonly.CmnApplicantTVORowImpl;
import qa.gov.mme.component.model.view.readonly.CmnBeneficiaryTVORowImpl;
import qa.gov.mme.component.view.beans.utils.CustomComponentsMiscUtils;
import qa.gov.mme.component.view.constant.CustomComponentConstants;
import qa.gov.mme.envservices.naturalresources.model.bc.view.EnvCrusherLocCoordinatesVOImpl;
import qa.gov.mme.envservices.naturalresources.model.bc.view.EnvCrusherLocCoordinatesVORowImpl;
import qa.gov.mme.envservices.naturalresources.model.bc.view.EnvCrusherPermitDetailsVORowImpl;
import qa.gov.mme.envservices.naturalresources.model.bc.view.EnvCrusherPermitsVORowImpl;
import qa.gov.mme.envservices.naturalresources.model.bc.view.EnvCrusherPmtCategoriesVOImpl;
import qa.gov.mme.envservices.naturalresources.model.bc.view.EnvCrusherPmtCategoriesVORowImpl;
import qa.gov.mme.envservices.naturalresources.model.bc.view.readonly.EnvCrusherPermitTVORowImpl;
import qa.gov.mme.envservices.naturalresources.view.constant.NaturalResourcesConstants;
import qa.gov.mme.envservices.naturalresources.view.utils.NaturalResourcesUtils;

public class CrusherPermitBean extends CommonBaseBean {
    @SuppressWarnings("compatibility:6355686098300048952")
    private static final long serialVersionUID = 1L;
    private RichInputFile recipientSignUploadFileBnd;
    private RichPopup acknowledgePopupBnd;
    private RichPopup confirmationSuccessPopupBnd;

    public CrusherPermitBean() {
    }


    public void onValidateRequestDetailsAction() {
        // Add event code here...
        ADFUtils.executeOperationBinding("validateCrusherReqSubmitDetails");
        ADFUtils.putPageFlowScopeAttribute("pMandatoryAttachmentIdList", this.setMandatoryAttachmentIdList());
        ADFUtils.markPageFlowScopeAsDirty();
    }

    public void onValidateAttachmentDetailsAction() {
        // Add event code here...isMandateAttachmentsUploaded
        ADFUtils.executeOperationBinding("isMandateAttachmentsUploaded");
        ADFUtils.putPageFlowScopeAttribute("pMandatoryAttachmentIdList", this.setMandatoryAttachmentIdList());
        ADFUtils.putPageFlowScopeAttribute("pEntityTypeIdList", this.setEntityTypeIdList());
        ADFUtils.markPageFlowScopeAsDirty();
    }

    public List<BigDecimal> setMandatoryAttachmentIdList() {
        String lcAccessMode = null;
        lcAccessMode = (String) ADFUtils.getPageFlowScopeAttribute("pAccessMode");
        List<BigDecimal> attachmentList = null;
        attachmentList = new ArrayList<BigDecimal>();
        if (null != lcAccessMode && "BACK".equals(lcAccessMode)) {
            attachmentList.add(new BigDecimal(1));
            attachmentList.add(new BigDecimal(87));
        }
        attachmentList.add(new BigDecimal(36));
        return attachmentList;

    }

    public List<BigDecimal> setEntityTypeIdList() {
        List<BigDecimal> attachmentList = null;
        attachmentList = new ArrayList<BigDecimal>();
        attachmentList.add(new BigDecimal(2));
        attachmentList.add(new BigDecimal(3));
        return attachmentList;

    }

    public String onValidateApplicantDetailsAction() {
        // Add event code here...
        String retVal = null;
        ADFUtils.executeOperationBinding("validateApplicantFormAndData");
        ADFUtils.putPageFlowScopeAttribute("pEntityTypeIdList", this.setEntityTypeIdList());
        ADFUtils.markPageFlowScopeAsDirty();
        return retVal;
    }

    public String onValidateBeneficiaryDetailsAction() {
        // Add event code here...validateBeneficiaryFormAndData
        String retVal = null;
        CmnBeneficiaryTVORowImpl cmnBeneficiaryTVORowImpl = null;
        cmnBeneficiaryTVORowImpl = (CmnBeneficiaryTVORowImpl) ADFUtils.findIterator("CmnBeneficiaryTVO1Iterator")
                                                                      .getViewObject()
                                                                      .first();
        if (null != cmnBeneficiaryTVORowImpl && null != cmnBeneficiaryTVORowImpl.getEntityTypeIdT()) {
            if (!(new BigDecimal(2).equals(cmnBeneficiaryTVORowImpl.getEntityTypeIdT()) &&
                  ("GOVERNMENT".equals(cmnBeneficiaryTVORowImpl.getSectorDescEnT())))) {

                Row compRow = null;
                compRow = (Row) ADFUtils.executeOperationBinding("doFilterRegisterCompanies");
                System.out.println("" + ADFUtils.getBoundAttributeValue("EIDT"));
                if (null == compRow) {
                    ADFUtils.setBoundAttributeValue("IsGovtEstablishmentT", "N");
                    NaturalResourcesUtils.showErrorMessageFromResourceBundle("BENEFICIARY_GOVT_EST_REG_COMPANY_MSG");
                } else {
                    retVal = "validateBeneficiaryData";
                }
            } else {
                ADFUtils.executeOperationBinding("validateBeneficiaryFormAndData");
                retVal = "validateBeneficiaryData";
                ADFUtils.setBoundAttributeValue("IsGovtEstablishmentT", "Y");
            }
        }
        return retVal;
    }

    public void onCategoriesSelectValueChangeListener(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        if (null != valueChangeEvent.getNewValue()) {
            valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
//            EnvCrusherPmtCategoriesVORowImpl envCrusherPmtCategoriesVORowImpl = null;
            //            EnvCrusherPmtCategoriesVOImpl envCrusherPmtCategoriesVOImpl = null;
            //            EnvCrusherPermitTVORowImpl envCrusherPermitTVORowImpl = null;
            //            envCrusherPermitTVORowImpl = (EnvCrusherPermitTVORowImpl) ADFUtils.findIterator("EnvCrusherPermitTVO1Iterator")
            //                                                                              .getViewObject()
            //                                                                              .first();
            //            RowSetIterator envCrusherPermitCategoriesESI = null;
            //            envCrusherPmtCategoriesVOImpl =
            //                (EnvCrusherPmtCategoriesVOImpl) ADFUtils.findIterator("EnvCrusherPmtCategoriesVO1Iterator")
            //                .getViewObject();
            //            envCrusherPmtCategoriesVORowImpl =
            //                (EnvCrusherPmtCategoriesVORowImpl) envCrusherPmtCategoriesVOImpl.getCurrentRow();
            //            if (null != envCrusherPmtCategoriesVORowImpl) {
            //                System.out.println("Id" + envCrusherPmtCategoriesVORowImpl.getCategoryId());
            //                if (new BigDecimal(3).equals(envCrusherPmtCategoriesVORowImpl.getCategoryId())) {
            //                    envCrusherPermitTVORowImpl.setIsCrusherInstallationT(NaturalResourcesConstants.CHAR_Y);
            //                    envCrusherPermitCategoriesESI = envCrusherPmtCategoriesVOImpl.createRowSetIterator(null);
            //                    if (Boolean.TRUE.equals(valueChangeEvent.getNewValue())) {
            //
            //                        this.setPermitCategoryRecord(envCrusherPermitCategoriesESI, Boolean.TRUE,
            //                                                     NaturalResourcesConstants.CHAR_Y, envCrusherPermitTVORowImpl);
            //                    } else if (Boolean.FALSE.equals(valueChangeEvent.getNewValue())) {
            //                        this.setPermitCategoryRecord(envCrusherPermitCategoriesESI, Boolean.FALSE,
            //                                                     NaturalResourcesConstants.CHAR_N, envCrusherPermitTVORowImpl);
            //                    }
            //                    envCrusherPmtCategoriesVOImpl.setCurrentRow(envCrusherPmtCategoriesVORowImpl);
            //                } else if (new BigDecimal(2).equals(envCrusherPmtCategoriesVORowImpl.getCategoryId())) {
            //                    if (Boolean.TRUE.equals(valueChangeEvent.getNewValue())) {
            //                        envCrusherPermitTVORowImpl.setIsDebrisSiftingT(NaturalResourcesConstants.CHAR_Y);
            //                    } else if (Boolean.FALSE.equals(valueChangeEvent.getNewValue())) {
            //                        envCrusherPermitTVORowImpl.setIsDebrisSiftingT(NaturalResourcesConstants.CHAR_N);
            //                    }
            //                } else if (new BigDecimal(1).equals(envCrusherPmtCategoriesVORowImpl.getCategoryId())) {
            //                    if (Boolean.TRUE.equals(valueChangeEvent.getNewValue())) {
            //                        envCrusherPermitTVORowImpl.setIsCrusherInstallationT(NaturalResourcesConstants.CHAR_Y);
            //                    } else if (Boolean.FALSE.equals(valueChangeEvent.getNewValue())) {
            //                        envCrusherPermitTVORowImpl.setIsCrusherInstallationT(NaturalResourcesConstants.CHAR_N);
            //                    }
            //                }
            //            }
        }
    }

    public void setPermitCategoryRecord(RowSetIterator envCrusherPermitCategoriesESI, Boolean pCheckFlag,
                                        String pIsReadOnly, EnvCrusherPermitTVORowImpl envCrusherPermitTVORowImpl) {
        if (null != envCrusherPermitCategoriesESI && null != pCheckFlag && null != pIsReadOnly) {
            EnvCrusherPmtCategoriesVORowImpl envCrusherPmtCategoriesVORowImpl = null;
            envCrusherPermitCategoriesESI.reset();
            while (envCrusherPermitCategoriesESI.hasNext()) {
                envCrusherPmtCategoriesVORowImpl =
                    (EnvCrusherPmtCategoriesVORowImpl) envCrusherPermitCategoriesESI.next();
                if (null != envCrusherPmtCategoriesVORowImpl) {
                    System.out.println("Catid" + envCrusherPmtCategoriesVORowImpl.getCategoryId());
                    envCrusherPermitTVORowImpl.setIsRequestSpaceDebrisT(pIsReadOnly);
                    if (new BigDecimal(1).equals(envCrusherPmtCategoriesVORowImpl.getCategoryId())) {
                        envCrusherPermitTVORowImpl.setIsCrusherInstallationT(pIsReadOnly);
                        envCrusherPmtCategoriesVORowImpl.setCheckFlagT(pCheckFlag);
                        envCrusherPmtCategoriesVORowImpl.setIsReadonlyFlagT(pIsReadOnly);
                        break;
                    }
                }
            }
            envCrusherPermitCategoriesESI.closeRowSetIterator();
        }
    }

    public void initCrusherPermitDetails() {
        // Add event code here...
        try {
            String lcSource = null;
            lcSource = (String) ADFUtils.getPageFlowScopeAttribute("pSource");
            String lcFromOutcome = null;
            String lcTaskflowLoaded = null;
            BigDecimal lcRequestId = null;
            lcFromOutcome = (String) ADFUtils.getPageFlowScopeAttribute("pFromOutcome");
            lcTaskflowLoaded = (String) ADFUtils.getBoundAttributeValue("IsTaskFlowLoadedT");
            lcRequestId = (BigDecimal) ADFUtils.getPageFlowScopeAttribute("pRequestId");
            if (null != lcFromOutcome) {
                //            if(!"CRUSHER_SUBMIT".equals(lcFromOutcome)){
                ////            ("MYREQUEST".equals(lcFromOutcome) || "SEARCH_REQUESTS".equals(lcFromOutcome) ||
                ////             "SEARCH_CRUSHER_PERMITSs".equals(lcFromOutcome))) {
                //
                //        }
                if (null == lcRequestId) {
                    if (!("CRUSHER_SUBMIT".equals(lcFromOutcome))) {
                        ADFUtils.putPageFlowScopeAttribute("pIsReadOnly", "Y");
                    }
                    if (null == lcTaskflowLoaded) {
                        ADFUtils.setBoundAttributeValue("IsTaskFlowLoadedT", "Y");
                        ADFUtils.executeOperationBinding("initializeCrusherPermitDetails");
                    }
                } else {
                    ADFUtils.putPageFlowScopeAttribute("pIsReadOnly", "Y");
                    ADFUtils.executeOperationBinding("filterCrusherPermitRequestDetails");
                }
            }
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }
    }

    public void onContractorSearchActionListener(ActionEvent actionEvent) {
        // Add event code here...
        EnvCrusherPermitDetailsVORowImpl envCrusherPermitDetailsVORowImpl = null;
        envCrusherPermitDetailsVORowImpl = (EnvCrusherPermitDetailsVORowImpl) ADFUtils.findIterator("EnvCrusherPermitDetailsVO1Iterator")
                                                                                      .getViewObject()
                                                                                      .first();
        if (null != envCrusherPermitDetailsVORowImpl) {
            System.out.println("" + envCrusherPermitDetailsVORowImpl.getContractorEIDT());
            if (null != envCrusherPermitDetailsVORowImpl.getContractorEIDT() ||
                (null != envCrusherPermitDetailsVORowImpl.getContractorCPNoT() ||
                 null != envCrusherPermitDetailsVORowImpl.getContractorCRNoT())) {
                CmnRegisteredCompaniesRVORowImpl cmnRegisteredCompaniesRVORowImpl = null;
                cmnRegisteredCompaniesRVORowImpl =
                    (CmnRegisteredCompaniesRVORowImpl) ADFUtils.executeOperationBinding("doFilterRegisterCompanies");
                if (null != cmnRegisteredCompaniesRVORowImpl) {
                    envCrusherPermitDetailsVORowImpl.setContractorNameArT(cmnRegisteredCompaniesRVORowImpl.getNameAr());
                    envCrusherPermitDetailsVORowImpl.setContractorNameEnT(cmnRegisteredCompaniesRVORowImpl.getNameEn());
                    envCrusherPermitDetailsVORowImpl.setContractorEIDTypeIdT(null !=
                                                                             cmnRegisteredCompaniesRVORowImpl.getTypeId() ?
                                                                             cmnRegisteredCompaniesRVORowImpl.getTypeId()
                                                                             .toString() : null);
                    envCrusherPermitDetailsVORowImpl.setContractorEIDTypeNameArT(cmnRegisteredCompaniesRVORowImpl.getCompanyTypesNameAr());
                    envCrusherPermitDetailsVORowImpl.setContractorEIDTypeNameEnT(cmnRegisteredCompaniesRVORowImpl.getCompanyTypesNameEn());
                    envCrusherPermitDetailsVORowImpl.setContractorId(cmnRegisteredCompaniesRVORowImpl.getId());
                }
            } else if (null == envCrusherPermitDetailsVORowImpl.getContractorEIDT()) {
                NaturalResourcesUtils.showErrorMessageFromResourceBundle("CONTRACTOR_EID_REQUIRED_MSG");
            } else if (null == envCrusherPermitDetailsVORowImpl.getContractorCPNoT() ||
                       null == envCrusherPermitDetailsVORowImpl.getContractorCRNoT()) {
                NaturalResourcesUtils.showErrorMessageFromResourceBundle("CONTRACTOR_CP_NO_OR_CR_NO_REQUIRED_MSG");
            }

        }
    }


    public void onEnvPermitSearchActionListener(ActionEvent actionEvent) {
        // Add event code here...
        String lcEnvPermitNo = null;
        Date lcEnvPermitNoExpDate = null;
        BigDecimal lcEnvPermitId = null;
        lcEnvPermitNo = (String) ADFUtils.getBoundAttributeValue("EnvPermitNoT");
        lcEnvPermitNoExpDate = (Date) ADFUtils.getBoundAttributeValue("EnvPermitNoExpDateT");
        if (null != lcEnvPermitNo && null != lcEnvPermitNoExpDate) {
            lcEnvPermitId = (BigDecimal) ADFUtils.executeOperationBinding("doFilterEnvPermitDetails");
            if (null != lcEnvPermitId) {
                ADFUtils.setBoundAttributeValue("EnvironmentalPermitId", lcEnvPermitId);
            }
        } else if (null == lcEnvPermitNo) {
            NaturalResourcesUtils.showErrorMessageFromResourceBundle("ENV_PERMIT_NO_REQUIRED_MSG");
        } else if (null == lcEnvPermitNoExpDate) {
            NaturalResourcesUtils.showErrorMessageFromResourceBundle("ENV_PERMIT_DATE_REQUIRED_MSG");
        }
    }

    public void onEnvPermitClearActionListener(ActionEvent actionEvent) {
        // Add event code here...
        ADFUtils.setBoundAttributeValue("EnvPermitNoT", null);
        ADFUtils.setBoundAttributeValue("EnvPermitNoExpDateT", null);
        ADFUtils.findIterator("EnvPermitDetailsRVO1Iterator")
                .getViewObject()
                .executeEmptyRowSet();

    }

    public void onLocationCoordinatesSelectMapValueChangeListener(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        if (null != valueChangeEvent.getNewValue()) {
            System.out.println("New value " + valueChangeEvent.getNewValue());
            String rawLocationCodinates = null;
            String longitude = null;
            String latitude = null;
            String[] locationCoArray = null;
            rawLocationCodinates = (String) valueChangeEvent.getNewValue();
            if (null != rawLocationCodinates) {
                locationCoArray = rawLocationCodinates.split(";");
                if (null != locationCoArray && 4 >= locationCoArray.length) {
                    CustomComponentsMiscUtils.showErrorMessageFromResourceBundle(CustomComponentConstants.PLEASE_SELECT_LOCATION_COORDINATE_MSG);
                    return;
                } else {
                    int index = 1;
                    for (String locationCoordinate : locationCoArray) {
                        if (null != locationCoordinate) {
                            longitude = locationCoordinate.substring(1, locationCoordinate.indexOf(","));
                            latitude =
                                locationCoordinate.substring(locationCoordinate.indexOf(",") + 1,
                                                             locationCoordinate.length() - 1);
                            if ((null != longitude && StringUtils.isNumeric(longitude)) &&
                                (null != latitude && StringUtils.isNumeric(latitude))) {
                                ADFUtils.executeOperationBinding("EnvCrusherLocCoordinatesCreateInsert");
                                ADFUtils.setBoundAttributeValue("Longtitude", new BigDecimal(longitude));
                                ADFUtils.setBoundAttributeValue("Latitude", new BigDecimal(latitude));
                                ADFUtils.setBoundAttributeValue("CoordinateOrder", new BigDecimal(index));
                                ++index;
                            }
                        }

                    }
                    ADFUtils.findIterator("EnvCrusherLocCoordinatesVO1Iterator").executeQuery();
                }
            }


        }
    }

    public void onPermitPrintActionListener(ActionEvent actionEvent) {
        // Add event code here...
        System.out.println("Print");
    }


    public EnvNewCrusherPmtRequestTO getCrusherPmtRequestsTO(String pAccessMode, String pPersonQid) {
        EnvCrusherPermitDetailsVORowImpl envCrusherPermitDetailsVORowImpl = null;
        envCrusherPermitDetailsVORowImpl = (EnvCrusherPermitDetailsVORowImpl) ADFUtils.findIterator("EnvCrusherPermitDetailsVO1Iterator")
                                                                                      .getViewObject()
                                                                                      .first();
        EnvNewCrusherPmtRequestTO envNewCrusherPmtRequestTO = null;
        envNewCrusherPmtRequestTO = new EnvNewCrusherPmtRequestTO();
        envNewCrusherPmtRequestTO.getBeneficiaries().addAll(getEnvCrusherBeneficiariesList());
        envNewCrusherPmtRequestTO.getAttachments().addAll(getEnvCrusherAttachmentList());
        envNewCrusherPmtRequestTO.getCrusherLocList().addAll(getEnvCrusherLocCoordinatesList());
        envNewCrusherPmtRequestTO.getCrushPmtCategoryList().addAll(getEnvCrusherPermitCategoriesList());
        EnvCrusherPermitDetailsTO envCrusherPermitDetailsTO = null;
        envCrusherPermitDetailsTO = getCrusherPmtDetails(envCrusherPermitDetailsVORowImpl);
        envNewCrusherPmtRequestTO.setCrusherPmtDtls(envCrusherPermitDetailsTO);
        envNewCrusherPmtRequestTO.setPermitDuration(envCrusherPermitDetailsVORowImpl.getPermitDurationNewT());
        EntitiesTO applicant = null;
        applicant = entitiesTOData();
        envNewCrusherPmtRequestTO =
            (EnvNewCrusherPmtRequestTO) transactionalWSRequestData(envNewCrusherPmtRequestTO, applicant, pAccessMode,
                                                                   pPersonQid);
        return envNewCrusherPmtRequestTO;
    }

    public EnvRenewCrusherPmtRequestTO getRenewCrusherPmtRequestsTO(String pAccessMode, String pPersonQid,
                                                                    EnvCrusherPermitsVORowImpl envCrusherPermitsVORowImpl) {
        EnvCrusherPermitDetailsVORowImpl envCrusherPermitDetailsVORowImpl = null;
        envCrusherPermitDetailsVORowImpl = (EnvCrusherPermitDetailsVORowImpl) ADFUtils.findIterator("EnvCrusherPermitDetailsVO1Iterator")
                                                                                      .getViewObject()
                                                                                      .first();
        EnvRenewCrusherPmtRequestTO envRenewCrusherPmtRequestTO = null;
        envRenewCrusherPmtRequestTO = new EnvRenewCrusherPmtRequestTO();
        envRenewCrusherPmtRequestTO.getBeneficiaries().addAll(getEnvCrusherBeneficiariesList());
        envRenewCrusherPmtRequestTO.getAttachments().addAll(getEnvCrusherAttachmentList());
        if (null != envCrusherPermitDetailsVORowImpl && null != envCrusherPermitDetailsVORowImpl.getId()) {
            envRenewCrusherPmtRequestTO.setPermitId(envCrusherPermitsVORowImpl.getId().toString());
        }
        envRenewCrusherPmtRequestTO.setPermitDuration(envCrusherPermitDetailsVORowImpl.getPermitDurationRenewT());
        EntitiesTO applicant = null;
        applicant = entitiesTOData();
        envRenewCrusherPmtRequestTO =
            (EnvRenewCrusherPmtRequestTO) transactionalWSRequestData(envRenewCrusherPmtRequestTO, applicant,
                                                                     pAccessMode, pPersonQid);
        return envRenewCrusherPmtRequestTO;
    }


    public EnvAmdCrusherPmtRequestTO getModifyCrusherPmtRequestsTO(String pAccessMode, String pPersonQid) {
        EnvCrusherPermitDetailsVORowImpl envCrusherPermitDetailsVORowImpl = null;
        envCrusherPermitDetailsVORowImpl = (EnvCrusherPermitDetailsVORowImpl) ADFUtils.findIterator("EnvCrusherPermitDetailsVO1Iterator")
                                                                                      .getViewObject()
                                                                                      .first();
        EnvAmdCrusherPmtRequestTO envAmdCrusherPmtRequestTO = null;
        envAmdCrusherPmtRequestTO = new EnvAmdCrusherPmtRequestTO();
        envAmdCrusherPmtRequestTO.getBeneficiaries().addAll(getEnvCrusherBeneficiariesList());
        envAmdCrusherPmtRequestTO.getAttachments().addAll(getEnvCrusherAttachmentList());
        envAmdCrusherPmtRequestTO.getCrusherLocList().addAll(getEnvCrusherLocCoordinatesList());
        envAmdCrusherPmtRequestTO.getCrushPmtCategoryList().addAll(getEnvCrusherPermitCategoriesList());
        EnvCrusherPermitDetailsTO envCrusherPermitDetailsTO = null;
        envCrusherPermitDetailsTO = getCrusherPmtDetails(envCrusherPermitDetailsVORowImpl);
        envAmdCrusherPmtRequestTO.setCrusherPmtDtls(envCrusherPermitDetailsTO);
        if (null != envCrusherPermitDetailsVORowImpl && null != envCrusherPermitDetailsVORowImpl.getId()) {
            envAmdCrusherPmtRequestTO.setPermitId(envCrusherPermitDetailsVORowImpl.getId().toString());
        }
        EntitiesTO applicant = null;
        applicant = entitiesTOData();
        envAmdCrusherPmtRequestTO =
            (EnvAmdCrusherPmtRequestTO) transactionalWSRequestData(envAmdCrusherPmtRequestTO, applicant, pAccessMode,
                                                                   pPersonQid);
        return envAmdCrusherPmtRequestTO;
    }


    public EnvCrusherPermitDetailsTO getCrusherPmtDetails(EnvCrusherPermitDetailsVORowImpl envCrusherPermitDetailsVORowImpl) {
        EnvCrusherPermitDetailsTO envCrusherPermitDetailsTO = null;
        envCrusherPermitDetailsTO = new EnvCrusherPermitDetailsTO();
        if (null != envCrusherPermitDetailsVORowImpl) {
            envCrusherPermitDetailsTO.setContractorId(null != envCrusherPermitDetailsVORowImpl.getContractorId() ?
                                                      envCrusherPermitDetailsVORowImpl.getContractorId().toString() :
                                                      null);
            envCrusherPermitDetailsTO.setContractorMobNo(envCrusherPermitDetailsVORowImpl.getContractorMobileNo());
            envCrusherPermitDetailsTO.setCrushersCount(null != envCrusherPermitDetailsVORowImpl.getCrushersCount() ?
                                                       envCrusherPermitDetailsVORowImpl.getCrushersCount().toString() :
                                                       null);
            envCrusherPermitDetailsTO.setDebrisInventoryId(null !=
                                                           envCrusherPermitDetailsVORowImpl.getDebrisInventoryId() ?
                                                           envCrusherPermitDetailsVORowImpl.getDebrisInventoryId()
                                                           .toString() : null);
            BigDecimal envPermitId = null;
            envPermitId = envCrusherPermitDetailsVORowImpl.getEnvironmentalPermitId();
            if (null != envPermitId) {
                envCrusherPermitDetailsTO.setEnvPermitId(envPermitId.toString());
            }
            envCrusherPermitDetailsTO.setSiftingUnitsCount(null !=
                                                           envCrusherPermitDetailsVORowImpl.getSiftingUnitsCount() ?
                                                           envCrusherPermitDetailsVORowImpl.getSiftingUnitsCount()
                                                           .toString() : null);
        }
        return envCrusherPermitDetailsTO;
    }

    private String domainDateToString(oracle.jbo.domain.Date pUtilDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        java.util.Date date = pUtilDate.dateValue();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return sdf.format(cal.getTime());
    }


    //APPLICANT ENTRY

    private EntitiesTO entitiesTOData() {
        EntitiesTO applicantEntitiesTO = null;
        applicantEntitiesTO = new EntitiesTO();
        CmnApplicantTVORowImpl cmnApplicantTVORowImpl = null;
        cmnApplicantTVORowImpl = (CmnApplicantTVORowImpl) ADFUtils.findIterator("CmnApplicantTVO1Iterator")
                                                                  .getViewObject()
                                                                  .first();
        if (null != cmnApplicantTVORowImpl) {
            applicantEntitiesTO.setPersonQId(cmnApplicantTVORowImpl.getQIDT());
            applicantEntitiesTO.setTypeId(new Long(1));
            applicantEntitiesTO.setMobileNo(cmnApplicantTVORowImpl.getMobileNumberT());
            applicantEntitiesTO.setEmail(cmnApplicantTVORowImpl.getEmailT());
            applicantEntitiesTO.setNameAr(cmnApplicantTVORowImpl.getNameArT());
            applicantEntitiesTO.setNameEn(cmnApplicantTVORowImpl.getNameEnT());
            applicantEntitiesTO.setIsDeceased("N");
            applicantEntitiesTO.setFax(cmnApplicantTVORowImpl.getFaxT());
            applicantEntitiesTO.setPoBox(cmnApplicantTVORowImpl.getPoBoxT());
        }
        return applicantEntitiesTO;
    }
    //BENEFICIARY Details
    private List<EntitiesTO> getEnvCrusherBeneficiariesList() {
        EntitiesTO beneficiariesEnitiesTO = new EntitiesTO();
        List<EntitiesTO> beneficiariesList = null;
        beneficiariesList = new ArrayList<EntitiesTO>();
        CmnBeneficiaryTVORowImpl cmnBeneficiaryTVORowImpl = null;
        cmnBeneficiaryTVORowImpl = (CmnBeneficiaryTVORowImpl) ADFUtils.findIterator("CmnBeneficiaryTVO1Iterator")
                                                                      .getViewObject()
                                                                      .first();
        if (null != cmnBeneficiaryTVORowImpl) {
            beneficiariesEnitiesTO.setTypeId(null != cmnBeneficiaryTVORowImpl.getEntityTypeIdT() ?
                                             cmnBeneficiaryTVORowImpl.getEntityTypeIdT().longValue() : 1);
            beneficiariesEnitiesTO.setEstablishmentId(cmnBeneficiaryTVORowImpl.getEIDT());
            beneficiariesEnitiesTO.setCommercialRegistrationNo(cmnBeneficiaryTVORowImpl.getCPNoT());
            beneficiariesEnitiesTO.setCompanyId(cmnBeneficiaryTVORowImpl.getCPNoT());
            beneficiariesEnitiesTO.setCompanyBranchNo(cmnBeneficiaryTVORowImpl.getBranchNoT());
            beneficiariesEnitiesTO.setMobileNo(cmnBeneficiaryTVORowImpl.getMobileNumberT());
            beneficiariesEnitiesTO.setEmail(cmnBeneficiaryTVORowImpl.getEmailT());
            beneficiariesEnitiesTO.setNameAr(cmnBeneficiaryTVORowImpl.getNameArT());
            beneficiariesEnitiesTO.setNameEn(cmnBeneficiaryTVORowImpl.getNameEnT());
            beneficiariesEnitiesTO.setIsDeceased("N");
            beneficiariesEnitiesTO.setFax(cmnBeneficiaryTVORowImpl.getFaxT());
            beneficiariesEnitiesTO.setPoBox(cmnBeneficiaryTVORowImpl.getPoBoxT());
            beneficiariesEnitiesTO.setLandLineNo(cmnBeneficiaryTVORowImpl.getLandLineT());
            beneficiariesEnitiesTO.setDescription(cmnBeneficiaryTVORowImpl.getDescriptionT());
            beneficiariesList.add(beneficiariesEnitiesTO);
        }
        return beneficiariesList;
    }

    //ATTACHMENT DETAILS
    private List<AttachmentTO> getEnvCrusherAttachmentList() {
        List<AttachmentTO> attachmentsList = null;
        attachmentsList = new ArrayList<AttachmentTO>();
        CmnRequestsAttachmentsRVOImpl cmnRequestsAttachmentsRVOImpl = null;
        cmnRequestsAttachmentsRVOImpl =
            (CmnRequestsAttachmentsRVOImpl) ADFUtils.findIterator("CmnRequestsAttachmentsRVO1Iterator").getViewObject();
        RowSetIterator cmnRequestsAttachmentsRSI = null;
        cmnRequestsAttachmentsRSI = cmnRequestsAttachmentsRVOImpl.createRowSetIterator(null);
        if (null != cmnRequestsAttachmentsRSI) {
            while (cmnRequestsAttachmentsRSI.hasNext()) {
                AttachmentTO attachment = new AttachmentTO();
                CmnRequestsAttachmentsRVORowImpl cmnRequestsAttachmentsRVORowImpl = null;
                cmnRequestsAttachmentsRVORowImpl = (CmnRequestsAttachmentsRVORowImpl) cmnRequestsAttachmentsRSI.next();
                if (null != cmnRequestsAttachmentsRVORowImpl &&
                    (null != cmnRequestsAttachmentsRVORowImpl.getFileContentT() &&
                     null != cmnRequestsAttachmentsRVORowImpl.getFileTitleT())) {
                    System.out.println("attachmentCurrRow.getFileTitleT() " +
                                       cmnRequestsAttachmentsRVORowImpl.getFileTitleT());
                    attachment.setTypeId(null != cmnRequestsAttachmentsRVORowImpl.getId() ?
                                         cmnRequestsAttachmentsRVORowImpl.getId().toString() : null);
                    attachment.setContents(cmnRequestsAttachmentsRVORowImpl.getFileContentT().toByteArray());
                    attachment.setMimeType(URLConnection.guessContentTypeFromName(cmnRequestsAttachmentsRVORowImpl.getFileTitleT()));
                    attachment.setTitle(cmnRequestsAttachmentsRVORowImpl.getFileTitleT());
                    attachmentsList.add(attachment);
                }
            }
            cmnRequestsAttachmentsRSI.closeRowSetIterator();
        }
        return attachmentsList;
    }

    private TransactionalWSRequest transactionalWSRequestData(TransactionalWSRequest request, EntitiesTO applicant,
                                                              String pAccessMode, String pPersonQid) {
        request.setApplicant(applicant);
        if (null != pAccessMode && "FRONT".equals(pAccessMode)) {
            request.setChannelId(new Long(MMEConstants.CHANNEL_PORTAL));
            request.setCreatedBy(pPersonQid);
        } else {
            request.setChannelId(new Long(MMEConstants.CHANNEL_SERVICE_CENTER));
            request.setCreatedBy(ADFUtils.getSecurityContextUserName());
        }
        request.setMunicipalityId("1");
        request.setSectorId(new Long(2));
        request.setStatusId(new Long(MMEConstants.REQUEST_STATUS_IN_REVIEW));
        request.setSubmissionUnitId("1");
        request.setBPMOutcome(MMEConstants.BPM_PROCESS_OUTCOME_SUBMIT);
        return request;
    }


    public List<EnvCrusherLocCoordinatesTO> getEnvCrusherLocCoordinatesList() {
        List<EnvCrusherLocCoordinatesTO> envCrusherLocCoordinatesList = null;
        envCrusherLocCoordinatesList = new ArrayList<EnvCrusherLocCoordinatesTO>();
        EnvCrusherLocCoordinatesVOImpl envCrusherLocCoordinatesVOImpl = null;
        envCrusherLocCoordinatesVOImpl =
            (EnvCrusherLocCoordinatesVOImpl) ADFUtils.findIterator("EnvCrusherLocCoordinatesVO1Iterator")
            .getViewObject();
        RowSetIterator envCrusherLocCoordinatesRSI = null;
        envCrusherLocCoordinatesRSI = envCrusherLocCoordinatesVOImpl.createRowSetIterator(null);
        if (null != envCrusherLocCoordinatesRSI) {
            while (envCrusherLocCoordinatesRSI.hasNext()) {
                EnvCrusherLocCoordinatesTO envCrusherLocCoordinatesTO = new EnvCrusherLocCoordinatesTO();
                EnvCrusherLocCoordinatesVORowImpl envCrusherLocCoordinatesVORowImpl = null;
                envCrusherLocCoordinatesVORowImpl =
                    (EnvCrusherLocCoordinatesVORowImpl) envCrusherLocCoordinatesRSI.next();
                if (null != envCrusherLocCoordinatesVORowImpl &&
                    (null != envCrusherLocCoordinatesVORowImpl.getLatitude() &&
                     null != envCrusherLocCoordinatesVORowImpl.getLongtitude() &&
                     null != envCrusherLocCoordinatesVORowImpl.getCoordinateOrder())) {
                    envCrusherLocCoordinatesTO.setCoordinateOrder(envCrusherLocCoordinatesVORowImpl.getCoordinateOrder()
                                                                  .toString());
                    envCrusherLocCoordinatesTO.setLatitude(envCrusherLocCoordinatesVORowImpl.getLatitude().toString());
                    envCrusherLocCoordinatesTO.setLongtitude(envCrusherLocCoordinatesVORowImpl.getLongtitude()
                                                             .toString());
                    envCrusherLocCoordinatesList.add(envCrusherLocCoordinatesTO);
                }
            }
            envCrusherLocCoordinatesRSI.closeRowSetIterator();
        }
        return envCrusherLocCoordinatesList;
    }


    public List<EnvCrusherPermitCategoriesTO> getEnvCrusherPermitCategoriesList() {
        List<EnvCrusherPermitCategoriesTO> envCrusherPermitCategoriesList = null;
        envCrusherPermitCategoriesList = new ArrayList<EnvCrusherPermitCategoriesTO>();
        EnvCrusherPmtCategoriesVOImpl envCrusherPmtCategoriesVOImpl = null;
        envCrusherPmtCategoriesVOImpl =
            (EnvCrusherPmtCategoriesVOImpl) ADFUtils.findIterator("EnvCrusherPmtCategoriesVO1Iterator").getViewObject();
        RowSetIterator envCrusherPmtCategoriesRSI = null;
        envCrusherPmtCategoriesRSI = envCrusherPmtCategoriesVOImpl.createRowSetIterator(null);
        if (null != envCrusherPmtCategoriesRSI) {
            while (envCrusherPmtCategoriesRSI.hasNext()) {
                EnvCrusherPermitCategoriesTO envCrusherPermitCategoriesTO = new EnvCrusherPermitCategoriesTO();
                EnvCrusherPmtCategoriesVORowImpl envCrusherPmtCategoriesVORowImpl = null;
                envCrusherPmtCategoriesVORowImpl = (EnvCrusherPmtCategoriesVORowImpl) envCrusherPmtCategoriesRSI.next();
                if ((null != envCrusherPmtCategoriesVORowImpl &&
                     null != envCrusherPmtCategoriesVORowImpl.getCheckFlagT()) &&
                    (null != envCrusherPmtCategoriesVORowImpl.getCategoryId() &&
                     Boolean.TRUE.equals(envCrusherPmtCategoriesVORowImpl.getCheckFlagT()))) {
                    envCrusherPermitCategoriesTO.setCategoryId(envCrusherPmtCategoriesVORowImpl.getCategoryId()
                                                               .toString());
                    envCrusherPermitCategoriesList.add(envCrusherPermitCategoriesTO);
                }
            }
            envCrusherPmtCategoriesRSI.closeRowSetIterator();
        }
        return envCrusherPermitCategoriesList;
    }

    public void onSubmitActionListener(ActionEvent actionEvent) {
        // Add event code here...
        if (null != acknowledgePopupBnd) {
            ResetUtils.reset(acknowledgePopupBnd);
            ADFUtils.showPopup(acknowledgePopupBnd, true);
        }
    }

    public void onLocationCoordinatesAddListener(ActionEvent actionEvent) {
        // Add event code here...
        EnvCrusherLocCoordinatesVORowImpl envCrusherLocCoordinatesVORowImpl = null;
        envCrusherLocCoordinatesVORowImpl =
            (EnvCrusherLocCoordinatesVORowImpl) ADFUtils.findIterator("EnvCrusherLocCoordinatesVO1Iterator")
            .getCurrentRow();
        if (null != envCrusherLocCoordinatesVORowImpl) {
            if (null == envCrusherLocCoordinatesVORowImpl.getLatitude()) {
                CustomComponentsMiscUtils.showErrorMessageFromResourceBundle(CustomComponentConstants.LATITUDE_REQUIRED_MSG);
                return;
            }
            if (null == envCrusherLocCoordinatesVORowImpl.getLongtitude()) {
                CustomComponentsMiscUtils.showErrorMessageFromResourceBundle(CustomComponentConstants.LONGITUDE_REQUIRED_MSG);
                return;
            }
        }
        ADFUtils.executeOperationBinding("EnvCrusherLocCoordinatesCreateInsert");
        long coordinateOrder = 0;
        coordinateOrder = ADFUtils.findIterator("EnvCrusherLocCoordinatesVO1Iterator").getEstimatedRowCount();
        ADFUtils.setBoundAttributeValue("CoordinateOrder", new BigDecimal(coordinateOrder + 1));
    }

    public void validateCrusterSubmitRequestDetails() {

    }

    public void onRecipientSignFileUploadValueChangeListener(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        UploadedFile newValue = null;
        String fileType = null;
        newValue = (UploadedFile) valueChangeEvent.getNewValue();
        if (null != newValue) {
            fileType = newValue.getContentType();
            if (null != fileType) {
                if (fileType.equalsIgnoreCase(CustomComponentConstants.APPLICATION_PDF) ||
                    fileType.equalsIgnoreCase(CustomComponentConstants.IMAGE_PNG) ||
                    fileType.equalsIgnoreCase(CustomComponentConstants.IMAGE_JPEG) ||
                    fileType.equalsIgnoreCase(CustomComponentConstants.IMAGE_JPG)) {
                    // Upload File into Blob Column
                    ADFUtils.setBoundAttributeValue(CustomComponentConstants.FILE_CONTENT_T,
                                                    this.createBlobDomain(newValue));
                    ADFUtils.setBoundAttributeValue(CustomComponentConstants.FILE_TITLE_T, newValue.getFilename());
                } else {
                    CustomComponentsMiscUtils.showErrorMessageFromResourceBundle(CustomComponentConstants.INVALID_FILE_FORMAT);
                    recipientSignUploadFileBnd.resetValue();
                    ResetUtils.reset(recipientSignUploadFileBnd);

                }
            }
        }
    }

    private BlobDomain createBlobDomain(UploadedFile file) {
        InputStream in = null;
        BlobDomain blobDomain = null;
        OutputStream out = null;
        try {
            in = file.getInputStream();
            blobDomain = new BlobDomain();
            out = blobDomain.getBinaryOutputStream();
            byte[] buffer = new byte[8192];
            int bytesRead = 0;
            while ((bytesRead = in.read(buffer, 0, 8192)) != -1) {
                out.write(buffer, 0, bytesRead);
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.fillInStackTrace();
        }
        return blobDomain;
    }

    public void setRecipientSignUploadFileBnd(RichInputFile recipientSignUploadFileBnd) {
        this.recipientSignUploadFileBnd = recipientSignUploadFileBnd;
    }

    public RichInputFile getRecipientSignUploadFileBnd() {
        return recipientSignUploadFileBnd;
    }

    public void setAcknowledgePopupBnd(RichPopup acknowledgePopupBnd) {
        this.acknowledgePopupBnd = acknowledgePopupBnd;
    }

    public RichPopup getAcknowledgePopupBnd() {
        return acknowledgePopupBnd;
    }

    public void onAcknowledgeOkActionListener(ActionEvent actionEvent) {
        // Add event code here...
        String lcAccessMode = null;
        String lcPersonQid = null;
        String lcFromOutcome = null;
        lcAccessMode = (String) ADFUtils.getPageFlowScopeAttribute("pAccessMode");
        lcPersonQid = (String) ADFUtils.getPageFlowScopeAttribute("personQID");
        lcFromOutcome = (String) ADFUtils.getPageFlowScopeAttribute("pFromOutcome");
        NaturalResourcesServiceClient naturalResourcesServiceClient = null;
        TransactionalWSReturn transactionalWSReturn = null;
        EnvCrusherPermitsVORowImpl envCrusherPermitsVORowImpl = null;
        if (null != lcFromOutcome) {
            naturalResourcesServiceClient = new NaturalResourcesServiceClient();
            try {
                if (NaturalResourcesConstants.CRUSHER_SUBMIT.equals(lcFromOutcome)) {
                    EnvNewCrusherPmtRequestTO envNewCrusherPmtRequestTO = null;
                    envNewCrusherPmtRequestTO = getCrusherPmtRequestsTO(lcAccessMode, lcPersonQid);
                    transactionalWSReturn =
                        naturalResourcesServiceClient.submitNewCrusherPermit(envNewCrusherPmtRequestTO);
                } else if (NaturalResourcesConstants.MODIFY_CRUSHER_SUBMIT.equals(lcFromOutcome)) {
                    envCrusherPermitsVORowImpl = (EnvCrusherPermitsVORowImpl) ADFUtils.findIterator("EnvCrusherPermitsVO1Iterator")
                                                                                      .getViewObject()
                                                                                      .first();
                    EnvAmdCrusherPmtRequestTO envAmdCrusherPmtRequestTO = null;
                    envAmdCrusherPmtRequestTO = getModifyCrusherPmtRequestsTO(lcAccessMode, lcPersonQid);
                    transactionalWSReturn =
                        naturalResourcesServiceClient.submitAmendCrusherPermit(envAmdCrusherPmtRequestTO);
                } else if (NaturalResourcesConstants.RENEW_CRUSHER_SUBMIT.equals(lcFromOutcome)) {
                    EnvRenewCrusherPmtRequestTO envRenewCrusherPmtRequestTO = null;
                    envCrusherPermitsVORowImpl = (EnvCrusherPermitsVORowImpl) ADFUtils.findIterator("EnvCrusherPermitsVO1Iterator")
                                                                                      .getViewObject()
                                                                                      .first();
                    envRenewCrusherPmtRequestTO =
                        getRenewCrusherPmtRequestsTO(lcAccessMode, lcPersonQid, envCrusherPermitsVORowImpl);
                    transactionalWSReturn =
                        naturalResourcesServiceClient.submitRenewCrusherPermit(envRenewCrusherPmtRequestTO);
                }
                System.out.println("Error Code: " + transactionalWSReturn.getErrorCode());
                System.out.println("Error Message: " + transactionalWSReturn.getErrorMessage());
                System.out.println("Transaction Id: " + transactionalWSReturn.getTransactionId());
                if (0 != transactionalWSReturn.getTransactionId()) {
                    ADFUtils.putPageFlowScopeAttribute("pTransactionId", transactionalWSReturn.getTransactionId());
                    //   ADFUtils.putPageFlowScopeAttribute("pFromOutcome", NaturalResourcesConstants.SEARCH_REQUESTS);
                    ADFUtils.showPopup(acknowledgePopupBnd, false);
                    ADFUtils.showPopup(confirmationSuccessPopupBnd, true);
                }


            } catch (Throwable e) {
                e.printStackTrace();
                e.getMessage();
            }
        }
    }

    public void onAcknowledgeCancelAction(ActionEvent actionEvent) {
        // Add event code here...
        if (null != acknowledgePopupBnd) {
            ResetUtils.reset(acknowledgePopupBnd);
            ADFUtils.showPopup(acknowledgePopupBnd, false);
        }
    }

    public void setConfirmationSuccessPopupBnd(RichPopup confirmationSuccessPopupBnd) {
        this.confirmationSuccessPopupBnd = confirmationSuccessPopupBnd;
    }

    public RichPopup getConfirmationSuccessPopupBnd() {
        return confirmationSuccessPopupBnd;
    }

    public void onClearTransaction() {
        // Add event code here...
        ADFUtils.executeOperationBinding("NaturalResourcesRollback");
        ADFUtils.executeOperationBinding("CustomComponentRollback");
    }

    public void onAgreeValueChangeListener(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        if (null != valueChangeEvent.getNewValue()) {
            valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
            ADFUtils.putViewScopeAttribute("pAccessMode", valueChangeEvent.getNewValue());
        }
        reRenderComponents(new String[] { "mfa2" });
    }

    public void onPermitClearActionListener(ActionEvent actionEvent) {
        // Add event code here...
        ADFUtils.executeOperationBinding("clearPermitDetails");
    }

    public void onPermitSearchActionListener(ActionEvent actionEvent) {
        // Add event code here...
        String lcPermit = null;
        Date lcPermitExpDate = null;
        lcPermit = (String) ADFUtils.getBoundAttributeValue("PermitNoT");
        lcPermitExpDate = (Date) ADFUtils.getBoundAttributeValue("PermitExpiryDateT");
        if (null != lcPermit && null != lcPermitExpDate) {
            ADFUtils.executeOperationBinding("doEnvRenewCrusherPermitSearch");
        } else if (null == lcPermit) {
            NaturalResourcesUtils.showErrorMessageFromResourceBundle(NaturalResourcesConstants.PERMIT_NO_REQUIRED_MSG);
        } else if (null == lcPermitExpDate) {
            NaturalResourcesUtils.showErrorMessageFromResourceBundle(NaturalResourcesConstants.PERMIT_DATE_REQUIRED_MSG);
        }

    }
}
