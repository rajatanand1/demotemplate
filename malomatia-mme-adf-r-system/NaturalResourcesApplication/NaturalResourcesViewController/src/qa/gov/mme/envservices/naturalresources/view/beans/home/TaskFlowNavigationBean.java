package qa.gov.mme.envservices.naturalresources.view.beans.home;

import java.io.Serializable;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.servlet.http.HttpServletRequest;

import oracle.adf.controller.TaskFlowId;

import oracle.jbo.domain.Timestamp;

import qa.gov.mme.common.beans.CommonBaseBean;
import qa.gov.mme.common.utils.ADFUtils;
import qa.gov.mme.common.utils.UIUtils;
import qa.gov.mme.envservices.naturalresources.view.constant.NaturalResourcesConstants;

public class TaskFlowNavigationBean extends CommonBaseBean implements Serializable {
    @SuppressWarnings("compatibility:-6210846999608112018")
    private static final long serialVersionUID = 1L;
    private String currentPageTitle = "HomePage";
    private transient String accessMode = NaturalResourcesConstants.DEFAULT_ACCESS_MODE;
        private String taskFlowId = NaturalResourcesConstants.HOME_TF;
//    private String taskFlowId = NaturalResourcesViewControllerConstants.DEBRIS_INVENTORIES_TF;

    public void setCurrentPageTitle(String currentPageTitle) {
        this.currentPageTitle = currentPageTitle;
    }

    public String getCurrentPageTitle() {
        return currentPageTitle;
    }

    public TaskFlowNavigationBean() {
        initParams();
    }


    public void initParams() {
        // Add event code here...
        FacesContext fctx = FacesContext.getCurrentInstance();
        ExternalContext ectx = fctx.getExternalContext();
        HttpServletRequest request = (HttpServletRequest) ectx.getRequest();
        String lcAccessMode = null;
        lcAccessMode = request.getServletContext().getInitParameter("pAccessModeParam");
        if (null != lcAccessMode) {
            this.setAccessMode(lcAccessMode);
        } else {
            this.setAccessMode(NaturalResourcesConstants.DEFAULT_ACCESS_MODE);
        }
    }

    private Timestamp getCurrentTimeStamp() {
        return new Timestamp(System.currentTimeMillis());
    }

    public String onHomeAction() {
        // Add event code here...
        this.setDynamicTaskFlowId(NaturalResourcesConstants.HOME_TF);
        ADFUtils.putPageFlowScopeAttribute("pTimeStamp", this.getCurrentTimeStamp());
        return null;
    }

    public String onPrivateInboxAction() {
        // Add event code here...
        this.setDynamicTaskFlowId(NaturalResourcesConstants.INBOX_TF);
        ADFUtils.putPageFlowScopeAttribute("pFromOutcome", NaturalResourcesConstants.PRIVATE_INBOX_PARAM);
        ADFUtils.putPageFlowScopeAttribute("pTimeStamp", this.getCurrentTimeStamp());
        return null;
    }

    public String onPublicInboxAction() {
        // Add event code here...
        this.setDynamicTaskFlowId(NaturalResourcesConstants.INBOX_TF);
        ADFUtils.putPageFlowScopeAttribute("pFromOutcome", NaturalResourcesConstants.PUBLIC_INBOX_PARAM);
        ADFUtils.putPageFlowScopeAttribute("pTimeStamp", this.getCurrentTimeStamp());
        return null;
    }

    public String onRequestSearchAction() {
        // Add event code here...
        this.setDynamicTaskFlowId(NaturalResourcesConstants.INBOX_TF);
        ADFUtils.putPageFlowScopeAttribute("pFromOutcome", NaturalResourcesConstants.PUBLIC_INBOX_PARAM);
        ADFUtils.putPageFlowScopeAttribute("pTimeStamp", this.getCurrentTimeStamp());
        return null;
    }

    public String onSearchDebrisInventories() {
        // Add event code here...
        this.setDynamicTaskFlowId(NaturalResourcesConstants.DEBRIS_INVENTORIES_TF);
        ADFUtils.putPageFlowScopeAttribute("pFromOutcome", NaturalResourcesConstants.PUBLIC_INBOX_PARAM);
        ADFUtils.putPageFlowScopeAttribute("pTimeStamp", this.getCurrentTimeStamp());
        return null;
    }

    public String onTaskAssignmentAction() {
        // Add event code here...
        this.setDynamicTaskFlowId(NaturalResourcesConstants.DEBRIS_INVENTORIES_PARAM);
        ADFUtils.putPageFlowScopeAttribute("pFromOutcome", NaturalResourcesConstants.DEBRIS_INVENTORIES_TF);
        ADFUtils.putPageFlowScopeAttribute("pTimeStamp", this.getCurrentTimeStamp());
        return null;
    }

    public void setTitleFromResourceBundle(String title) {
        if (title.equals("HomePage")) {
            setCurrentPageTitle(title);
        } else {
            setCurrentPageTitle(UIUtils.messageFromResourceBundle(NaturalResourcesConstants.NATURAL_RESOURCES_VIEW_CONTROLLER_BUNDLE,
                                                                  title));
        }
        //refreshPage();
    }

    public TaskFlowId getDynamicTaskFlowId() {
        //  return TaskFlowId.parse(taskFlowId);
        return TaskFlowId.parse(taskFlowId);
    }

    public void setDynamicTaskFlowId(String taskFlowId) {
        this.taskFlowId = taskFlowId;
    }

    public String onQuarriesDataAdmin() {
        this.setDynamicTaskFlowId("/WEB-INF/naturalresources/taskflows/quarries/QuarriesDataAdminTF.xml#QuarriesDataAdminTF");
        return null;
    }

    public void setAccessMode(String accessMode) {
        this.accessMode = accessMode;
    }

    public String getAccessMode() {
        return accessMode;
    }

}
