package qa.gov.mme.envservices.naturalresources.view.beans.searchrequests;

import javax.faces.event.ActionEvent;

import oracle.binding.OperationBinding;

import qa.gov.mme.common.beans.CommonBaseBean;
import qa.gov.mme.common.utils.ADFUtils;

public class NaturalResourcesSearchRequestsBean extends CommonBaseBean {
    @SuppressWarnings("compatibility:-2792166360890546748")
    private static final long serialVersionUID = 1L;

    public NaturalResourcesSearchRequestsBean() {
        super();
    }

    public void onSearchRequestActionListener(ActionEvent actionEvent) {
        System.out.println("Calling Search");
        OperationBinding filterRequestSearchQueryOperBnd = ADFUtils.findOperation("filterRequestSearchQuery");
        filterRequestSearchQueryOperBnd.execute();
        reRenderComponents(new String[] { "t1" });
    }

    public String onClearSearchRequestActionListener() {
        reRenderComponents(new String[] { "mfp3" });
        return null;
    }
}
