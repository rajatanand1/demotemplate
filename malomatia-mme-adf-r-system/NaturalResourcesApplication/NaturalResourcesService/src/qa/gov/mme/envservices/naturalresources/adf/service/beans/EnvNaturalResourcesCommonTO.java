package qa.gov.mme.envservices.naturalresources.adf.service.beans;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import oracle.jbo.ApplicationModule;
import oracle.jbo.Row;
import oracle.jbo.ViewObject;

import qa.gov.mme.common.core.service.bean.request.TransactionalWSRequest;

public class EnvNaturalResourcesCommonTO extends TransactionalWSRequest{
    public EnvNaturalResourcesCommonTO() {
        super();
    }
       
    public static Date getDateFromString(String dateStr) {
        SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date utilDate = null;
        try {
            if(null != dateStr && !dateStr.isEmpty() && !dateStr.trim().isEmpty()) {
                utilDate = sdf.parse(dateStr);   
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return utilDate;
    }
    
    public void updateRequestBeneficiaryId(ApplicationModule am,Long benfId) {
        ViewObject vo = am.findViewObject("CmnRequests1");
        Row r = vo.getCurrentRow();
        r.setAttribute("BeneficiaryId", benfId);
        this.setBeneficiaryId(benfId);
    }
    
}
