package qa.gov.mme.envservices.falconpassport.adf.service.beans.request;

import javax.xml.bind.annotation.XmlElement;

import java.io.Serializable;

public class FalconPassportDetailsTO implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    public FalconPassportDetailsTO() {
        super();
    }
    
    @XmlElement
    protected String classId;
    
    @XmlElement
    protected String categoryId;
    
    @XmlElement
    protected String originCountryId;
    
    @XmlElement
    protected String genderId;
    
    @XmlElement
    protected String sourceId;
    
    @XmlElement
    protected String pitNo;
    
    @XmlElement
    protected String citesPermitNo;


    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassId() {
        return classId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setOriginCountryId(String originCountryId) {
        this.originCountryId = originCountryId;
    }

    public String getOriginCountryId() {
        return originCountryId;
    }

    public void setGenderId(String genderId) {
        this.genderId = genderId;
    }

    public String getGenderId() {
        return genderId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setPitNo(String pitNo) {
        this.pitNo = pitNo;
    }

    public String getPitNo() {
        return pitNo;
    }

    public void setCitesPermitNo(String citesPermitNo) {
        this.citesPermitNo = citesPermitNo;
    }

    public String getCitesPermitNo() {
        return citesPermitNo;
    }

}
