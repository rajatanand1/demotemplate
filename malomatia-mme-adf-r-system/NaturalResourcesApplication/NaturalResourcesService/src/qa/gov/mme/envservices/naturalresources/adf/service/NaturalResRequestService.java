package qa.gov.mme.envservices.naturalresources.adf.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import qa.gov.mme.common.core.service.CoreRequestService;
import qa.gov.mme.common.core.service.bean.response.TransactionalWSReturn;
import qa.gov.mme.common.core.service.util.BCUtils;
import qa.gov.mme.envservices.naturalresources.adf.service.beans.constant.EnvNaturalResServiceConstant;
import qa.gov.mme.envservices.naturalresources.adf.service.beans.request.EnvNewCrusherPmtRequestTO;
import qa.gov.mme.envservices.naturalresources.model.bc.app.NaturalResourcesServiceAMImpl;


@WebService(serviceName = "NaturalResRequestService", portName = "NaturalResRequestService",
            wsdlLocation = "WEB-INF/wsdl/NaturalResRequestService.wsdl")
public class NaturalResRequestService extends CoreRequestService {
    @SuppressWarnings("compatibility:1171231227198340388")
    private static final long serialVersionUID = 1L;

    public NaturalResRequestService() {
        super();
    }


    @WebMethod
    public TransactionalWSReturn submitNewCrusherPermit(@WebParam(name = "request") EnvNewCrusherPmtRequestTO request) {
        NaturalResourcesServiceAMImpl am =
            (NaturalResourcesServiceAMImpl) BCUtils.getApplicationModule(EnvNaturalResServiceConstant.NR_SERVICES_AM_DEF,
                                                                         EnvNaturalResServiceConstant.NR_SERVICES_AM_CONFIG);
        return submitRequest(am, request);
    }


}
