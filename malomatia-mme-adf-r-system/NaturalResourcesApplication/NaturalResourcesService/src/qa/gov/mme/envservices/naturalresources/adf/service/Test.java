package qa.gov.mme.envservices.naturalresources.adf.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import java.math.BigDecimal;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;

import java.util.List;

import oracle.jbo.ViewObject;

import qa.gov.mme.common.constants.MMEConstants;
import qa.gov.mme.common.core.service.util.BCUtils;

import qa.gov.mme.common.core.service.bean.AttachmentTO;
import qa.gov.mme.common.core.service.bean.EntitiesTO;

import qa.gov.mme.common.core.service.bean.request.TransactionalWSRequest;
import qa.gov.mme.common.core.service.bean.response.TransactionalWSReturn;
import qa.gov.mme.envservices.naturalresources.adf.service.beans.constant.EnvNaturalResServiceConstant;
import qa.gov.mme.envservices.naturalresources.adf.service.beans.request.EnvCrusherLocCoordinatesTO;
import qa.gov.mme.envservices.naturalresources.adf.service.beans.request.EnvCrusherPermitCategoriesTO;
import qa.gov.mme.envservices.naturalresources.adf.service.beans.request.EnvCrusherPermitDetailsTO;
import qa.gov.mme.envservices.naturalresources.adf.service.beans.request.EnvNewCrusherPmtRequestTO;
import qa.gov.mme.envservices.naturalresources.model.bc.app.NaturalResourcesServiceAMImpl;

public class Test {
    private String currentDateStr;
    private String amDef = EnvNaturalResServiceConstant.NR_SERVICES_AM_DEF;
    private String config = EnvNaturalResServiceConstant.NR_SERVICES_AM_CONFIG;

    public Test() {
        super();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        this.currentDateStr = sdf.format(new Date());
        System.out.println(this.currentDateStr);
    }

    public static void main(String[] args) {
        NaturalResRequestService client = new NaturalResRequestService();
        TransactionalWSReturn result = null;
        Test cc = new Test();
        result = client.submitNewCrusherPermit(cc.getEnvNewCrusherPmtRequestTO());

        System.out.println(result.getTransactionId());
        System.out.println("Error Code:" + result.getErrorCode());
        System.out.println("Error Message:" + result.getErrorMessage());
    }


    public EnvNewCrusherPmtRequestTO getEnvNewCrusherPmtRequestTO() {
        EntitiesTO applicant = entitiesTOData();
        List<EntitiesTO> beneficiaries = listBeneficiaries();
        //   List<AttachmentTO> listAttachment = listAttachment(new Long(56));
        EnvNewCrusherPmtRequestTO ReqT = new EnvNewCrusherPmtRequestTO();
        ReqT.setBeneficiaryId(beneficiaries.get(0).getId());
        //   ReqT.setAttachments(listAttachment);
        ReqT.setCreatedBy("zaid");
        ReqT.setPermitDuration("20");

        System.out.println("Beneficiary size:" + beneficiaries.size());
        ReqT.setBeneficiaries(beneficiaries);

        EnvCrusherPermitDetailsTO pmtT = new EnvCrusherPermitDetailsTO();
        pmtT.setContractorId("1002");
        pmtT.setContractorMobNo("12345678");
        pmtT.setCrushersCount("2");
        pmtT.setDebrisInventoryId("2");
        pmtT.setEnvPermitId("1");
        pmtT.setSiftingUnitsCount("29");
        pmtT.setCrushersCount("40");

        ReqT.setCrusherPmtDtls(pmtT);

        List<EnvCrusherLocCoordinatesTO> locList = new ArrayList<EnvCrusherLocCoordinatesTO>();
        EnvCrusherLocCoordinatesTO locs = new EnvCrusherLocCoordinatesTO();
        locs.setLongtitude("1");
        locs.setLatitude("20");
        locs.setCoordinateOrder("90");

        locList.add(locs);
        ReqT.setCrusherLocList(locList);

        List<EnvCrusherPermitCategoriesTO> catList = new ArrayList<EnvCrusherPermitCategoriesTO>();
        EnvCrusherPermitCategoriesTO catT = new EnvCrusherPermitCategoriesTO();
        catT.setCategoryId("1");

        catList.add(catT);
        ReqT.setCrushPmtCategoryList(catList);
        ReqT.setChannelId(Long.valueOf(MMEConstants.CHANNEL_SERVICE_CENTER));
        ReqT = (EnvNewCrusherPmtRequestTO) transactionalWSRequestData(ReqT, applicant);
        return ReqT;
    }


    private EntitiesTO entitiesTOData() {
        EntitiesTO entity = new EntitiesTO();
        entity.setCreatedBy("value");
        entity.setMobileNo("66953867");
        entity.setNameAr("محم د رمضان");
        entity.setNameEn("Mohammad Ramadan");
        entity.setPersonQId("28476003071");
        entity.setTypeId(new Long(1));
        entity.setIsDeceased("N");
        return entity;
    }

    private List<EntitiesTO> listBeneficiaries() {
        List<EntitiesTO> listBenf = new ArrayList<EntitiesTO>();
        EntitiesTO benf = new EntitiesTO();
        benf.setCreatedBy("value");
        benf.setMobileNo("66953867");
        benf.setNameAr("محم د رمضان");
        benf.setNameEn("Mohammad Ramadan");
        benf.setPersonQId("28476003071");
        benf.setTypeId(new Long(1));
        benf.setIsDeceased("N");
        listBenf.add(benf);

        //        EntitiesTO benf1 = new EntitiesTO();
        //        benf1.setCreatedBy("value");
        //        benf1.setMobileNo("33717627");
        //        benf1.setNameAr("احم د محم د");
        //        benf1.setNameEn("Mohammad Ramadan");
        //        benf1.setPersonQId("28476003072");
        //        benf1.setTypeId(new Long(1));
        //        benf.setIsDeceased("N");
        //        listBenf.add(benf1);
        return listBenf;
    }

    private TransactionalWSRequest transactionalWSRequestData(TransactionalWSRequest request, EntitiesTO applicant) {
        request.setApplicant(applicant);
        request.setChannelId(new Long(MMEConstants.CHANNEL_PORTAL));
        request.setCreatedBy("value");
        request.setMunicipalityId("1");
        request.setSectorId(new Long(2));
        request.setStatusId(new Long(MMEConstants.REQUEST_STATUS_IN_REVIEW));
        request.setSubmissionUnitId("1");
        request.setBPMOutcome(MMEConstants.BPM_PROCESS_OUTCOME_SUBMIT);
        return request;
    }

    private List<AttachmentTO> listAttachment(Long typeId) {
        List<AttachmentTO> attachmentsList = new ArrayList<AttachmentTO>();
        try {
            File f = new File("C:\\Users\\mramasamy\\Desktop\\Capture1.JPG");
            NaturalResourcesServiceAMImpl am =
                (NaturalResourcesServiceAMImpl) BCUtils.getApplicationModule(amDef, config);
            String sqlStmt =
                "select ATTACHMENT_TYPE_ID\n" + "from CMN_REQUEST_ATTACHMENT_TYPES\n" + "where REQUEST_TYPE_ID = " +
                typeId;
            ViewObject attchmentTypesVO = am.getDBTransaction().createViewObjectFromQueryStmt(sqlStmt);
            attchmentTypesVO.executeQuery();
            while (attchmentTypesVO.hasNext()) {
                oracle.jbo.domain.Number attTypeId = (oracle.jbo.domain.Number) attchmentTypesVO.next().getAttribute(0);
                AttachmentTO attachment = new AttachmentTO();
                attachment.setTypeId(attTypeId.toString());
                attachment.setMimeType("application/jpg");
                attachment.setTitle("TestFromWS1");
                FileInputStream content = new FileInputStream(f);
                byte[] uploadDocContentBytes = streamToByteArray(content);
                attachment.setContents(uploadDocContentBytes);
                attachmentsList.add(attachment);
            }
            return attachmentsList;
        } catch (FileNotFoundException fnfe) {
            fnfe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return attachmentsList;
    }

    public static byte[] streamToByteArray(InputStream stream) throws IOException {
        byte[] buffer = new byte[1024];
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        int line = 0;
        while ((line = stream.read(buffer)) != -1) {
            os.write(buffer, 0, line);
        }
        stream.close();
        os.flush();
        os.close();
        return os.toByteArray();
    }


}
