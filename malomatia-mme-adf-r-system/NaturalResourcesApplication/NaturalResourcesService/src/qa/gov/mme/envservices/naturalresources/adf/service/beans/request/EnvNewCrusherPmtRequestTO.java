package qa.gov.mme.envservices.naturalresources.adf.service.beans.request;

import java.io.Serializable;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import oracle.jbo.ApplicationModule;
import oracle.jbo.Row;
import oracle.jbo.ViewObject;

import oracle.jbo.domain.DBSequence;

import qa.gov.mme.common.core.service.bean.EntitiesTO;
import qa.gov.mme.envservices.naturalresources.adf.service.beans.EnvCrusherRequestsTO;
import qa.gov.mme.envservices.naturalresources.adf.service.beans.EnvNaturalResourcesRequestsTO;
import qa.gov.mme.envservices.naturalresources.adf.service.beans.constant.EnvNaturalResServiceConstant;
import qa.gov.mme.envservices.naturalresources.model.bc.app.NaturalResourcesServiceAMImpl;

public class EnvNewCrusherPmtRequestTO extends EnvCrusherRequestsTO{
    @SuppressWarnings("compatibility:5697147542811538373")
    private static final long serialVersionUID = 1L;

    public EnvNewCrusherPmtRequestTO() {
        super();
        typeId = EnvNaturalResServiceConstant.REQUEST_TYPE_NEWCRUSPMT;
        BPMType = EnvNaturalResServiceConstant.BPM_REQUEST_TYPE_NEWCRUSPMT;
    }
    
    @XmlElement
    protected List<EntitiesTO> beneficiaries;
    
    @XmlElement
    protected EnvCrusherPermitDetailsTO crusherPmtDtls;
    
    @XmlElement
    protected String permitDuration;
    

    public void setCrusherPmtDtls(EnvCrusherPermitDetailsTO crusherPmtDtls) {
        this.crusherPmtDtls = crusherPmtDtls;
    }

    public EnvCrusherPermitDetailsTO getCrusherPmtDtls() {
        return crusherPmtDtls;
    }

    public void setPermitDuration(String permitDuration) {
        this.permitDuration = permitDuration;
    }

    public String getPermitDuration() {
        return permitDuration;
    }

    public void setBeneficiaries(List<EntitiesTO> beneficiaries) {
        this.beneficiaries = beneficiaries;
    }

    public List<EntitiesTO> getBeneficiaries() {
        return beneficiaries;
    }
    
    public ViewObject createRow(ApplicationModule am) {
        ViewObject entitiesVo = am.findViewObject("CmnEntities1");
        ViewObject identitiesVo = am.findViewObject("CmnIdentities1");

        Long beneficiaryId =
            ((NaturalResourcesServiceAMImpl) am).createGroupEntity(beneficiaries, entitiesVo, identitiesVo, true);
        this.setBeneficiaryId(beneficiaryId);
        updateRequestBeneficiaryId(am, beneficiaryId);

        BigDecimal dtlId = null;
        if(this.crusherPmtDtls!=null)
            dtlId = createPermitDtls(am);

        ViewObject crushNewPmtReqVO = am.findViewObject("EnvNewCrusherPmtRequestsVO1");
        Row requestRow = crushNewPmtReqVO.createRow();
 
        requestRow.setAttribute("PermitDetailsId", dtlId);
        requestRow.setAttribute("PermitDuration", getPermitDuration());
        requestRow.setAttribute("CreatedBy", getCreatedBy());
        crushNewPmtReqVO.insertRow(requestRow);
        
        createCrusherLocList(am, dtlId);
        createCrushCategoryList(am, dtlId);
        return crushNewPmtReqVO;
    }
    
    private BigDecimal createPermitDtls(ApplicationModule am){
        ViewObject crusPermitDtlsVO = am.findViewObject("EnvCrusherPermitDetailsVO1");
        Row dtlsR = crusPermitDtlsVO.createRow();
        dtlsR.setAttribute("ContractorId", crusherPmtDtls.getContractorId());
        dtlsR.setAttribute("ContractorMobileNo", crusherPmtDtls.getContractorMobNo());
        dtlsR.setAttribute("EnvironmentalPermitId", crusherPmtDtls.getEnvPermitId());
        dtlsR.setAttribute("DebrisInventoryId", crusherPmtDtls.getDebrisInventoryId());
        dtlsR.setAttribute("CrushersCount", crusherPmtDtls.getCrushersCount());
        dtlsR.setAttribute("CreatedBy", getCreatedBy());
        dtlsR.setAttribute("SiftingUnitsCount", crusherPmtDtls.getSiftingUnitsCount());
        crusPermitDtlsVO.insertRow(dtlsR);
        am.getTransaction().postChanges();
        
        DBSequence seq = (DBSequence) crusPermitDtlsVO.getCurrentRow().getAttribute("Id");
        return BigDecimal.valueOf(seq.getValue());
    }
}
