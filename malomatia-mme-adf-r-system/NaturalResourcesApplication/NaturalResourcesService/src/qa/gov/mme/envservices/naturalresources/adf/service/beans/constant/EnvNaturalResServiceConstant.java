package qa.gov.mme.envservices.naturalresources.adf.service.beans.constant;

public class EnvNaturalResServiceConstant {
    public EnvNaturalResServiceConstant() {
        super();
    }
    
    public static final String BPM_NATURAL_RES_PROCESS_NAME = "QuarryProcess";
    public static final String BPM_NATURAL_RES_PROCESS_PAYLOAD = "<payload><QuarryBO xmlns=\"http://xmlns.oracle.com/bpm/bpmobject/QuarryModule/QuarryBO\">\n" + 
    "<municipalityID></municipalityID>\n" + 
    "<channelID></channelID>\n" + 
    "<submittedByUserName></submittedByUserName>\n" + 
    "<isFreeCharge></isFreeCharge>\n" + 
    "<requestID></requestID>\n" + 
    "<requestNo></requestNo>\n" + 
    "<requestStatus></requestStatus>\n" + 
    "<requestType></requestType>\n" + 
    "<requestTypeID></requestTypeID>\n" + 
    "<requestSubmissionDate></requestSubmissionDate>\n" + 
    "<applicantID></applicantID>\n" + 
    "<applicantQID></applicantQID>\n" + 
    "<applicantDelegationID></applicantDelegationID>\n" + 
    "<beneficiaryID></beneficiaryID>\n" + 
    "<isApplicantBeneficiary></isApplicantBeneficiary>\n" + 
    "<inspectorUserName></inspectorUserName>\n" + 
    "<inspectionNaturalResOutcome></inspectionNaturalResOutcome>\n" + 
    "</QuarryBO>\n" + 
    "</payload>";
    public static final long REQUEST_TYPE_QURPROD_RPT = 43;
    public static final String BPM_REQUEST_TYPE_QURPROD_RPT = "QuarryProductionReportProcess";
    public static final long REQUEST_TYPE_NEWQUR_PERMIT = 44;
    public static final String BPM_REQUEST_TYPE_NEWQUR_PERMIT = "NewQuarryPermitRequestProcess";
    public static final String NR_SERVICES_AM_DEF = "qa.gov.mme.envservices.naturalresources.model.bc.app.NaturalResourcesServiceAM";
    public static final String NR_SERVICES_AM_CONFIG = "NaturalResourcesServiceAMLocal";
    public static final long REQUEST_TYPE_RENEWQUR_PERMIT = 45;
    public static final String BPM_REQUEST_TYPE_RENEWQUR_PERMIT = "RenewQuarryPermitRequest";
    
    
    public static final String BPM_TRANSFER_DEBRIS_PROCESS_NAME = "TransferDebrisLicenseProcesses";
    public static final String BPM_TRANSFER_DEBRIS_PROCESS_PAYLOAD = "<payload><TransferDebrisLicenseBO xmlns=\"http://xmlns.oracle.com/bpm/bpmobject/TransferDebrisLicenseModule/TransferDebrisLicenseBO\">\n" + 
        "<municipalityID></municipalityID>\n" + 
        "<channelID></channelID>\n" + 
        "<submittedByUserName></submittedByUserName>\n" + 
        "<isFreeCharge></isFreeCharge>\n" + 
        "<requestID></requestID>\n" + 
        "<requestNo></requestNo>\n" + 
        "<requestStatus></requestStatus>\n" + 
        "<requestType></requestType>\n" + 
        "<requestTypeID></requestTypeID>\n" + 
        "<requestSubmissionDate></requestSubmissionDate>\n" + 
        "<applicantID></applicantID>\n" + 
        "<applicantQID></applicantQID>\n" + 
        "<applicantDelegationID></applicantDelegationID>\n" + 
        "<beneficiaryID></beneficiaryID>\n" + 
        "<isApplicantBeneficiary></isApplicantBeneficiary>\n" + 
        "<inspectorUserName></inspectorUserName>\n" + 
        "<inspectionNaturalResOutcome></inspectionNaturalResOutcome>\n" +
        "<movementTypeId></movementTypeId>\n" + 
        "</TransferDebrisLicenseBO>\n" + 
        "</payload>";
    public static final long REQUEST_TYPE_NEW_MV_DBRS_PERMIT = 51;//to be provided
    public static final String BPM_REQUEST_TYPE_NEW_MV_DBRS_PERMIT = "IssueTransferDebrisLicense"; // to be provided
    public static final long REQUEST_TYPE_RENEW_MV_DBRS_PERMIT = 52;
    public static final String BPM_REQUEST_TYPE_RENEW_MV_DBRS_PERMIT = "RenewTransferDebrisLicense";
    public static final long REQUEST_TYPE_MODIFY_MV_DBRS_PERMIT = 53;
    public static final String BPM_REQUEST_TYPE_MODIFY_MV_DBRS_PERMIT = "AmendTransferDebrisLicense";
    
    
    /*
     * Denation services 
     * @malek
     */
    public static final String BPM_DENOATION_PROCESS_NAME = "DetonationLicenseProcesses";
    public static final String BPM_REQUEST_TYPE_NEW_DENOATION_PERMIT = "IssueDetonationLicense";
    public static final String BPM_REQUEST_TYPE_RENEW_DENOATION_PERMIT = "RenewDetonationLicense";
    public static final long REQUEST_TYPE_NEW_DENOATION_PERMIT = 54;
    public static final long REQUEST_TYPE_RENEW_DENOATION_PERMIT = 55;
    
    public static final String BPM_DENOATION_PERMIT_PROCESS_PAYLOAD = 
    "<payload><DetonationLicenseBO xmlns=\"http://xmlns.oracle.com/bpm/bpmobject/DetonationLicenseModule/DetonationLicenseBO\">\n" + 
    "<municipalityID></municipalityID>\n" + 
    "<channelID></channelID>\n" + 
    "<submittedByUserName></submittedByUserName>\n" + 
    "<isFreeCharge></isFreeCharge>\n" + 
    "<requestID></requestID>\n" + 
    "<requestNo></requestNo>\n" + 
    "<requestStatus></requestStatus>\n" + 
    "<requestType></requestType>\n" + 
    "<requestTypeID></requestTypeID>\n" + 
    "<requestSubmissionDate></requestSubmissionDate>\n" + 
    "<applicantID></applicantID>\n" + 
    "<applicantQID></applicantQID>\n" + 
    "<applicantDelegationID></applicantDelegationID>\n" + 
    "<beneficiaryID></beneficiaryID>\n" + 
    "<isApplicantBeneficiary></isApplicantBeneficiary>\n" + 
    "<inspectorUserName></inspectorUserName>\n" + 
    "<inspectionNaturalResOutcome></inspectionNaturalResOutcome>\n" + 
    "</DetonationLicenseBO>\n" + 
    "</payload>\n";
    
    public static final String BPM_CRUSHER_PROCESS_NAME = "CrusherLicenseProcesses";
    public static final String BPM_CRUSHER_PROCESS_PAYLOAD = "<payload><CrusherLicenseBO xmlns=\"http://xmlns.oracle.com/bpm/bpmobject/CrusherLicenseModule/CrusherLicenseBO\">\n" + 
    "<municipalityID></municipalityID>\n" + 
    "<channelID></channelID>\n" + 
    "<submittedByUserName></submittedByUserName>\n" + 
    "<isFreeCharge></isFreeCharge>\n" + 
    "<requestID></requestID>\n" + 
    "<requestNo></requestNo>\n" + 
    "<requestStatus></requestStatus>\n" + 
    "<requestType></requestType>\n" + 
    "<requestTypeID></requestTypeID>\n" + 
    "<requestSubmissionDate></requestSubmissionDate>\n" + 
    "<applicantID></applicantID>\n" + 
    "<applicantQID></applicantQID>\n" + 
    "<applicantDelegationID></applicantDelegationID>\n" + 
    "<beneficiaryID></beneficiaryID>\n" + 
    "<isApplicantBeneficiary></isApplicantBeneficiary>\n" + 
    "<inspectorUserName></inspectorUserName>\n" + 
    "<inspectionNaturalResOutcome></inspectionNaturalResOutcome>\n" + 
    "</CrusherLicenseBO>\n" + 
    "</payload>";
    
    public static final long REQUEST_TYPE_NEWCRUSPMT = 56;
    public static final String BPM_REQUEST_TYPE_NEWCRUSPMT = "IssueCrusherLicense";
    public static final long REQUEST_TYPE_RENEWCRUSPMT = 58;
    public static final String BPM_REQUEST_TYPE_RENEWCRUSPMT = "RenewCrusherLicense";
    public static final long REQUEST_TYPE_AMENDCRUSPMT = 59;
    public static final String BPM_REQUEST_TYPE_AMENDCRUSPMT = "AmendCrusherLicense";
    /*
     * Denation services 
     * @malek
     */
}
