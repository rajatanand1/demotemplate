package qa.gov.mme.envservices.naturalresources.adf.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import java.math.BigDecimal;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;

import java.util.List;

import oracle.jbo.ViewObject;

import qa.gov.mme.common.constants.MMEConstants;

import qa.gov.mme.common.core.service.util.BCUtils;
import qa.gov.mme.common.ws.proxy.client.NaturalResourcesServiceClient;
import qa.gov.mme.common.ws.proxy.request.naturalresources.types.AttachmentTO;
import qa.gov.mme.common.ws.proxy.request.naturalresources.types.EntitiesTO;
import qa.gov.mme.common.ws.proxy.request.naturalresources.types.EnvDebrisLocCoordinatesTO;
import qa.gov.mme.common.ws.proxy.request.naturalresources.types.EnvMoveDebrisPermitDetailsTO;
import qa.gov.mme.common.ws.proxy.request.naturalresources.types.EnvNewMvDebrisPmtRequestsTO;
import qa.gov.mme.common.ws.proxy.request.naturalresources.types.EnvQuarryProdRepProductsTO;
import qa.gov.mme.common.ws.proxy.request.naturalresources.types.EnvQuarryProdRepRequestsTO;
import qa.gov.mme.common.ws.proxy.request.naturalresources.types.EnvQuarryProductionReportTO;
import qa.gov.mme.common.ws.proxy.request.naturalresources.types.EnvRenewMvDbrsPmtRequestsTO;
import qa.gov.mme.common.ws.proxy.request.naturalresources.types.EnvRenewQuarryPmtRequestTO;
import qa.gov.mme.common.ws.proxy.request.naturalresources.types.TransactionalWSRequest;
import qa.gov.mme.common.ws.proxy.request.naturalresources.types.TransactionalWSReturn;
import qa.gov.mme.envservices.naturalresources.adf.service.beans.constant.EnvNaturalResServiceConstant;
import qa.gov.mme.common.ws.proxy.request.naturalresources.types.EnvCrusherLocCoordinatesTO;
import qa.gov.mme.common.ws.proxy.request.naturalresources.types.EnvCrusherPermitCategoriesTO;
import qa.gov.mme.common.ws.proxy.request.naturalresources.types.EnvCrusherPermitDetailsTO;
import qa.gov.mme.common.ws.proxy.request.naturalresources.types.EnvNewCrusherPmtRequestTO;
import qa.gov.mme.common.ws.proxy.request.naturalresources.types.EnvAmdCrusherPmtRequestTO;
import qa.gov.mme.common.ws.proxy.request.naturalresources.types.EnvRenewCrusherPmtRequestTO;
import qa.gov.mme.envservices.naturalresources.model.bc.app.NaturalResourcesServiceAMImpl;

public class TestProxyClient {
    private String currentDateStr;
    private String amDef = EnvNaturalResServiceConstant.NR_SERVICES_AM_DEF;
    private String config = EnvNaturalResServiceConstant.NR_SERVICES_AM_CONFIG;
    
    public TestProxyClient() {
        super();
        SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        this.currentDateStr=sdf.format(new Date());
        System.out.println(this.currentDateStr);
    }
    
    private EntitiesTO entitiesTOData() {
        EntitiesTO entity = new EntitiesTO();
        entity.setCreatedBy("value");
        entity.setMobileNo("66953867");
        entity.setNameAr("محم د رمضان");
        entity.setNameEn("Mohammad Ramadan");
        entity.setPersonQId("28476003071");
        entity.setTypeId(new Long(1));
        entity.setIsDeceased("N");
        return entity;
    }
    
    private List<EntitiesTO> listBeneficiaries() {
        List<EntitiesTO> listBenf = new ArrayList<EntitiesTO>();
        EntitiesTO benf = new EntitiesTO();
        benf.setCreatedBy("value");
        benf.setMobileNo("66953867");
        benf.setNameAr("محم د رمضان");
        benf.setNameEn("Mohammad Ramadan");
        benf.setPersonQId("28476003071");
        benf.setTypeId(new Long(1));
        benf.setIsDeceased("N");
        listBenf.add(benf);
//
//            EntitiesTO benf1 = new EntitiesTO();
//            benf1.setCreatedBy("value");
//            benf1.setMobileNo("33717627");
//            benf1.setNameAr("احم د محم د");
//            benf1.setNameEn("Mohammad Ramadan");
//            benf1.setPersonQId("28476003072");
//            benf1.setTypeId(new Long(1));
//            benf.setIsDeceased("N");
//            listBenf.add(benf1);
        return listBenf;
    }
    
//    public EnvNewMvDebrisPmtRequestsTO getEnvNewMvDebrisRequestsTO(){
//        EntitiesTO applicant = entitiesTOData();
//        List<EntitiesTO> beneficiaries = listBeneficiaries();
//        EnvNewMvDebrisPmtRequestsTO req = new EnvNewMvDebrisPmtRequestsTO();
//        EnvMoveDebrisPermitDetailsTO pmtDetails = new EnvMoveDebrisPermitDetailsTO();
//        List<EnvDebrisLocCoordinatesTO> coordinates = new ArrayList<EnvDebrisLocCoordinatesTO>();
//        for(int i=0; i < 2; i++){
//            EnvDebrisLocCoordinatesTO coordinate = new EnvDebrisLocCoordinatesTO();
//            coordinate.setLongitude(new BigDecimal(i));
//            coordinate.setLatitude(new BigDecimal(2*i));
//            coordinate.setCreatedBy("mramadan");
//            coordinate.setCoordinateOrder(new BigDecimal(1));
//            coordinates.add(coordinate);
//        }
//        pmtDetails.setMovementTypeId(new BigDecimal(1));
//        pmtDetails.setCarrierId(new BigDecimal(1));
//        pmtDetails.setCreatedBy("mramadan");
//        req.setApplicant(applicant);
//        
//        List<EnvDebrisLocCoordinatesTO> reqCordinates = req.getCoordinates();
//        reqCordinates.addAll(coordinates);
//        
//        List<EntitiesTO> reqBeneficiaries = req.getBeneficiaries();
//        reqBeneficiaries.addAll(beneficiaries);
//        
//        req.setPermitDetails(pmtDetails);
//        req.setPermitDuration(new BigDecimal(200));
//        req.setQuantity(new BigDecimal(200));
//        req.setUomId(new BigDecimal(1));
//        transactionalWSRequestData(req, applicant);
//        return req;
//    }

    private TransactionalWSRequest transactionalWSRequestData(TransactionalWSRequest request, EntitiesTO applicant) {
        request.setApplicant(applicant);
        request.setChannelId(new Long(MMEConstants.CHANNEL_PORTAL));
        request.setCreatedBy("value");
        request.setMunicipalityId("1");
        request.setSectorId(new Long(2));
        request.setStatusId(new Long(MMEConstants.REQUEST_STATUS_IN_REVIEW));
        request.setSubmissionUnitId("1");
        request.setBPMOutcome(MMEConstants.BPM_PROCESS_OUTCOME_SUBMIT);
        return request;
    }
    
    public EnvRenewMvDbrsPmtRequestsTO getEnvRenewMvDebrisRequestsTO(){
        EntitiesTO applicant = entitiesTOData();
        List<EntitiesTO> beneficiaries = listBeneficiaries();
        EnvRenewMvDbrsPmtRequestsTO req = new EnvRenewMvDbrsPmtRequestsTO();
        List<EntitiesTO> reqBeneficiaries = req.getBeneficiaries();
        reqBeneficiaries.addAll(beneficiaries);
        req.setPermitDuration(new BigDecimal(200));
        req.setPermitId(new BigDecimal(1));
        transactionalWSRequestData(req, applicant);
        return req;
    }
    
    public static void main(String[] args) {
        NaturalResourcesServiceClient client = new NaturalResourcesServiceClient();
        TransactionalWSReturn result = null;
        TestProxyClient cc = new TestProxyClient();
//    //        result = client.submitDesignHomeGardenRequest(client.getDesignHomeGardenRequestsTO());
//        result = client.submitNewQuarryPermit(cc.getEnvNewQuarryPmtRequestsTO());
//    //        result = client.submitParkReservationRequest(client.getPubParkReservRequestsTO());
//    result = client.submitNewMvDbrsPermit(cc.getEnvNewMvDebrisRequestsTO());
        result = client.submitRenewCrusherPermit(cc.getEnvRenewCrusherPmtRequestTO());
        System.out.println(result.getTransactionId());
        System.out.println("Error Code:"+result.getErrorCode());
        System.out.println("Error Message:"+result.getErrorMessage());
    }    
    public EnvQuarryProdRepRequestsTO getEnvQuarryProdRepRequestsTO() {
        EntitiesTO applicant = entitiesTOData();
        List<EntitiesTO> beneficiaries = listBeneficiaries();
      //  List<AttachmentTO> listAttachment = listAttachment(new Long(43));
        EnvQuarryProdRepRequestsTO quarryRptReq = new EnvQuarryProdRepRequestsTO();
        quarryRptReq.setBeneficiaryId(beneficiaries.get(0).getId());
     //   quarryRptReq.getAttachments().addAll(listAttachment);
        quarryRptReq.setCreatedBy("value");
        quarryRptReq.setIsAnnualReportAvailable("Y");
        
        System.out.println("Beneficiary size:"+beneficiaries.size());
        quarryRptReq.getBeneficiaries().addAll(beneficiaries);
        
        EnvQuarryProductionReportTO quarryRpt = new EnvQuarryProductionReportTO();
        quarryRpt.setQuarryPermitId("1");
        quarryRpt.setQuarryId("1");
        quarryRpt.setAnnualReportCmsId("2");
        quarryRpt.setMonth("12");
        quarryRpt.setReportCmsId("2");
        quarryRpt.setReportStatusDate(this.currentDateStr);
        quarryRpt.setReportStatusId("1");
        quarryRpt.setReportText("2");
        quarryRpt.setStatusUserName("state user");
        quarryRpt.setSurveyReportCmsId("2");
        quarryRpt.setYear("2020");
        quarryRptReq.setQuarryProdRpt(quarryRpt);
        
        List<EnvQuarryProdRepProductsTO> prodList = new ArrayList<EnvQuarryProdRepProductsTO>();
        EnvQuarryProdRepProductsTO prods = new EnvQuarryProdRepProductsTO();
        prods.setProductId("1");
        prods.setMonthlyProdQtyQM("20");
        prods.setMaxProdQtyQM("90");
        prods.setProductNotes("tEST Notes");
        
        prods.setUsagePurpose("Test");
        
        prodList.add(prods);
        
        EnvQuarryProdRepProductsTO prodsR = new EnvQuarryProdRepProductsTO();
        prodsR.setProductId("2");
        prodsR.setMonthlyProdQtyQM("10");
        prodsR.setMaxProdQtyQM("50");
        prodsR.setProductNotes("TEST 2");
        
        prodsR.setUsagePurpose("TEST 2 PP");
        
        prodList.add(prodsR);
        quarryRptReq.getQuarryProductsList().addAll(prodList);
        quarryRptReq.setChannelId(Long.valueOf(MMEConstants.CHANNEL_SERVICE_CENTER));
        quarryRptReq = (EnvQuarryProdRepRequestsTO) transactionalWSRequestData(quarryRptReq, applicant);
        return quarryRptReq;
    }
    
    public EnvNewCrusherPmtRequestTO getEnvNewCrusherPmtRequestTO() {
        EntitiesTO applicant = entitiesTOData();
        List<EntitiesTO> beneficiaries = listBeneficiaries();
     //   List<AttachmentTO> listAttachment = listAttachment(new Long(56));
        EnvNewCrusherPmtRequestTO ReqT = new EnvNewCrusherPmtRequestTO();
        ReqT.setBeneficiaryId(beneficiaries.get(0).getId());
    //    ReqT.setAttachments(listAttachment);
        ReqT.setCreatedBy("zaid");
        ReqT.setPermitDuration("20");
        
        System.out.println("Beneficiary size:"+beneficiaries.size());
        ReqT.getBeneficiaries().addAll(beneficiaries);
//        ReqT.setBeneficiaries(beneficiaries);
        
        EnvCrusherPermitDetailsTO pmtT = new EnvCrusherPermitDetailsTO();
        pmtT.setContractorId("1002");
        pmtT.setContractorMobNo("12345678");
        pmtT.setCrushersCount("2");
        pmtT.setDebrisInventoryId("2");
        pmtT.setEnvPermitId("1");
        pmtT.setSiftingUnitsCount("29");
        pmtT.setCrushersCount("40");
        
        ReqT.setCrusherPmtDtls(pmtT);
        
        List<EnvCrusherLocCoordinatesTO> locList = new ArrayList<EnvCrusherLocCoordinatesTO>();
        EnvCrusherLocCoordinatesTO locs = new EnvCrusherLocCoordinatesTO();
        locs.setLongtitude("1");
        locs.setLatitude("20");
        locs.setCoordinateOrder("90");
        
        locList.add(locs);
        ReqT.getCrusherLocList().addAll(locList);
        
        List<EnvCrusherPermitCategoriesTO> catList = new ArrayList<EnvCrusherPermitCategoriesTO>();
        EnvCrusherPermitCategoriesTO catT = new EnvCrusherPermitCategoriesTO();
        catT.setCategoryId("1");
        
        catList.add(catT);
        ReqT.getCrushPmtCategoryList().addAll(catList);
        
        ReqT.setChannelId(Long.valueOf(MMEConstants.CHANNEL_SERVICE_CENTER));
        ReqT = (EnvNewCrusherPmtRequestTO) transactionalWSRequestData(ReqT, applicant);
        return ReqT;
    }
    
    public EnvAmdCrusherPmtRequestTO getEnvAmdCrusherPmtRequestTO() {
        EntitiesTO applicant = entitiesTOData();
        List<EntitiesTO> beneficiaries = listBeneficiaries();
     //   List<AttachmentTO> listAttachment = listAttachment(new Long(56));
        EnvAmdCrusherPmtRequestTO ReqT = new EnvAmdCrusherPmtRequestTO();
        ReqT.setBeneficiaryId(beneficiaries.get(0).getId());
     //   ReqT.setAttachments(listAttachment);
        ReqT.setCreatedBy("zaid");
        ReqT.setPermitId("2");
        
        System.out.println("Beneficiary size:"+beneficiaries.size());
        ReqT.getBeneficiaries().addAll(beneficiaries);
        
        EnvCrusherPermitDetailsTO pmtT = new EnvCrusherPermitDetailsTO();
        pmtT.setContractorId("1002");
        pmtT.setContractorMobNo("12345678");
        pmtT.setCrushersCount("2");
        pmtT.setDebrisInventoryId("2");
        pmtT.setEnvPermitId("1");
        pmtT.setSiftingUnitsCount("29");
        pmtT.setCrushersCount("40");
        
        ReqT.setCrusherPmtDtls(pmtT);
        
        List<EnvCrusherLocCoordinatesTO> locList = new ArrayList<EnvCrusherLocCoordinatesTO>();
        EnvCrusherLocCoordinatesTO locs = new EnvCrusherLocCoordinatesTO();
        locs.setLongtitude("1");
        locs.setLatitude("20");
        locs.setCoordinateOrder("90");
        
        locList.add(locs);
        ReqT.getCrusherLocList().addAll(locList);
        
        List<EnvCrusherPermitCategoriesTO> catList = new ArrayList<EnvCrusherPermitCategoriesTO>();
        EnvCrusherPermitCategoriesTO catT = new EnvCrusherPermitCategoriesTO();
        catT.setCategoryId("1");
        
        catList.add(catT);
        ReqT.getCrushPmtCategoryList().addAll(catList);
        ReqT.setChannelId(Long.valueOf(MMEConstants.CHANNEL_SERVICE_CENTER));
        ReqT = (EnvAmdCrusherPmtRequestTO) transactionalWSRequestData(ReqT, applicant);
        return ReqT;
    }
    
    public EnvRenewCrusherPmtRequestTO getEnvRenewCrusherPmtRequestTO() {
        EntitiesTO applicant = entitiesTOData();
        List<EntitiesTO> beneficiaries = listBeneficiaries();
     //   List<AttachmentTO> listAttachment = listAttachment(new Long(56));
        EnvRenewCrusherPmtRequestTO ReqT = new EnvRenewCrusherPmtRequestTO();
        ReqT.setBeneficiaryId(beneficiaries.get(0).getId());
     //   ReqT.setAttachments(listAttachment);
        ReqT.setCreatedBy("zaid");
        ReqT.setPermitId("2");
        ReqT.setPermitDuration("45");
        
        System.out.println("Beneficiary size:"+beneficiaries.size());
        ReqT.getBeneficiaries().addAll(beneficiaries);
        
        
        ReqT.setChannelId(Long.valueOf(MMEConstants.CHANNEL_SERVICE_CENTER));
        ReqT = (EnvRenewCrusherPmtRequestTO) transactionalWSRequestData(ReqT, applicant);
        return ReqT;
    }
    
//    
//    public EnvNewQuarryPmtRequestsTO getEnvNewQuarryPmtRequestsTO() {
//        EntitiesTO applicant = entitiesTOData();
//        List<EntitiesTO> beneficiaries = listBeneficiaries();
//        List<AttachmentTO> listAttachment = listAttachment(new Long(44));
//        EnvNewQuarryPmtRequestsTO quarryPmtReq = new EnvNewQuarryPmtRequestsTO();
//        quarryPmtReq.setBeneficiaryId(beneficiaries.get(0).getId());
//        quarryPmtReq.getAttachments().addAll(listAttachment);
//        quarryPmtReq.setCreatedBy("value");
//        quarryPmtReq.setPermitDuration("12");
//        
//        quarryPmtReq.getBeneficiaries().addAll(beneficiaries);
//        
//        EnvQuarryPermitDetailsTO quarryPmt = new EnvQuarryPermitDetailsTO();
//        quarryPmt.setBenficiaryCompanyId("1002");
//        quarryPmt.setQuarryId("1");
//        quarryPmt.setContractNo("2234");
//        quarryPmt.setContractEndDate(this.currentDateStr);
//        quarryPmtReq.setPermitDetails(quarryPmt);
//        
//        quarryPmtReq.setChannelId(Long.valueOf(MMEConstants.CHANNEL_SERVICE_CENTER));
//        quarryPmtReq = (EnvNewQuarryPmtRequestsTO) transactionalWSRequestData(quarryPmtReq, applicant);
//        return quarryPmtReq;
//    }
//    
    public EnvRenewQuarryPmtRequestTO getEnvRenewQuarryPmtRequestTO() {
        EntitiesTO applicant = entitiesTOData();
        List<EntitiesTO> beneficiaries = listBeneficiaries();
      //  List<AttachmentTO> listAttachment = listAttachment(new Long(45));
        EnvRenewQuarryPmtRequestTO quarryPmtReq = new EnvRenewQuarryPmtRequestTO();
        quarryPmtReq.setBeneficiaryId(beneficiaries.get(0).getId());
     //   quarryPmtReq.getAttachments().addAll(listAttachment);
        quarryPmtReq.setCreatedBy("value");
        
        quarryPmtReq.setPermitId("1");
        quarryPmtReq.setAreaSqm("12");
        quarryPmtReq.setProdQuantityQm("120");
        quarryPmtReq.setPermitDuration("12");
        
        
        quarryPmtReq.getBeneficiaries().addAll(beneficiaries);
        
        quarryPmtReq.setChannelId(Long.valueOf(MMEConstants.CHANNEL_SERVICE_CENTER));
        quarryPmtReq = (EnvRenewQuarryPmtRequestTO) transactionalWSRequestData(quarryPmtReq, applicant);
        return quarryPmtReq;
    }
//    

//    
    private List<AttachmentTO> listAttachment(Long typeId) {
        List<AttachmentTO> attachmentsList = new ArrayList<AttachmentTO>();
        try {
            File f = new File("C:\\Users\\mramasamy\\Desktop\\Capture1.jpg");
            NaturalResourcesServiceAMImpl am = (NaturalResourcesServiceAMImpl) BCUtils.getApplicationModule(amDef, config);
            String sqlStmt =
                "select ATTACHMENT_TYPE_ID\n" + "from CMN_REQUEST_ATTACHMENT_TYPES\n" + "where REQUEST_TYPE_ID = " +
                typeId;
            ViewObject attchmentTypesVO = am.getDBTransaction().createViewObjectFromQueryStmt(sqlStmt);
            attchmentTypesVO.executeQuery();
            while (attchmentTypesVO.hasNext()) {
                BigDecimal attTypeId = (BigDecimal) attchmentTypesVO.next().getAttribute(0);
                AttachmentTO attachment = new AttachmentTO();
                attachment.setTypeId(attTypeId.toString());
                attachment.setMimeType("application/pdf");
                attachment.setTitle("TestFromWS1");
                FileInputStream content = new FileInputStream(f);
                byte[] uploadDocContentBytes = streamToByteArray(content);
                attachment.setContents(uploadDocContentBytes);
                attachmentsList.add(attachment);
            }
            return attachmentsList;
        } catch (FileNotFoundException fnfe) {
            fnfe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return attachmentsList;
    }
    
    public byte[] streamToByteArray(InputStream stream) throws IOException {
        byte[] buffer = new byte[1024];
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        int line = 0;
        while ((line = stream.read(buffer)) != -1) {
            os.write(buffer, 0, line);
        }
        stream.close();
        os.flush();
        os.close();
        return os.toByteArray();
    }
}
