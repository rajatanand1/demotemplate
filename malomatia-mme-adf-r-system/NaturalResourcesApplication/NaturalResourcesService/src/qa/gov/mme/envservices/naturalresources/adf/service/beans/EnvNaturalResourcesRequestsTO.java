package qa.gov.mme.envservices.naturalresources.adf.service.beans;

import qa.gov.mme.envservices.naturalresources.adf.service.beans.constant.EnvNaturalResServiceConstant;



public class EnvNaturalResourcesRequestsTO extends EnvNaturalResourcesCommonTO {
    @SuppressWarnings("compatibility:6430011250530405768")
    private static final long serialVersionUID = 1L;

    public EnvNaturalResourcesRequestsTO() {
        super();
        processName = EnvNaturalResServiceConstant.BPM_NATURAL_RES_PROCESS_NAME;
        processPayload = EnvNaturalResServiceConstant.BPM_NATURAL_RES_PROCESS_PAYLOAD;
    }


}
