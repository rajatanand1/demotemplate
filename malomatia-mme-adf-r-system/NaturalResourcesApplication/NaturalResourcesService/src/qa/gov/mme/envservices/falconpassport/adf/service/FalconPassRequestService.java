package qa.gov.mme.envservices.falconpassport.adf.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;

import oracle.jbo.ApplicationModule;

import qa.gov.mme.common.core.service.CoreRequestService;
import qa.gov.mme.common.core.service.bean.response.TransactionalWSReturn;
import qa.gov.mme.common.core.service.util.BCUtils;
import qa.gov.mme.envservices.falconpassport.adf.service.beans.constant.FalconPassortServiceConstant;
import qa.gov.mme.envservices.falconpassport.adf.service.beans.request.FalconPassportNewRequestTO;
import qa.gov.mme.envservices.falconpassport.model.bc.app.FalconPassportServiceAMImpl;

public class FalconPassRequestService extends CoreRequestService{
    
    private static final long serialVersionUID = 1L;
    
    public FalconPassRequestService() {
        super();
    }
    
    public TransactionalWSReturn submitNewFalconPassRequest(@WebParam(name = "request") FalconPassportNewRequestTO request) {
            FalconPassportServiceAMImpl am = (FalconPassportServiceAMImpl) BCUtils.getApplicationModule(FalconPassortServiceConstant.NR_SERVICES_AM_DEF, 
                                                                                                        FalconPassortServiceConstant.NR_SERVICES_AM_CONFIG);
            
            return submitRequest(am, request);
   }   
}
