package qa.gov.mme.envservices.naturalresources.adf.service.beans.request;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

public class EnvCrusherPermitCategoriesTO implements Serializable{
    @SuppressWarnings("compatibility:7903437418297947594")
    private static final long serialVersionUID = 1L;

    public EnvCrusherPermitCategoriesTO() {
        super();
    }
    
    @XmlElement
    protected String categoryId;

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryId() {
        return categoryId;
    }
}
