package qa.gov.mme.envservices.falconpassport.adf.service.beans.constant;

public class FalconPassortServiceConstant {
    public FalconPassortServiceConstant() {
        super();
    }
    
    /*
     * Issue Falcon Passport Web Service
     * @Rajat Anand
     */
    public static final String BPM_FALCON_PAS_PROCESS_NAME = "FalconsProcess";
    public static final String BPM_FALCON_PAS_PROCESS_PAYLOAD = "<payload><FalconsBO xmlns=\\http://xmlns.oracle.com/bpm/bpmobject/FalconsModule/FalconsBO\\>\n" + 
    "<requestID></requestID> \n" + 
    "<requestNo></requestNo> \n" + 
    "<requestStatus></requestStatus>\n" + 
    "<requestType></requestType> \n" + 
    "<municipalityID></municipalityID> \n" + 
    "<channelId></channelId>\n" + 
    "<applicantID></applicantID> \n" + 
    "<applicantDelegationId></applicantDelegationId>\n" + 
    "<beneficiaryId></beneficiaryId>\n" + 
    "<isApplicantBeneficiary></isApplicantBeneficiary>\n" + 
    "<isFreeCharge></isFreeCharge>\n" + 
    "<processOutcome></processOutcome> \n" + 
    "</FalconsBO> \n" + 
    "</payload>\n";
    
    public static final long REQUEST_TYPE_NEWFALPASS = 37;
    public static final String BPM_REQUEST_TYPE_NEWFALPASS = "IssueFalconPassport";
    
    public static final String NR_SERVICES_AM_DEF = "qa.gov.mme.envservices.falconpassport.model.bc.app.FalconPassportServiceAM";
    public static final String NR_SERVICES_AM_CONFIG = "FalconPassportServiceAMLocal";
}
