package qa.gov.mme.envservices.naturalresources.adf.service.beans.request;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

public class EnvCrusherLocCoordinatesTO implements Serializable{
    @SuppressWarnings("compatibility:201261631761597442")
    private static final long serialVersionUID = 1L;

    public EnvCrusherLocCoordinatesTO() {
        super();
    }
    
    @XmlElement
    protected String longtitude;
    
    @XmlElement
    protected String latitude;
    
    @XmlElement
    protected String coordinateOrder;

    public void setLongtitude(String longtitude) {
        this.longtitude = longtitude;
    }

    public String getLongtitude() {
        return longtitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setCoordinateOrder(String coordinateOrder) {
        this.coordinateOrder = coordinateOrder;
    }

    public String getCoordinateOrder() {
        return coordinateOrder;
    }
}
