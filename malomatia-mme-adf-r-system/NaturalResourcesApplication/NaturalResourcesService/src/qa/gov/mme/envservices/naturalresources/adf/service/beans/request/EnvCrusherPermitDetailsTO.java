package qa.gov.mme.envservices.naturalresources.adf.service.beans.request;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

public class EnvCrusherPermitDetailsTO implements Serializable{
    @SuppressWarnings("compatibility:-5741973052762396634")
    private static final long serialVersionUID = 1L;

    public EnvCrusherPermitDetailsTO() {
        super();
    }
    
    @XmlElement
    protected String contractorId;
    
    @XmlElement
    protected String contractorMobNo;
    
    @XmlElement
    protected String usagePurpose;
    
    @XmlElement
    protected String envPermitId;
    
    @XmlElement
    protected String debrisInventoryId;
    
    @XmlElement
    protected String crushersCount;
    
    @XmlElement
    protected String siftingUnitsCount;

    public void setContractorId(String contractorId) {
        this.contractorId = contractorId;
    }

    public String getContractorId() {
        return contractorId;
    }

    public void setContractorMobNo(String contractorMobNo) {
        this.contractorMobNo = contractorMobNo;
    }

    public String getContractorMobNo() {
        return contractorMobNo;
    }

    public void setUsagePurpose(String usagePurpose) {
        this.usagePurpose = usagePurpose;
    }

    public String getUsagePurpose() {
        return usagePurpose;
    }

    public void setEnvPermitId(String envPermitId) {
        this.envPermitId = envPermitId;
    }

    public String getEnvPermitId() {
        return envPermitId;
    }

    public void setDebrisInventoryId(String debrisInventoryId) {
        this.debrisInventoryId = debrisInventoryId;
    }

    public String getDebrisInventoryId() {
        return debrisInventoryId;
    }

    public void setCrushersCount(String crushersCount) {
        this.crushersCount = crushersCount;
    }

    public String getCrushersCount() {
        return crushersCount;
    }

    public void setSiftingUnitsCount(String siftingUnitsCount) {
        this.siftingUnitsCount = siftingUnitsCount;
    }

    public String getSiftingUnitsCount() {
        return siftingUnitsCount;
    }
}
