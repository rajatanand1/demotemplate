package qa.gov.mme.envservices.falconpassport.adf.service.beans.request;

import java.math.BigDecimal;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import oracle.jbo.ApplicationModule;
import oracle.jbo.Row;
import oracle.jbo.ViewObject;

import oracle.jbo.domain.DBSequence;

import qa.gov.mme.common.core.service.bean.EntitiesTO;
import qa.gov.mme.common.core.service.bean.request.TransactionalWSRequest;
import qa.gov.mme.envservices.falconpassport.adf.service.beans.constant.FalconPassortServiceConstant;
import qa.gov.mme.envservices.falconpassport.adf.service.beans.request.FalconPassportDetailsTO;
import qa.gov.mme.envservices.falconpassport.model.bc.app.FalconPassportServiceAMImpl;

public class FalconPassportNewRequestTO extends TransactionalWSRequest{

    private static final long serialVersionUID = 1L;
    
    public FalconPassportNewRequestTO() {
        super();
        typeId = FalconPassortServiceConstant.REQUEST_TYPE_NEWFALPASS;
        BPMType = FalconPassortServiceConstant.BPM_REQUEST_TYPE_NEWFALPASS;
    }
    
    @XmlElement
    protected List<EntitiesTO> beneficiaries;
    
    @XmlElement
    protected FalconPassportDetailsTO falconPassDtls;

    public void setBeneficiaries(List<EntitiesTO> beneficiaries) {
        this.beneficiaries = beneficiaries;
    }

    public List<EntitiesTO> getBeneficiaries() {
        return beneficiaries;
    }

    public void setFalconPassDtls(FalconPassportDetailsTO falconPassDtls) {
        this.falconPassDtls = falconPassDtls;
    }

    public FalconPassportDetailsTO getFalconPassDtls() {
        return falconPassDtls;
    }
    
    @Override
    public ViewObject createRow(ApplicationModule am) throws Exception {
        // TODO Implement this method
        ViewObject entitiesVo = am.findViewObject("CmnEntities1");
        ViewObject identitiesVo = am.findViewObject("CmnIdentities1");
        
        Long beneficiaryId = ((FalconPassportServiceAMImpl) am).createGroupEntity(beneficiaries, entitiesVo, identitiesVo, true);
        this.setBeneficiaryId(beneficiaryId);
        
        BigDecimal fpassDtlId = null;
        // Nocites = 2 then only cereate row else update new pit no only.
        ViewObject animalDtlsVO = am.findViewObject("CmnAnimalsVO1");
        
        if(this.falconPassDtls != null)
            fpassDtlId = createAnimalDtls(am);            
        
        
        ViewObject envFalIssPassReqVO = am.findViewObject("EnvIssueFlcPassRequestsVO1");
        Row passIssR = envFalIssPassReqVO.createRow();
        passIssR.setAttribute("AnimalId", fpassDtlId);
        //passIssR.setAttribute("NewPitNo", arg1);
        passIssR.setAttribute("CreatedBy", getCreatedBy());
        envFalIssPassReqVO.insertRow(passIssR);
        
        return envFalIssPassReqVO;
    }
    
    private BigDecimal createAnimalDtls(ApplicationModule am){
        ViewObject animalDtlsVO = am.findViewObject("CmnAnimalsVO1");
        Row fpdtlsR = animalDtlsVO.createRow();
        fpdtlsR.setAttribute("ClassId", falconPassDtls.getClassId());
        fpdtlsR.setAttribute("CategoryId", falconPassDtls.getCategoryId());
        fpdtlsR.setAttribute("OriginCountryId", falconPassDtls.getOriginCountryId());
        fpdtlsR.setAttribute("GenderId", falconPassDtls.getGenderId());
        fpdtlsR.setAttribute("SourceId", falconPassDtls.getSourceId());
        fpdtlsR.setAttribute("PitNo", falconPassDtls.getPitNo());
        fpdtlsR.setAttribute("CitesPermitNo", falconPassDtls.getCitesPermitNo());
        animalDtlsVO.insertRow(fpdtlsR);
        am.getTransaction().postChanges();

        DBSequence seq = (DBSequence) animalDtlsVO.getCurrentRow().getAttribute("Id");
        return BigDecimal.valueOf(seq.getValue());
    }
}
