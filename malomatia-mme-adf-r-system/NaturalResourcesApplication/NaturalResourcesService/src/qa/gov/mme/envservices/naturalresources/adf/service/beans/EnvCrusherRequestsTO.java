package qa.gov.mme.envservices.naturalresources.adf.service.beans;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import oracle.jbo.ApplicationModule;
import oracle.jbo.Row;
import oracle.jbo.ViewObject;

import qa.gov.mme.envservices.naturalresources.adf.service.beans.constant.EnvNaturalResServiceConstant;
import qa.gov.mme.envservices.naturalresources.adf.service.beans.request.EnvCrusherLocCoordinatesTO;
import qa.gov.mme.envservices.naturalresources.adf.service.beans.request.EnvCrusherPermitCategoriesTO;

public class EnvCrusherRequestsTO extends EnvNaturalResourcesCommonTO{
    @SuppressWarnings("compatibility:-3548921842878494024")
    private static final long serialVersionUID = 1L;

    public EnvCrusherRequestsTO() {
        super();
        processName = EnvNaturalResServiceConstant.BPM_CRUSHER_PROCESS_NAME;
        processPayload = EnvNaturalResServiceConstant.BPM_CRUSHER_PROCESS_PAYLOAD;
    }
    
    @XmlElement
    protected List<EnvCrusherLocCoordinatesTO> crusherLocList;
    
    @XmlElement
    protected List<EnvCrusherPermitCategoriesTO> crushPmtCategoryList;
    
    protected void createCrusherLocList(ApplicationModule am, BigDecimal dltsId){
        if (null!= crusherLocList && crusherLocList.size()>0) {
            for (EnvCrusherLocCoordinatesTO locs : crusherLocList) {
                ViewObject crusherLocVO = am.findViewObject("EnvCrusherLocCoordinatesVO1");
                Row locsR = crusherLocVO.createRow();     
                locsR.setAttribute("PermitDetailsId", dltsId);
                locsR.setAttribute("Longtitude", locs.getLongtitude());
                locsR.setAttribute("Latitude", locs.getLatitude());
                locsR.setAttribute("CoordinateOrder", locs.getCoordinateOrder());
                locsR.setAttribute("CreatedBy", getCreatedBy());
                crusherLocVO.insertRow(locsR);
            }
        }
    }
    
    protected void createCrushCategoryList(ApplicationModule am, BigDecimal dltsId){
        if (null!= crushPmtCategoryList && crushPmtCategoryList.size()>0) {
            for (EnvCrusherPermitCategoriesTO cats : crushPmtCategoryList) {
                ViewObject crusherCatVO = am.findViewObject("EnvCrusherPmtCategoriesVO1");
                Row catsR = crusherCatVO.createRow();   
                catsR.setAttribute("PermitDetailsId", dltsId);
                catsR.setAttribute("CategoryId", cats.getCategoryId());
                catsR.setAttribute("CreatedBy", getCreatedBy());
                crusherCatVO.insertRow(catsR);
            }
        }
    }

    public void setCrusherLocList(List<EnvCrusherLocCoordinatesTO> crusherLocList) {
        this.crusherLocList = crusherLocList;
    }

    public List<EnvCrusherLocCoordinatesTO> getCrusherLocList() {
        return crusherLocList;
    }

    public void setCrushPmtCategoryList(List<EnvCrusherPermitCategoriesTO> crushPmtCategoryList) {
        this.crushPmtCategoryList = crushPmtCategoryList;
    }

    public List<EnvCrusherPermitCategoriesTO> getCrushPmtCategoryList() {
        return crushPmtCategoryList;
    }
}
