package qa.gov.mme.envservices.falconpassport.model.bc.app;

import qa.gov.mme.common.model.bc.extension.MmeApplicationModuleImpl;
import qa.gov.mme.common.model.bc.extension.MmeViewObjectImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Thu Nov 28 10:50:15 AST 2019
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class FalconPassportServiceAMImpl extends MmeApplicationModuleImpl {
    /**
     * This is the default constructor (do not remove).
     */
    public FalconPassportServiceAMImpl() {
    }

    /**
     * Container's getter for CmnAnimalsVO1.
     * @return CmnAnimalsVO1
     */
    public MmeViewObjectImpl getCmnAnimalsVO1() {
        return (MmeViewObjectImpl) findViewObject("CmnAnimalsVO1");
    }

    /**
     * Container's getter for EnvIssueFlcPassRequestsVO1.
     * @return EnvIssueFlcPassRequestsVO1
     */
    public MmeViewObjectImpl getEnvIssueFlcPassRequestsVO1() {
        return (MmeViewObjectImpl) findViewObject("EnvIssueFlcPassRequestsVO1");
    }
}

