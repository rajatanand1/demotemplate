package qa.gov.mme.envservices.naturalresources.model.bc.view;

import java.math.BigDecimal;

import java.sql.Timestamp;

import oracle.jbo.RowIterator;
import oracle.jbo.RowSet;
import oracle.jbo.domain.DBSequence;
import oracle.jbo.domain.Date;

import qa.gov.mme.common.model.bc.extension.MmeEntityImpl;
import qa.gov.mme.common.model.bc.extension.MmeViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Nov 13 15:30:33 AST 2019
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class EnvCrusherPermitDetailsVORowImpl extends MmeViewRowImpl {


    public static final int ENTITY_ENVCRUSHERPERMITDETAILSEO = 0;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    protected enum AttributesEnum {
        Id,
        ContractorId,
        ContractorMobileNo,
        EnvironmentalPermitId,
        CreatedOn,
        CreatedBy,
        ModifiedOn,
        ModifiedBy,
        CrushersCount,
        SiftingUnitsCount,
        DebrisInventoryId,
        DebrisInventoryIdSwitcherT,
        PermitDurationNewT,
        ContractorEIDT,
        ContractorCPNoT,
        ContractorCRNoT,
        ContractorEmailT,
        ContractorNameEnT,
        ContractorNameArT,
        LocaleT,
        EnvPermitNoT,
        EnvPermitNoExpDateT,
        ContractorEIDTypeNameArT,
        ContractorEIDTypeIdT,
        ContractorEIDTypeNameEnT,
        PermitDurationRenewT,
        PermitDurationModifyT,
        EnvCrusherPermitsVO,
        EnvCrusherLocCoordinatesVO,
        EnvCrusherPmtCategoriesVO,
        EnvCrusherPermitsVO1,
        EnvEnvironmentalPermitsVO,
        EnvCrusherLocCoordinatesVO1,
        EnvCrusherPmtCategoriesVO1,
        EnvPermitDetailsRVO,
        EnvDebrisInventoriesLOVA,
        CmnRegisteredCompaniesRVA;
        private static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        protected int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        protected static final int firstIndex() {
            return firstIndex;
        }

        protected static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        protected static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int ID = AttributesEnum.Id.index();
    public static final int CONTRACTORID = AttributesEnum.ContractorId.index();
    public static final int CONTRACTORMOBILENO = AttributesEnum.ContractorMobileNo.index();
    public static final int ENVIRONMENTALPERMITID = AttributesEnum.EnvironmentalPermitId.index();
    public static final int CREATEDON = AttributesEnum.CreatedOn.index();
    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int MODIFIEDON = AttributesEnum.ModifiedOn.index();
    public static final int MODIFIEDBY = AttributesEnum.ModifiedBy.index();
    public static final int CRUSHERSCOUNT = AttributesEnum.CrushersCount.index();
    public static final int SIFTINGUNITSCOUNT = AttributesEnum.SiftingUnitsCount.index();
    public static final int DEBRISINVENTORYID = AttributesEnum.DebrisInventoryId.index();
    public static final int DEBRISINVENTORYIDSWITCHERT = AttributesEnum.DebrisInventoryIdSwitcherT.index();
    public static final int PERMITDURATIONNEWT = AttributesEnum.PermitDurationNewT.index();
    public static final int CONTRACTOREIDT = AttributesEnum.ContractorEIDT.index();
    public static final int CONTRACTORCPNOT = AttributesEnum.ContractorCPNoT.index();
    public static final int CONTRACTORCRNOT = AttributesEnum.ContractorCRNoT.index();
    public static final int CONTRACTOREMAILT = AttributesEnum.ContractorEmailT.index();
    public static final int CONTRACTORNAMEENT = AttributesEnum.ContractorNameEnT.index();
    public static final int CONTRACTORNAMEART = AttributesEnum.ContractorNameArT.index();
    public static final int LOCALET = AttributesEnum.LocaleT.index();
    public static final int ENVPERMITNOT = AttributesEnum.EnvPermitNoT.index();
    public static final int ENVPERMITNOEXPDATET = AttributesEnum.EnvPermitNoExpDateT.index();
    public static final int CONTRACTOREIDTYPENAMEART = AttributesEnum.ContractorEIDTypeNameArT.index();
    public static final int CONTRACTOREIDTYPEIDT = AttributesEnum.ContractorEIDTypeIdT.index();
    public static final int CONTRACTOREIDTYPENAMEENT = AttributesEnum.ContractorEIDTypeNameEnT.index();
    public static final int PERMITDURATIONRENEWT = AttributesEnum.PermitDurationRenewT.index();
    public static final int PERMITDURATIONMODIFYT = AttributesEnum.PermitDurationModifyT.index();
    public static final int ENVCRUSHERPERMITSVO = AttributesEnum.EnvCrusherPermitsVO.index();
    public static final int ENVCRUSHERLOCCOORDINATESVO = AttributesEnum.EnvCrusherLocCoordinatesVO.index();
    public static final int ENVCRUSHERPMTCATEGORIESVO = AttributesEnum.EnvCrusherPmtCategoriesVO.index();
    public static final int ENVCRUSHERPERMITSVO1 = AttributesEnum.EnvCrusherPermitsVO1.index();
    public static final int ENVENVIRONMENTALPERMITSVO = AttributesEnum.EnvEnvironmentalPermitsVO.index();
    public static final int ENVCRUSHERLOCCOORDINATESVO1 = AttributesEnum.EnvCrusherLocCoordinatesVO1.index();
    public static final int ENVCRUSHERPMTCATEGORIESVO1 = AttributesEnum.EnvCrusherPmtCategoriesVO1.index();
    public static final int ENVPERMITDETAILSRVO = AttributesEnum.EnvPermitDetailsRVO.index();
    public static final int ENVDEBRISINVENTORIESLOVA = AttributesEnum.EnvDebrisInventoriesLOVA.index();
    public static final int CMNREGISTEREDCOMPANIESRVA = AttributesEnum.CmnRegisteredCompaniesRVA.index();

    /**
     * This is the default constructor (do not remove).
     */
    public EnvCrusherPermitDetailsVORowImpl() {
    }

    /**
     * Gets EnvCrusherPermitDetailsEO entity object.
     * @return the EnvCrusherPermitDetailsEO
     */
    public MmeEntityImpl getEnvCrusherPermitDetailsEO() {
        return (MmeEntityImpl) getEntity(ENTITY_ENVCRUSHERPERMITDETAILSEO);
    }

    /**
     * Gets the attribute value for ID using the alias name Id.
     * @return the ID
     */
    public DBSequence getId() {
        return (DBSequence) getAttributeInternal(ID);
    }

    /**
     * Sets <code>value</code> as attribute value for ID using the alias name Id.
     * @param value value to set the ID
     */
    public void setId(DBSequence value) {
        setAttributeInternal(ID, value);
    }

    /**
     * Gets the attribute value for CONTRACTOR_ID using the alias name ContractorId.
     * @return the CONTRACTOR_ID
     */
    public BigDecimal getContractorId() {
        return (BigDecimal) getAttributeInternal(CONTRACTORID);
    }

    /**
     * Sets <code>value</code> as attribute value for CONTRACTOR_ID using the alias name ContractorId.
     * @param value value to set the CONTRACTOR_ID
     */
    public void setContractorId(BigDecimal value) {
        setAttributeInternal(CONTRACTORID, value);
    }

    /**
     * Gets the attribute value for CONTRACTOR_MOBILE_NO using the alias name ContractorMobileNo.
     * @return the CONTRACTOR_MOBILE_NO
     */
    public String getContractorMobileNo() {
        return (String) getAttributeInternal(CONTRACTORMOBILENO);
    }

    /**
     * Sets <code>value</code> as attribute value for CONTRACTOR_MOBILE_NO using the alias name ContractorMobileNo.
     * @param value value to set the CONTRACTOR_MOBILE_NO
     */
    public void setContractorMobileNo(String value) {
        setAttributeInternal(CONTRACTORMOBILENO, value);
    }

    /**
     * Gets the attribute value for ENVIRONMENTAL_PERMIT_ID using the alias name EnvironmentalPermitId.
     * @return the ENVIRONMENTAL_PERMIT_ID
     */
    public BigDecimal getEnvironmentalPermitId() {
        return (BigDecimal) getAttributeInternal(ENVIRONMENTALPERMITID);
    }

    /**
     * Sets <code>value</code> as attribute value for ENVIRONMENTAL_PERMIT_ID using the alias name EnvironmentalPermitId.
     * @param value value to set the ENVIRONMENTAL_PERMIT_ID
     */
    public void setEnvironmentalPermitId(BigDecimal value) {
        setAttributeInternal(ENVIRONMENTALPERMITID, value);
    }

    /**
     * Gets the attribute value for CREATED_ON using the alias name CreatedOn.
     * @return the CREATED_ON
     */
    public Timestamp getCreatedOn() {
        return (Timestamp) getAttributeInternal(CREATEDON);
    }

    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for CREATED_BY using the alias name CreatedBy.
     * @param value value to set the CREATED_BY
     */
    public void setCreatedBy(String value) {
        setAttributeInternal(CREATEDBY, value);
    }

    /**
     * Gets the attribute value for MODIFIED_ON using the alias name ModifiedOn.
     * @return the MODIFIED_ON
     */
    public Timestamp getModifiedOn() {
        return (Timestamp) getAttributeInternal(MODIFIEDON);
    }

    /**
     * Gets the attribute value for MODIFIED_BY using the alias name ModifiedBy.
     * @return the MODIFIED_BY
     */
    public String getModifiedBy() {
        return (String) getAttributeInternal(MODIFIEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for MODIFIED_BY using the alias name ModifiedBy.
     * @param value value to set the MODIFIED_BY
     */
    public void setModifiedBy(String value) {
        setAttributeInternal(MODIFIEDBY, value);
    }

    /**
     * Gets the attribute value for CRUSHERS_COUNT using the alias name CrushersCount.
     * @return the CRUSHERS_COUNT
     */
    public BigDecimal getCrushersCount() {
        return (BigDecimal) getAttributeInternal(CRUSHERSCOUNT);
    }

    /**
     * Sets <code>value</code> as attribute value for CRUSHERS_COUNT using the alias name CrushersCount.
     * @param value value to set the CRUSHERS_COUNT
     */
    public void setCrushersCount(BigDecimal value) {
        setAttributeInternal(CRUSHERSCOUNT, value);
    }

    /**
     * Gets the attribute value for SIFTING_UNITS_COUNT using the alias name SiftingUnitsCount.
     * @return the SIFTING_UNITS_COUNT
     */
    public BigDecimal getSiftingUnitsCount() {
        return (BigDecimal) getAttributeInternal(SIFTINGUNITSCOUNT);
    }

    /**
     * Sets <code>value</code> as attribute value for SIFTING_UNITS_COUNT using the alias name SiftingUnitsCount.
     * @param value value to set the SIFTING_UNITS_COUNT
     */
    public void setSiftingUnitsCount(BigDecimal value) {
        setAttributeInternal(SIFTINGUNITSCOUNT, value);
    }

    /**
     * Gets the attribute value for DEBRIS_INVENTORY_ID using the alias name DebrisInventoryId.
     * @return the DEBRIS_INVENTORY_ID
     */
    public BigDecimal getDebrisInventoryId() {
        return (BigDecimal) getAttributeInternal(DEBRISINVENTORYID);
    }

    /**
     * Sets <code>value</code> as attribute value for DEBRIS_INVENTORY_ID using the alias name DebrisInventoryId.
     * @param value value to set the DEBRIS_INVENTORY_ID
     */
    public void setDebrisInventoryId(BigDecimal value) {
        setAttributeInternal(DEBRISINVENTORYID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute DebrisInventoryIdSwitcherT.
     * @return the DebrisInventoryIdSwitcherT
     */
    public String getDebrisInventoryIdSwitcherT() {
        return (String) getAttributeInternal(DEBRISINVENTORYIDSWITCHERT);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute DebrisInventoryIdSwitcherT.
     * @param value value to set the  DebrisInventoryIdSwitcherT
     */
    public void setDebrisInventoryIdSwitcherT(String value) {
        setAttributeInternal(DEBRISINVENTORYIDSWITCHERT, value);
    }


    /**
     * Gets the attribute value for the calculated attribute getPermitDurationNewT.
     * @return the getPermitDurationNewT
     */
    public String getPermitDurationNewT() {
        return (String) getAttributeInternal(PERMITDURATIONNEWT);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute PermitDurationCalT.
     * @param value value to set the  PermitDurationCalT
     */
    public void setPermitDurationNewT(String value) {
        setAttributeInternal(PERMITDURATIONNEWT, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ContractorEIDT.
     * @return the ContractorEIDT
     */
    public String getContractorEIDT() {
        return (String) getAttributeInternal(CONTRACTOREIDT);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ContractorEIDT.
     * @param value value to set the  ContractorEIDT
     */
    public void setContractorEIDT(String value) {
        setAttributeInternal(CONTRACTOREIDT, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ContractorCPNoT.
     * @return the ContractorCPNoT
     */
    public String getContractorCPNoT() {
        return (String) getAttributeInternal(CONTRACTORCPNOT);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ContractorCPNoT.
     * @param value value to set the  ContractorCPNoT
     */
    public void setContractorCPNoT(String value) {
        setAttributeInternal(CONTRACTORCPNOT, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ContractorCRNoT.
     * @return the ContractorCRNoT
     */
    public String getContractorCRNoT() {
        return (String) getAttributeInternal(CONTRACTORCRNOT);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ContractorCRNoT.
     * @param value value to set the  ContractorCRNoT
     */
    public void setContractorCRNoT(String value) {
        setAttributeInternal(CONTRACTORCRNOT, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ContractorEmailT.
     * @return the ContractorEmailT
     */
    public String getContractorEmailT() {
        return (String) getAttributeInternal(CONTRACTOREMAILT);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ContractorEmailT.
     * @param value value to set the  ContractorEmailT
     */
    public void setContractorEmailT(String value) {
        setAttributeInternal(CONTRACTOREMAILT, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ContractorNameEnT.
     * @return the ContractorNameEnT
     */
    public String getContractorNameEnT() {
        return (String) getAttributeInternal(CONTRACTORNAMEENT);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ContractorNameEnT.
     * @param value value to set the  ContractorNameEnT
     */
    public void setContractorNameEnT(String value) {
        setAttributeInternal(CONTRACTORNAMEENT, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ContractorNameArT.
     * @return the ContractorNameArT
     */
    public String getContractorNameArT() {
        return (String) getAttributeInternal(CONTRACTORNAMEART);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ContractorNameArT.
     * @param value value to set the  ContractorNameArT
     */
    public void setContractorNameArT(String value) {
        setAttributeInternal(CONTRACTORNAMEART, value);
    }

    /**
     * Gets the attribute value for the calculated attribute LocaleT.
     * @return the LocaleT
     */
    public String getLocaleT() {
        return (String) getAttributeInternal(LOCALET);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute LocaleT.
     * @param value value to set the  LocaleT
     */
    public void setLocaleT(String value) {
        setAttributeInternal(LOCALET, value);
    }

    /**
     * Gets the attribute value for the calculated attribute EnvPermitNoT.
     * @return the EnvPermitNoT
     */
    public String getEnvPermitNoT() {
        return (String) getAttributeInternal(ENVPERMITNOT);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute EnvPermitNoT.
     * @param value value to set the  EnvPermitNoT
     */
    public void setEnvPermitNoT(String value) {
        setAttributeInternal(ENVPERMITNOT, value);
    }

    /**
     * Gets the attribute value for the calculated attribute EnvPermitNoExpDateT.
     * @return the EnvPermitNoExpDateT
     */
    public Date getEnvPermitNoExpDateT() {
        return (Date) getAttributeInternal(ENVPERMITNOEXPDATET);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute EnvPermitNoExpDateT.
     * @param value value to set the  EnvPermitNoExpDateT
     */
    public void setEnvPermitNoExpDateT(Date value) {
        setAttributeInternal(ENVPERMITNOEXPDATET, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ContractorEIDTypeT.
     * @return the ContractorEIDTypeT
     */
    public String getContractorEIDTypeNameArT() {
        return (String) getAttributeInternal(CONTRACTOREIDTYPENAMEART);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ContractorEIDTypeT.
     * @param value value to set the  ContractorEIDTypeT
     */
    public void setContractorEIDTypeNameArT(String value) {
        setAttributeInternal(CONTRACTOREIDTYPENAMEART, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ContractorEIDTypeIdT.
     * @return the ContractorEIDTypeIdT
     */
    public String getContractorEIDTypeIdT() {
        return (String) getAttributeInternal(CONTRACTOREIDTYPEIDT);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ContractorEIDTypeIdT.
     * @param value value to set the  ContractorEIDTypeIdT
     */
    public void setContractorEIDTypeIdT(String value) {
        setAttributeInternal(CONTRACTOREIDTYPEIDT, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ContractorEIDTypeNameEnT.
     * @return the ContractorEIDTypeNameEnT
     */
    public String getContractorEIDTypeNameEnT() {
        return (String) getAttributeInternal(CONTRACTOREIDTYPENAMEENT);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ContractorEIDTypeNameEnT.
     * @param value value to set the  ContractorEIDTypeNameEnT
     */
    public void setContractorEIDTypeNameEnT(String value) {
        setAttributeInternal(CONTRACTOREIDTYPENAMEENT, value);
    }

    /**
     * Gets the attribute value for the calculated attribute PermitDurationRenewT.
     * @return the PermitDurationRenewT
     */
    public String getPermitDurationRenewT() {
        return (String) getAttributeInternal(PERMITDURATIONRENEWT);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute PermitDurationRenewT.
     * @param value value to set the  PermitDurationRenewT
     */
    public void setPermitDurationRenewT(String value) {
        setAttributeInternal(PERMITDURATIONRENEWT, value);
    }

    /**
     * Gets the attribute value for the calculated attribute PermitDurationModifyT.
     * @return the PermitDurationModifyT
     */
    public String getPermitDurationModifyT() {
        return (String) getAttributeInternal(PERMITDURATIONMODIFYT);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute PermitDurationModifyT.
     * @param value value to set the  PermitDurationModifyT
     */
    public void setPermitDurationModifyT(String value) {
        setAttributeInternal(PERMITDURATIONMODIFYT, value);
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link EnvCrusherPermitsVO.
     */
    public RowIterator getEnvCrusherPermitsVO() {
        return (RowIterator) getAttributeInternal(ENVCRUSHERPERMITSVO);
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link EnvCrusherLocCoordinatesVO.
     */
    public RowIterator getEnvCrusherLocCoordinatesVO() {
        return (RowIterator) getAttributeInternal(ENVCRUSHERLOCCOORDINATESVO);
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link EnvCrusherPmtCategoriesVO.
     */
    public RowIterator getEnvCrusherPmtCategoriesVO() {
        return (RowIterator) getAttributeInternal(ENVCRUSHERPMTCATEGORIESVO);
    }


    /**
     * Gets the associated <code>EnvCrusherPermitsVORowImpl</code> using master-detail link EnvCrusherPermitsVO1.
     */
    public EnvCrusherPermitsVORowImpl getEnvCrusherPermitsVO1() {
        return (EnvCrusherPermitsVORowImpl) getAttributeInternal(ENVCRUSHERPERMITSVO1);
    }

    /**
     * Sets the master-detail link EnvCrusherPermitsVO1 between this object and <code>value</code>.
     */
    public void setEnvCrusherPermitsVO1(EnvCrusherPermitsVORowImpl value) {
        setAttributeInternal(ENVCRUSHERPERMITSVO1, value);
    }

    /**
     * Gets the associated <code>MmeViewRowImpl</code> using master-detail link EnvEnvironmentalPermitsVO.
     */
    public MmeViewRowImpl getEnvEnvironmentalPermitsVO() {
        return (MmeViewRowImpl) getAttributeInternal(ENVENVIRONMENTALPERMITSVO);
    }

    /**
     * Sets the master-detail link EnvEnvironmentalPermitsVO between this object and <code>value</code>.
     */
    public void setEnvEnvironmentalPermitsVO(MmeViewRowImpl value) {
        setAttributeInternal(ENVENVIRONMENTALPERMITSVO, value);
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link EnvCrusherLocCoordinatesVO1.
     */
    public RowIterator getEnvCrusherLocCoordinatesVO1() {
        return (RowIterator) getAttributeInternal(ENVCRUSHERLOCCOORDINATESVO1);
    }


    /**
     * Gets the associated <code>RowIterator</code> using master-detail link EnvCrusherPmtCategoriesVO1.
     */
    public RowIterator getEnvCrusherPmtCategoriesVO1() {
        return (RowIterator) getAttributeInternal(ENVCRUSHERPMTCATEGORIESVO1);
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link EnvPermitDetailsRVO.
     */
    public RowIterator getEnvPermitDetailsRVO() {
        return (RowIterator) getAttributeInternal(ENVPERMITDETAILSRVO);
    }

    /**
     * Gets the view accessor <code>RowSet</code> EnvDebrisInventoriesLOVA.
     */
    public RowSet getEnvDebrisInventoriesLOVA() {
        return (RowSet) getAttributeInternal(ENVDEBRISINVENTORIESLOVA);
    }

    /**
     * Gets the view accessor <code>RowSet</code> CmnRegisteredCompaniesRVA.
     */
    public RowSet getCmnRegisteredCompaniesRVA() {
        return (RowSet) getAttributeInternal(CMNREGISTEREDCOMPANIESRVA);
    }
}

