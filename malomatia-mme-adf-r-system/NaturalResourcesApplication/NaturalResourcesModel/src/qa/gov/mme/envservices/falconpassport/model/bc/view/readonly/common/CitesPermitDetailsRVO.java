package qa.gov.mme.envservices.falconpassport.model.bc.view.readonly.common;

import oracle.jbo.Row;
import oracle.jbo.ViewObject;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Thu Nov 28 16:29:42 AST 2019
// ---------------------------------------------------------------------
public interface CitesPermitDetailsRVO extends ViewObject {
    Row doFilterCitePermitDetails(String pCitePermitNo);

    void clearCitePermitDetails();
}

