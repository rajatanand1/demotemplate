package qa.gov.mme.envservices.naturalresources.model.utils;

import java.util.ResourceBundle;
import oracle.javatools.resourcebundle.BundleFactory;

import qa.gov.mme.common.model.exception.MmeExtJboException;
//import qa.gov.mme.common.model.utils.MmeModelUtils;
import qa.gov.mme.common.model.utils.MmeModelUtils;
import qa.gov.mme.envservices.naturalresources.model.constant.NaturalResourcesModelConstant;


public class NaturalResourcesModelBundleUtils {
    public NaturalResourcesModelBundleUtils() {
        super();
    }
    public static void validationFromResourceBundle(String key){
        ResourceBundle bundle = null;
//        try {
            bundle =
                BundleFactory.getBundle(NaturalResourcesModelConstant.NATURAL_RESOURCES_MODEL_BUNDLE,
                                        MmeModelUtils.getCurrentLocale());
            if (bundle != null) {
                String message = null;
                message = bundle.getString(key);
                if (message != null) {
                    throw new MmeExtJboException(message);
                }
            }
//        } 
//        catch (MmeExtJboException meje) {
//            // TODO: Add catch code
//            meje.printStackTrace();
//        }
    }
}
