package qa.gov.mme.envservices.naturalresources.model.constant;


public class NaturalResourcesModelConstant {
    public NaturalResourcesModelConstant() {
        super();
    }
    public static final String NATURAL_RESOURCES_MODEL_BUNDLE =
        "qa.gov.mme.envservices.naturalresources.model.bundle.NaturalResourcesModelBundle";
    public static final String DATE_TIME_FORMAT = "dd-MM-yyyy HH:mm:ss";
    public static final String LOCALE_AR = "ar_QA";
    public static final String MODE_VIEW = "VIEW";
    public static final String MODE_CREATE = "CREATE";
    public static final String CHAR_Y = "Y";
    public static final String CHAR_N = "N";
    public static final String BENEFICIARY_QUERY_STR =
        " AND (req.beneficiary_id = entities.id OR req.beneficiary_id = entities.parent_entity_id) ORDER BY req.ID desc";
    public static final String APPLICANT_QUERY_STR = " AND ( req.applicant_id = entities.id ) ORDER BY req.ID desc";
    public static final String CANT_RENEW_CANCALLED_PERMIT_MSG = "CANT_RENEW_CANCALLED_PERMIT_MSG";
    public static final String WRONG_PERMIT_NO_EXP_DATE_MSG = "WRONG_PERMIT_NO_EXP_DATE_MSG";
    public static final String REQ_BENEFICIARY_NOT_PERMIT_HOLDER_MSG="REQ_BENEFICIARY_NOT_PERMIT_HOLDER_MSG";
}
