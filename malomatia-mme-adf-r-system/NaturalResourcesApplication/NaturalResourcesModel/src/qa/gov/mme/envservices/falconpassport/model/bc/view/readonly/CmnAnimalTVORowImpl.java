package qa.gov.mme.envservices.falconpassport.model.bc.view.readonly;

import java.math.BigDecimal;

import oracle.jbo.RowSet;
import oracle.jbo.domain.Date;

import qa.gov.mme.common.model.bc.extension.MmeViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Thu Nov 28 12:58:29 AST 2019
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class CmnAnimalTVORowImpl extends MmeViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    protected enum AttributesEnum {
        J1,
        Id,
        CategoryId,
        RingNo,
        OriginCountryId,
        GenderId,
        BirthDate,
        PitNo,
        SourceId,
        ClassId,
        CitesPermitNo,
        AppendixId,
        IsDeceased,
        CreatedBy,
        CreatedOn,
        ModifiedBy,
        ModifiedOn,
        CatergoryName,
        OriginCountryName,
        GenderName,
        SourceName,
        ClassName,
        CategoryTypeLOV1,
        OriginCountryLOV1,
        GenderLOV1,
        FolconSourceLOV1,
        FalconClassLOV1,
        CategoryTypeLOV2;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        protected int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        protected static final int firstIndex() {
            return firstIndex;
        }

        protected static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        protected static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }
    public static final int J1 = AttributesEnum.J1.index();
    public static final int ID = AttributesEnum.Id.index();
    public static final int CATEGORYID = AttributesEnum.CategoryId.index();
    public static final int RINGNO = AttributesEnum.RingNo.index();
    public static final int ORIGINCOUNTRYID = AttributesEnum.OriginCountryId.index();
    public static final int GENDERID = AttributesEnum.GenderId.index();
    public static final int BIRTHDATE = AttributesEnum.BirthDate.index();
    public static final int PITNO = AttributesEnum.PitNo.index();
    public static final int SOURCEID = AttributesEnum.SourceId.index();
    public static final int CLASSID = AttributesEnum.ClassId.index();
    public static final int CITESPERMITNO = AttributesEnum.CitesPermitNo.index();
    public static final int APPENDIXID = AttributesEnum.AppendixId.index();
    public static final int ISDECEASED = AttributesEnum.IsDeceased.index();
    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATEDON = AttributesEnum.CreatedOn.index();
    public static final int MODIFIEDBY = AttributesEnum.ModifiedBy.index();
    public static final int MODIFIEDON = AttributesEnum.ModifiedOn.index();
    public static final int CATERGORYNAME = AttributesEnum.CatergoryName.index();
    public static final int ORIGINCOUNTRYNAME = AttributesEnum.OriginCountryName.index();
    public static final int GENDERNAME = AttributesEnum.GenderName.index();
    public static final int SOURCENAME = AttributesEnum.SourceName.index();
    public static final int CLASSNAME = AttributesEnum.ClassName.index();
    public static final int CATEGORYTYPELOV1 = AttributesEnum.CategoryTypeLOV1.index();
    public static final int ORIGINCOUNTRYLOV1 = AttributesEnum.OriginCountryLOV1.index();
    public static final int GENDERLOV1 = AttributesEnum.GenderLOV1.index();
    public static final int FOLCONSOURCELOV1 = AttributesEnum.FolconSourceLOV1.index();
    public static final int FALCONCLASSLOV1 = AttributesEnum.FalconClassLOV1.index();
    public static final int CATEGORYTYPELOV2 = AttributesEnum.CategoryTypeLOV2.index();

    /**
     * This is the default constructor (do not remove).
     */
    public CmnAnimalTVORowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute J1.
     * @return the J1
     */
    public BigDecimal getJ1() {
        return (BigDecimal) getAttributeInternal(J1);
    }

    /**
     * Gets the attribute value for the calculated attribute Id.
     * @return the Id
     */
    public BigDecimal getId() {
        return (BigDecimal) getAttributeInternal(ID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Id.
     * @param value value to set the  Id
     */
    public void setId(BigDecimal value) {
        setAttributeInternal(ID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute CategoryId.
     * @return the CategoryId
     */
    public BigDecimal getCategoryId() {
        return (BigDecimal) getAttributeInternal(CATEGORYID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute CategoryId.
     * @param value value to set the  CategoryId
     */
    public void setCategoryId(BigDecimal value) {
        setAttributeInternal(CATEGORYID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute RingNo.
     * @return the RingNo
     */
    public String getRingNo() {
        return (String) getAttributeInternal(RINGNO);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute RingNo.
     * @param value value to set the  RingNo
     */
    public void setRingNo(String value) {
        setAttributeInternal(RINGNO, value);
    }

    /**
     * Gets the attribute value for the calculated attribute OriginCountryId.
     * @return the OriginCountryId
     */
    public BigDecimal getOriginCountryId() {
        return (BigDecimal) getAttributeInternal(ORIGINCOUNTRYID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute OriginCountryId.
     * @param value value to set the  OriginCountryId
     */
    public void setOriginCountryId(BigDecimal value) {
        setAttributeInternal(ORIGINCOUNTRYID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute GenderId.
     * @return the GenderId
     */
    public BigDecimal getGenderId() {
        return (BigDecimal) getAttributeInternal(GENDERID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute GenderId.
     * @param value value to set the  GenderId
     */
    public void setGenderId(BigDecimal value) {
        setAttributeInternal(GENDERID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute BirthDate.
     * @return the BirthDate
     */
    public Date getBirthDate() {
        return (Date) getAttributeInternal(BIRTHDATE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute BirthDate.
     * @param value value to set the  BirthDate
     */
    public void setBirthDate(Date value) {
        setAttributeInternal(BIRTHDATE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute PitNo.
     * @return the PitNo
     */
    public String getPitNo() {
        return (String) getAttributeInternal(PITNO);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute PitNo.
     * @param value value to set the  PitNo
     */
    public void setPitNo(String value) {
        setAttributeInternal(PITNO, value);
    }

    /**
     * Gets the attribute value for the calculated attribute SourceId.
     * @return the SourceId
     */
    public BigDecimal getSourceId() {
        return (BigDecimal) getAttributeInternal(SOURCEID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute SourceId.
     * @param value value to set the  SourceId
     */
    public void setSourceId(BigDecimal value) {
        setAttributeInternal(SOURCEID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ClassId.
     * @return the ClassId
     */
    public BigDecimal getClassId() {
        return (BigDecimal) getAttributeInternal(CLASSID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ClassId.
     * @param value value to set the  ClassId
     */
    public void setClassId(BigDecimal value) {
        setAttributeInternal(CLASSID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute CitesPermitNo.
     * @return the CitesPermitNo
     */
    public String getCitesPermitNo() {
        return (String) getAttributeInternal(CITESPERMITNO);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute CitesPermitNo.
     * @param value value to set the  CitesPermitNo
     */
    public void setCitesPermitNo(String value) {
        setAttributeInternal(CITESPERMITNO, value);
    }

    /**
     * Gets the attribute value for the calculated attribute AppendixId.
     * @return the AppendixId
     */
    public BigDecimal getAppendixId() {
        return (BigDecimal) getAttributeInternal(APPENDIXID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute AppendixId.
     * @param value value to set the  AppendixId
     */
    public void setAppendixId(BigDecimal value) {
        setAttributeInternal(APPENDIXID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute IsDeceased.
     * @return the IsDeceased
     */
    public String getIsDeceased() {
        return (String) getAttributeInternal(ISDECEASED);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute IsDeceased.
     * @param value value to set the  IsDeceased
     */
    public void setIsDeceased(String value) {
        setAttributeInternal(ISDECEASED, value);
    }

    /**
     * Gets the attribute value for the calculated attribute CreatedBy.
     * @return the CreatedBy
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute CreatedBy.
     * @param value value to set the  CreatedBy
     */
    public void setCreatedBy(String value) {
        setAttributeInternal(CREATEDBY, value);
    }

    /**
     * Gets the attribute value for the calculated attribute CreatedOn.
     * @return the CreatedOn
     */
    public Date getCreatedOn() {
        return (Date) getAttributeInternal(CREATEDON);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute CreatedOn.
     * @param value value to set the  CreatedOn
     */
    public void setCreatedOn(Date value) {
        setAttributeInternal(CREATEDON, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ModifiedBy.
     * @return the ModifiedBy
     */
    public String getModifiedBy() {
        return (String) getAttributeInternal(MODIFIEDBY);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ModifiedBy.
     * @param value value to set the  ModifiedBy
     */
    public void setModifiedBy(String value) {
        setAttributeInternal(MODIFIEDBY, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ModifiedOn.
     * @return the ModifiedOn
     */
    public Date getModifiedOn() {
        return (Date) getAttributeInternal(MODIFIEDON);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ModifiedOn.
     * @param value value to set the  ModifiedOn
     */
    public void setModifiedOn(Date value) {
        setAttributeInternal(MODIFIEDON, value);
    }

    /**
     * Gets the attribute value for the calculated attribute CatergoryName.
     * @return the CatergoryName
     */
    public String getCatergoryName() {
        return (String) getAttributeInternal(CATERGORYNAME);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute CatergoryName.
     * @param value value to set the  CatergoryName
     */
    public void setCatergoryName(String value) {
        setAttributeInternal(CATERGORYNAME, value);
    }

    /**
     * Gets the attribute value for the calculated attribute OriginCountryName.
     * @return the OriginCountryName
     */
    public String getOriginCountryName() {
        return (String) getAttributeInternal(ORIGINCOUNTRYNAME);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute OriginCountryName.
     * @param value value to set the  OriginCountryName
     */
    public void setOriginCountryName(String value) {
        setAttributeInternal(ORIGINCOUNTRYNAME, value);
    }

    /**
     * Gets the attribute value for the calculated attribute GenderName.
     * @return the GenderName
     */
    public String getGenderName() {
        return (String) getAttributeInternal(GENDERNAME);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute GenderName.
     * @param value value to set the  GenderName
     */
    public void setGenderName(String value) {
        setAttributeInternal(GENDERNAME, value);
    }

    /**
     * Gets the attribute value for the calculated attribute SourceName.
     * @return the SourceName
     */
    public String getSourceName() {
        return (String) getAttributeInternal(SOURCENAME);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute SourceName.
     * @param value value to set the  SourceName
     */
    public void setSourceName(String value) {
        setAttributeInternal(SOURCENAME, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ClassName.
     * @return the ClassName
     */
    public String getClassName() {
        return (String) getAttributeInternal(CLASSNAME);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ClassName.
     * @param value value to set the  ClassName
     */
    public void setClassName(String value) {
        setAttributeInternal(CLASSNAME, value);
    }

    /**
     * Gets the view accessor <code>RowSet</code> CategoryTypeLOV1.
     */
    public RowSet getCategoryTypeLOV1() {
        return (RowSet) getAttributeInternal(CATEGORYTYPELOV1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> OriginCountryLOV1.
     */
    public RowSet getOriginCountryLOV1() {
        return (RowSet) getAttributeInternal(ORIGINCOUNTRYLOV1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> GenderLOV1.
     */
    public RowSet getGenderLOV1() {
        return (RowSet) getAttributeInternal(GENDERLOV1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> FolconSourceLOV1.
     */
    public RowSet getFolconSourceLOV1() {
        return (RowSet) getAttributeInternal(FOLCONSOURCELOV1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> FalconClassLOV1.
     */
    public RowSet getFalconClassLOV1() {
        return (RowSet) getAttributeInternal(FALCONCLASSLOV1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> CategoryTypeLOV2.
     */
    public RowSet getCategoryTypeLOV2() {
        return (RowSet) getAttributeInternal(CATEGORYTYPELOV2);
    }
}

