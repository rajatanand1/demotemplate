function showDatePickerTimer(elem) {
    if (!$(document.getElementById(elem.getSource().getClientId())).hasClass('datepikerLoad')) {
        $(document.getElementById(elem.getSource().getClientId())).find("input").datetimepicker( {
         autoclose : true, todayBtn : true, showMeridian: true, date : new Date(),format:"dd/mm/yyyy hh:ii"
        }).datetimepicker('show')
         $(document.getElementById(elem.getSource().getClientId())).find(".calendar-picker").click(function(){
            $(document.getElementById(elem.getSource().getClientId())).find("input").trigger('click');
         })
    }
}