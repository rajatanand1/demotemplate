package DemoExternalTemplateApp.view.beans.inbox;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;

import javax.faces.event.ActionEvent;

import java.io.Serializable;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;

import java.util.Date;
import java.util.List;

import oracle.binding.OperationBinding;

import qa.gov.mme.common.beans.SearchTemplateBean;
import qa.gov.mme.common.utils.ADFUtils;
import qa.gov.mme.common.ws.proxy.client.BpmSoapServiceClient;
import qa.gov.mme.common.ws.proxy.client.MmeTaskInstance;

public class DemoTemplateInboxBean extends SearchTemplateBean implements Serializable {
    @SuppressWarnings("compatibility:-6449415610384434904")
    private static final long serialVersionUID = 1L;

    private List<MmeTaskInstance> tasksList = null;

    public void setTasksList(List<MmeTaskInstance> tasksList) {
        this.tasksList = tasksList;
    }

    public List<MmeTaskInstance> getTasksList() {
        return tasksList;

    }

    public DemoTemplateInboxBean() {
    }

    public void initInbox() {
        System.out.println("init ");
    }

    public void onInboxSearchACL(ActionEvent actionEvent) {
        System.out.println("Calling Search");
        try {
            String lcReqNo = null;
            String beneficiaryName = null;
            String lcReqType = null;
            lcReqNo = (String) ADFUtils.getViewScopeAttribute("pRequestNo");
            beneficiaryName = (String) ADFUtils.getViewScopeAttribute("pBeneficiaryName");
            lcReqType = (String) ADFUtils.getViewScopeAttribute("pInboxRequestType");
            List<MmeTaskInstance> existTasksList = null;
            existTasksList = getBPMTasksList();
            List<MmeTaskInstance> filteredTasksList = null;
            if (null != lcReqNo || null != beneficiaryName || null != lcReqType) {
                filteredTasksList =
                    Lists.newArrayList(Collections2.filter(existTasksList,
                                                           new RequestNumberOrAppNameFilter(lcReqNo, beneficiaryName,
                                                                                            lcReqType)));
                this.setTasksList(filteredTasksList);

            }
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }
        reRenderComponents(new String[] { "t1" });
    }

    public void onInboxClearSearchACL(ActionEvent actionEvent) {
        ADFUtils.putViewScopeAttribute("pInboxRequestType", null);
        initInbox();
        reRenderComponents(new String[] { "t1" });
    }

    private List<MmeTaskInstance> getBPMTasksList() {
        String actionOutCome = null;
        List<MmeTaskInstance> tasksList = null;
        try {
            actionOutCome = (String) ADFUtils.getPageFlowScopeAttribute("pFromOutcome");
            if ("PRIVATE_INBOX_PARAM".equalsIgnoreCase(actionOutCome)) {
                tasksList = getMyBPMTasks();
            } else if ("PUBLIC_INBOX_PARAM".equalsIgnoreCase(actionOutCome)) {
                tasksList = getBPMTasks();
            } else if ("TASKS_ASSIGN_PARAM".equalsIgnoreCase(actionOutCome)) {
                tasksList = getAllAcqiuredByTasks();
            }
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }
        this.setTasksList(tasksList);
        return tasksList;
    }

    public void btnReleaseTask_action(ActionEvent actionEvent) {
        // Add event code here...
    }

    public void claimTaskActionListener(ActionEvent actionEvent) {
        // Add event code here...
    }

    public final class RequestNumberOrAppNameFilter implements Predicate<MmeTaskInstance> {
        private final String beneficiaryName;
        private final String requestNo;
        private final String requestType;

        public RequestNumberOrAppNameFilter(final String requestNo, final String beneficiaryName,
                                            final String requestType) {
            this.requestNo = requestNo;
            this.beneficiaryName = beneficiaryName;
            this.requestType = requestType;
        }

        @Override
        public boolean apply(final MmeTaskInstance mmeTaskInstance) {
            if (requestNo != null && !requestNo.isEmpty() && beneficiaryName != null && !beneficiaryName.isEmpty() &&
                requestType != null && !requestType.isEmpty()) {
                if ((mmeTaskInstance.getReqNumber() != null && mmeTaskInstance.getReqNumber().contains(requestNo)) &&
                    (mmeTaskInstance.getReqBeneficiaryName() != null &&
                     mmeTaskInstance.getReqBeneficiaryName().contains(beneficiaryName)) &&
                    (mmeTaskInstance.getProcessName() != null &&
                     mmeTaskInstance.getProcessName().contains(requestType))) {
                    return true;
                }
            } else if (requestNo != null && !requestNo.isEmpty() && requestType != null && !requestType.isEmpty()) {
                if ((mmeTaskInstance.getReqNumber() != null && mmeTaskInstance.getReqNumber().contains(requestNo)) &&
                    (mmeTaskInstance.getProcessName() != null &&
                     mmeTaskInstance.getProcessName().contains(requestType))) {
                    return true;
                }
            } else if (beneficiaryName != null && !beneficiaryName.isEmpty() && requestType != null &&
                       !requestType.isEmpty()) {
                if ((mmeTaskInstance.getReqBeneficiaryName() != null &&
                     mmeTaskInstance.getReqBeneficiaryName().contains(beneficiaryName)) &&
                    (mmeTaskInstance.getProcessName() != null &&
                     mmeTaskInstance.getProcessName().contains(requestType))) {
                    return true;
                }
            } else if (requestNo != null && !requestNo.isEmpty() && beneficiaryName != null &&
                       !beneficiaryName.isEmpty()) {
                if ((mmeTaskInstance.getReqNumber() != null && mmeTaskInstance.getReqNumber().contains(requestNo)) &&
                    (mmeTaskInstance.getReqBeneficiaryName() != null &&
                     mmeTaskInstance.getReqBeneficiaryName().contains(beneficiaryName))) {
                    return true;
                }
            } else {
                if ((requestNo == null || requestNo.isEmpty()) &&
                    (beneficiaryName == null || beneficiaryName.isEmpty()) &&
                    (requestType == null || requestType.isEmpty())) {
                    return true;
                } else if (requestNo != null && !requestNo.isEmpty() &&
                           (mmeTaskInstance.getReqNumber() != null &&
                            mmeTaskInstance.getReqNumber().contains(requestNo))) {
                    return true;
                } else if ((beneficiaryName != null) && !beneficiaryName.isEmpty() &&
                           (mmeTaskInstance.getReqBeneficiaryName() != null &&
                            mmeTaskInstance.getReqBeneficiaryName().contains(beneficiaryName))) {
                    return true;
                } else if ((requestType != null) && !requestType.isEmpty() &&
                           (mmeTaskInstance.getProcessName() != null &&
                            mmeTaskInstance.getProcessName().contains(requestType))) {
                    return true;
                }
            }
            return false;
        }
    }

    /**
     * This method is used for getting My task list
     */
    private List<MmeTaskInstance> getMyBPMTasks() {
        String lcReqType = null;
        BpmSoapServiceClient bpmSoapServiceClient = null;
        bpmSoapServiceClient = new BpmSoapServiceClient();
        lcReqType = (String) ADFUtils.getViewScopeAttribute("pInboxRequestType");
        List<MmeTaskInstance> myTasksList = null;
        if (null != lcReqType) {
            myTasksList = bpmSoapServiceClient.retrieveUserMyTasks(this.getLoggedInUser(), null);
        } else {
            myTasksList =
                bpmSoapServiceClient.retrieveUserMyTasks(this.getLoggedInUser(), "NATURAL_RESOURCES_PRIV_PROCESS_DEV");
        }
        System.out.println("myTasksList size:::" + myTasksList.size());
        return myTasksList;
    }

    /**
     * This method is used for getting all task list.
     */
    private List<MmeTaskInstance> getBPMTasks() {
        String lcReqType = null;
        BpmSoapServiceClient bpmSoapServiceClient = null;
        bpmSoapServiceClient = new BpmSoapServiceClient();
        lcReqType = (String) ADFUtils.getViewScopeAttribute("pInboxRequestType");
        List<MmeTaskInstance> tasksList = null;
        if (null != lcReqType) {
            tasksList = bpmSoapServiceClient.retrieveUserInboxTasks(this.getLoggedInUser(), lcReqType);
        } else {
            tasksList =
                bpmSoapServiceClient.retrieveUserInboxTasks(this.getLoggedInUser(),
                                                            "NATURAL_RESOURCES_PRIV_PROCESS_DEV");
        }
        return tasksList;
    }

    /**
     * This method is used for getting all acquired by task list
     */
    private List<MmeTaskInstance> getAllAcqiuredByTasks() {
        BpmSoapServiceClient bpmSoapServiceClient = null;
        bpmSoapServiceClient = new BpmSoapServiceClient();
        String requestType = null;
        List<MmeTaskInstance> acqiuredByTasks = null;
        if (requestType != null) {
            acqiuredByTasks = bpmSoapServiceClient.retrieveAllAcquiredByTasks(this.getLoggedInUser(), requestType);
        } else {
            acqiuredByTasks =
                bpmSoapServiceClient.retrieveAllAcquiredByTasks(this.getLoggedInUser(),
                                                                "NATURAL_RESOURCES_PRIV_PROCESS_DEV");
        }
        //        this.sortTaskInstances(acqiuredByTasks);
        //initialize waiting time
        LocalDateTime now = LocalDateTime.now();


        for (MmeTaskInstance t : acqiuredByTasks) {
            if (null != t.getUpdateDate()) {
                Date date = new Date();
                LocalDateTime ldt = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
                Duration duration = Duration.between(now, ldt);
                t.setPriority("" + Math.abs(duration.toHours()));
            }
            List<String> nameList = this.getNamesFromVC(t.getAcquiredBy());
            if (nameList.size() > 1) {

                t.setAcquiredBy(nameList.get(1));
            }
        }
        return acqiuredByTasks;
        //        this.setTaskList(acqiuredByTasks);

    }

    /**
     * Get logged in user details
     */
    private String getLoggedInUser() {
        String user = null;
        user = ADFUtils.getCurrentUserUsername();
        return user;
    }

    private List<String> getNamesFromVC(String qid) {
        OperationBinding operBnd = ADFUtils.findOperation("findNameByQid");
        operBnd.getParamsMap().put("qid", qid);
        return (List) operBnd.execute();
    }

}
