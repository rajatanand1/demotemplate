package DemoExternalTemplateApp.view.beans.common;

import java.io.Serializable;

import java.math.BigDecimal;

import javax.faces.event.ActionEvent;

import oracle.adf.controller.TaskFlowId;

import oracle.jbo.domain.Timestamp;

import qa.gov.mme.common.utils.ADFUtils;
import qa.gov.mme.common.beans.CommonBaseBean;

public class UserSessionBean extends CommonBaseBean implements Serializable {
    @SuppressWarnings("compatibility:5363541616163154449")
    private static final long serialVersionUID = 1L;
    private String currentPageTitle = "Home";
    //    "/WEB-INF/naturalresources/taskflows/home/NaturalResourcesHomeTF.xml#NaturalResourcesHomeTF";
    private transient String accessMode = "BACK";
    private String taskFlowId = "/WEB-INF/demotemplate/taskflows/home/DemoTemplateHomeTF.xml#DemoTemplateHomeTF";

    public UserSessionBean() {
    }

    public String onHomeAction() {
        System.out.println(" Calling inbox from inbox menu");
        setDynamicTaskFlowId("/WEB-INF/demotemplate/taskflows/home/DemoTemplateHomeTF.xml#DemoTemplateHomeTF");
        ADFUtils.putPageFlowScopeAttribute("pAccessMode", "BACK");
        ADFUtils.putPageFlowScopeAttribute("pTimeStamp", this.getCurrentTimeStamp());
        ADFUtils.markPageFlowScopeAsDirty();
        return null;
    }

    public String publicInboxTF() {
        System.out.println(" Calling inbox from inbox menu");
        setDynamicTaskFlowId("/WEB-INF/demotemplate/taskflows/inbox/DemoTemplateInboxTF.xml#DemoTemplateInboxTF");
        ADFUtils.putPageFlowScopeAttribute("pFromOutcome", "PUB_INBOX");
        ADFUtils.putPageFlowScopeAttribute("pAccessMode", "BACK");
        ADFUtils.putPageFlowScopeAttribute("pTimeStamp", this.getCurrentTimeStamp());
        ADFUtils.markPageFlowScopeAsDirty();
        return null;
    }

    public String privateInboxTF() {
        System.out.println("calling inbox privarte");
        setDynamicTaskFlowId("/WEB-INF/demotemplate/taskflows/submit/DemoTemplateSubmitTF.xml#DemoTemplateSubmitTF");
        ADFUtils.putPageFlowScopeAttribute("pFromOutcome", "CRUSHER_SUBMIT");
        ADFUtils.putPageFlowScopeAttribute("pRequestTypeId", new BigDecimal(56));
        ADFUtils.putPageFlowScopeAttribute("pTimeStamp", this.getCurrentTimeStamp());
        ADFUtils.putPageFlowScopeAttribute("pAccessMode", accessMode);
        setCurrentPageTitle("ERMIT_REQUEST");
        ADFUtils.markPageFlowScopeAsDirty();
        return null;
    }

    public String tasksAssignmentTF() {
        // Add event code here...
        return null;
    }

    public void setAccessMode(String accessMode) {
        this.accessMode = accessMode;
    }

    public String getAccessMode() {
        return accessMode;
    }

    public void setCurrentPageTitle(String currentPageTitle) {
        this.currentPageTitle = currentPageTitle;
    }

    public String getCurrentPageTitle() {
        return currentPageTitle;
    }

    public void setTaskFlowId(String taskFlowId) {
        this.taskFlowId = taskFlowId;
    }

    public String getTaskFlowId() {
        return taskFlowId;
    }

    private Timestamp getCurrentTimeStamp() {
        return new Timestamp(System.currentTimeMillis());
    }


    public TaskFlowId getDynamicTaskFlowId() {
        return TaskFlowId.parse(taskFlowId);
    }

    public void setDynamicTaskFlowId(String taskFlowId) {
        this.taskFlowId = taskFlowId;
    }

    public void goPublicInboxTF(ActionEvent actionEvent) {
        setDynamicTaskFlowId("/WEB-INF/demotemplate/taskflows/submit/DemoTemplateSubmitTF.xml#DemoTemplateSubmitTF");
    }

    public void goPrivateInboxTF(ActionEvent actionEvent) {
        System.out.println("calling inbox privarte");
        setDynamicTaskFlowId("/WEB-INF/demotemplate/taskflows/submit/DemoTemplateSubmitTF.xml#DemoTemplateSubmitTF");
        ADFUtils.putPageFlowScopeAttribute("pFromOutcome", "CRUSHER_SUBMIT");
        ADFUtils.putPageFlowScopeAttribute("pRequestTypeId", new BigDecimal(56));
        ADFUtils.putPageFlowScopeAttribute("pTimeStamp", this.getCurrentTimeStamp());
        ADFUtils.putPageFlowScopeAttribute("pAccessMode", accessMode);
        setCurrentPageTitle("ERMIT_REQUEST");
       //ADFUtils.markPageFlowScopeAsDirty();
    }
}
