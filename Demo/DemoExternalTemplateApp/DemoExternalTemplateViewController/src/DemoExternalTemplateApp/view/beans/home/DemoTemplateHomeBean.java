package DemoExternalTemplateApp.view.beans.home;

import oracle.jbo.domain.Timestamp;

import qa.gov.mme.common.utils.ADFUtils;
import qa.gov.mme.common.ws.proxy.client.BpmSoapServiceClient;

public class DemoTemplateHomeBean {

    public static final String NATURAL_RESOURCES_PRIV_PROCESS_DEV =
        "IssueCrusherLicense,QuarryProductionReportProcess,NewQuarryPermitRequestProcess,InspectionNaturalResProcess,RenewQuarryPermitRequest,IssueTransferDebrisLicense,AmendTransferDebrisLicense,RenewTransferDebrisLicense,TransferDebrisLicenseProcesses,RenewCrusherLicense,AmendCrusherLicense,IssueDetonationLicense,RenewDetonationLicense";

    public DemoTemplateHomeBean() {
        super();
    }

    public Integer getPublicInboxCount() {
        Integer count = 0;
        try {
            String user = null;
            user = "zaid"; // ADFUtils.getCurrentUserUsername();
            if (null != user) {
                BpmSoapServiceClient bpmSoapServiceClient = null;
                bpmSoapServiceClient = new BpmSoapServiceClient();
                count = bpmSoapServiceClient.retrieveUserInboxCount(user, NATURAL_RESOURCES_PRIV_PROCESS_DEV);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }

    public Integer getPrivateInboxCount() {
        Integer count = 0;
        try {
            String user = null;
            user = "zaid";
            ADFUtils.getCurrentUserUsername();
            if (null != user) {
                BpmSoapServiceClient bpmSoapServiceClient = null;
                bpmSoapServiceClient = new BpmSoapServiceClient();
                count = bpmSoapServiceClient.retrieveUserMyTasksCount(user, NATURAL_RESOURCES_PRIV_PROCESS_DEV);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }

    private Timestamp getCurrentTimeStamp() {
        return new Timestamp(System.currentTimeMillis());
    }
}
