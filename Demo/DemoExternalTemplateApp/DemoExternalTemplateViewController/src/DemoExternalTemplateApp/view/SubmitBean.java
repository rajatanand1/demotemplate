package DemoExternalTemplateApp.view;

import java.io.Serializable;

import oracle.adf.controller.TaskFlowId;

public class SubmitBean implements Serializable {
    private String taskFlowId = "/WEB-INF/qa/gov/mme/component/flows/common/CommonApplicantTF.xml#CommonApplicantTF";

    public SubmitBean() {
    }

    public TaskFlowId getDynamicTaskFlowId() {
        return TaskFlowId.parse(taskFlowId);
    }

    public void setDynamicTaskFlowId(String taskFlowId) {
        this.taskFlowId = taskFlowId;
    }
}
